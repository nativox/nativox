/* @flow */

import React from 'react'
import {AsyncStorage, Alert} from 'react-native'
import {
  StackNavigator,
  TabNavigator,
  DrawerNavigator,
  NavigationActions,
} from 'react-navigation'
import codePush from 'react-native-code-push'
import InitialTutorialScreen from './src/initialTutorial/InitialTutorialScreen'
import LoginScreen from './src/login/LoginScreen'
import RegisterScreen from './src/register/RegisterScreen'
import CategoriesScreen from './src/categories/CategoriesScreen'
import CategoryScreen from './src/categories/CategoryScreen'
import TutorialScreen from './src/tutorial/TutorialScreen'
import VideoTutorialScreen from './src/tutorial/VideoTutorialScreen'
import ProfileScreen from './src/profile/ProfileScreen'
import ContactScreen from './src/contact/ContactScreen'
import AccountScreen from './src/account/UpdateAccountScreen'
import SettingsScreen from './src/settings/SettingsScreen'
import MyCoursesScreen from './src/myCourses/MyCoursesScreen'
import LessonScreen from './src/lesson/LessonScreen'
import SectionsScreen from './src/lesson/SectionsScreen'
import Screen from './src/common/Screen'
import {strings} from './src/stringsApp'

import PushNotification from 'react-native-push-notification'
import Orientation from 'react-native-orientation'
import Device from 'react-native-device-info'
import SplashScreen from 'react-native-splash-screen'
import {Client} from 'bugsnag-react-native'

const bugsnag = new Client() // eslint-disable-line

PushNotification.configure({
  onNotification: notification => {
    alert(notification.message)
  },
})
PushNotification.setApplicationIconBadgeNumber(0)
PushNotification.getApplicationIconBadgeNumber(() => 0)

import {combineReducers, applyMiddleware, createStore} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'

import login from './src/login/loginDuck'
import register from './src/register/registerDuck'
import categories from './src/categories'
import contact from './src/contact/contact'
import account from './src/account'
import lesson from './src/lesson'

import {PRIMARY_COLOR} from './src/common/NativoxTheme'
import userSession from './src/user/userSession'

// $FlowIgnore
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore)
const store = createStoreWithMiddleware(
  combineReducers({
    login,
    register,
    categories: categories.reducer,
    contact,
    account: account.reducer,
    lesson: lesson.reducer,
  })
)
const {onLoadCategories} = categories.eventHandlers

const LessonApp = DrawerNavigator(
  {
    Lesson: {
      screen: LessonScreen,
    },
  },
  {
    drawerWidth: 400,
    headerMode: 'screen',
    contentComponent: SectionsScreen,
  }
)

const MainNavigator = TabNavigator(
  {
    Categories: {
      screen: CategoriesScreen,
    },
    Tutorial: {
      screen: TutorialScreen,
    },
    Profile: {
      screen: ProfileScreen,
    },
  },
  {
    tabBarOptions: {
      activeTintColor: PRIMARY_COLOR,
      showLabel: false,
      showIcon: true,
      style: {
        backgroundColor: 'rgb(25, 46, 52)',
      },
      indicatorStyle: {
        backgroundColor: PRIMARY_COLOR,
      },
    },
  }
)

const getInitialRouteName = hasSeenInitialTutorial => {
  const user = userSession.getUser()

  if (user && user.idUser) {
    return 'Main'
  }

  return hasSeenInitialTutorial === 'true' ? 'Login' : 'InitialTutorial'
}

class NativoxApp extends React.Component {
  state = {
    navigator: null,
  }

  // $FlowIgnore
  componentDidMount = async () => {
    if (!Device.isTablet()) {
      Orientation.lockToPortrait()
    } else {
      Orientation.unlockAllOrientations()
    }

    const hasSeenInitialTutorial = await AsyncStorage.getItem(
      'hasSeenInitialTutorial'
    )
    const user = userSession.getUser()

    if (user && user.idUser) {
      store.dispatch(onLoadCategories()).catch(error => {
        console.log(error)
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'Login'})],
        })

        this.navigator._navigation.dispatch(resetAction)
        setTimeout(
          () => Alert.alert('Nativox', strings.error_load_categories),
          500
        )
      })
    }

    if (user && user.language) {
      strings.setLanguage(user.language)
    }

    this.setState({
      navigator: StackNavigator(
        {
          InitialTutorial: {screen: InitialTutorialScreen},
          Login: {screen: LoginScreen},
          Register: {screen: RegisterScreen},
          Main: {screen: MainNavigator},
          VideoTutorial: {screen: VideoTutorialScreen},
          Contact: {screen: ContactScreen},
          Account: {screen: AccountScreen},
          Settings: {screen: SettingsScreen},
          MyCourses: {screen: MyCoursesScreen},
          Category: {screen: CategoryScreen},
          LessonApp: {
            screen: LessonApp,
            navigationOptions: {
              header: null,
            },
          },
        },
        {
          headerMode: 'screen',
          initialRouteName: getInitialRouteName(hasSeenInitialTutorial),
        }
      ),
    })

    SplashScreen.hide()
  }

  render() {
    const Navigator = this.state.navigator
    return (
      <Provider store={store}>
        {Navigator ? (
          <Navigator ref={navigator => (this.navigator = navigator)} />
        ) : (
          <Screen />
        )}
      </Provider>
    )
  }
}

export default codePush(NativoxApp)
