// flow-typed signature: 105df1fca1660bbd26b5f76d6550f82f
// flow-typed version: <<STUB>>/react-native-countdown-circle_v1.0.2/flow_v0.45.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'react-native-countdown-circle'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'react-native-countdown-circle' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'react-native-countdown-circle/src/index' {
  declare module.exports: any;
}

// Filename aliases
declare module 'react-native-countdown-circle/src/index.js' {
  declare module.exports: $Exports<'react-native-countdown-circle/src/index'>;
}
