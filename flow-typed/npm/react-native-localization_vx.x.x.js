// flow-typed signature: b72b683cc70e46f9d2593c1082f4b7f5
// flow-typed version: <<STUB>>/react-native-localization_v0.1.30/flow_v0.45.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'react-native-localization'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'react-native-localization' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'react-native-localization/LocalizedStrings' {
  declare module.exports: any;
}

// Filename aliases
declare module 'react-native-localization/LocalizedStrings.js' {
  declare module.exports: $Exports<'react-native-localization/LocalizedStrings'>;
}
