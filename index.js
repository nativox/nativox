/* @flow */

import {AppRegistry} from 'react-native'
import NativoxApp from './NativoxApp'

AppRegistry.registerComponent('nativox', () => NativoxApp)
