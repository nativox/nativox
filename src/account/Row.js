/* @flow */

import React from 'react'
import {Text, View, TextInput, StyleSheet} from 'react-native'
import {isSmallDevice} from '../common/DeviceUtility.js'
import Device from 'react-native-device-info'
import {FONT_SIZE_TEXT_INPUT, HEIGHT_TEXT_INPUT} from '../common/NativoxTheme'

const styles: Object = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: isSmallDevice() ? 6 : Device.isTablet() ? 18 : 12,
    marginBottom: isSmallDevice() ? 6 : Device.isTablet() ? 18 : 12,
  },
  label: {
    flex: 2,
    backgroundColor: 'rgba(255, 255, 255, 0)',
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isTablet() ? 26 : isSmallDevice() ? 12 : 16,
  },
  textInput: {
    height: HEIGHT_TEXT_INPUT,
    flexGrow: 4,
    backgroundColor: 'rgb(255, 255, 255)',
    borderRadius: 7,
    padding: Device.isTablet() ? 10 : 5,
    fontSize: FONT_SIZE_TEXT_INPUT,
  },
})

export default class Row extends React.PureComponent {
  props: {
    disable?: boolean,
    label: string,
    onChangeText?: Function,
    onRowFocus?: Function,
    password?: boolean,
    text: string,
  }

  render() {
    return (
      <View style={styles.row}>
        <Text style={styles.label}>{this.props.label}</Text>
        <TextInput
          editable={!this.props.disable}
          onChangeText={this.props.onChangeText}
          onFocus={this.props.onRowFocus}
          secureTextEntry={this.props.password}
          style={styles.textInput}
          underlineColorAndroid={'rgba(255, 255, 255, 0)'}
          value={this.props.text}
        />
      </View>
    )
  }
}
