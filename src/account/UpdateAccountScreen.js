/* @flow */

import React from 'react'
import {Text, View, StyleSheet} from 'react-native'
import Button from '../common/Button'
import {
  changeUserName,
  changeEmail,
  changePassword,
  changeConfirmationPassword,
  onSaveUserDetails,
  onLogout,
  onRestoreCourses,
} from './eventHandlers'
import {connect} from 'react-redux'
import {isSmallDevice} from '../common/DeviceUtility.js'
import Screen from '../common/Screen'
import userSession from '../user/userSession'
import Device from 'react-native-device-info'
import Row from './Row'
import {strings} from '../stringsApp'
import Orientation from 'react-native-orientation'
import ProgressHud from '../common/ProgressHud'
import {NavigationActions} from 'react-navigation'
import showAlertMessage from '../common/showAlertMessage'

const styles = StyleSheet.create({
  explain_update_data: {
    color: 'rgb(255, 255, 255)',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    fontSize: Device.isTablet() ? 27 : isSmallDevice() ? 13 : 16,
    fontFamily: 'HelveticaNeue',
    textAlign: 'justify',
    marginTop: Device.isTablet() ? 25 : 5,
    marginBottom: Device.isTablet() ? 25 : 5,
  },
  container: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  saveButton: {
    height: isSmallDevice() ? 27 : Device.isTablet() ? 70 : 40,
    marginBottom: isSmallDevice() ? 11 : 15,
  },
  saveButtonTitle: {
    fontSize: Device.isTablet() ? 40 : isSmallDevice() ? 17 : 19,
  },
  logoutButtonTitle: {
    fontSize: Device.isTablet() ? 40 : isSmallDevice() ? 17 : 19,
  },
  restoreButtonTitle: {
    fontSize: Device.isTablet() ? 40 : isSmallDevice() ? 17 : 19,
  },
  logoutButton: {
    height: isSmallDevice() ? 27 : Device.isTablet() ? 70 : 40,
    marginBottom: isSmallDevice() ? 11 : 15,
    backgroundColor: 'rgb(140, 1, 4)',
  },
  restoreButton: {
    height: isSmallDevice() ? 27 : Device.isTablet() ? 70 : 40,
    marginBottom: isSmallDevice() ? 11 : 15,
  },
})

class UpdateAccountScreen extends React.Component {
  static navigationOptions = {
    header: null,
  }

  props: {
    changeConfirmationPassword: Function,
    changeEmail: Function,
    changePassword: Function,
    changeUserName: Function,
    navigation: Object,
    onLogout: Function,
    onRestoreCourses: Function,
    onSaveUserDetails: Function,
    state: any,
  }

  state: {
    focusedField?: any,
    accountState: any,
    orientation: any,
    showProgressDialog: boolean,
    progressTitle: string,
  }

  constructor(props) {
    super(props)

    this.state = {
      focusedField: null,
      accountState: props.state,
      orientation: Orientation.getInitialOrientation(),
      showProgressDialog: false,
      progressTitle: '',
    }

    Orientation.addOrientationListener(this.orientationDidChange.bind(this))
  }

  componentDidMount() {
    const user = userSession.getUser()

    if (user) {
      this.props.changeEmail(user.email)
      this.props.changeUserName(user.name)
    }

    Orientation.getOrientation((err, orientation) => {
      this.setState({orientation})
    })
  }

  componentWillUnmount() {
    Orientation.removeOrientationListener(this.orientationDidChange)
  }

  orientationDidChange(orientation: any) {
    this.setState({orientation})
  }

  onRowFocus = (ref: any) => {
    this.setState({focusedField: ref})
  }

  onPressBackButton = () => {
    this.props.navigation.goBack()
  }

  onPressSaveDetailsButton = () => {
    this.props.onSaveUserDetails({
      onShowProgressDialog: title => {
        this.setState({
          showProgressDialog: true,
          progressTitle: title,
        })
      },
      onDismissProgressDialog: () => {
        this.setState({
          showProgressDialog: false,
          progressTitle: '',
        })
      },
    })
  }

  onPressLogoutButton = () => {
    this.props.onLogout({
      onShowProgressDialog: title => {
        this.setState({
          showProgressDialog: true,
          progressTitle: title,
        })
      },
      onDismissProgressDialog: () => {
        this.setState({
          showProgressDialog: false,
          progressTitle: '',
        })
      },
      onSuccess: () => {
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'Login'})],
        })
        this.props.navigation.dispatch(resetAction)
      },
    })
  }

  onPressRestoreCoursesButton = () => {
    this.props.onRestoreCourses({
      onShowProgressDialog: title => {
        this.setState({
          showProgressDialog: true,
          progressTitle: title,
        })
      },
      onDismissProgressDialog: () => {
        this.setState({
          showProgressDialog: false,
          progressTitle: '',
        })
      },
      onFailure: () => {
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'Login'})],
        })
        this.props.navigation.dispatch(resetAction)
        showAlertMessage(strings.error_load_categories)
      },
    })
  }

  render() {
    const {state} = this.props

    return (
      <Screen
        focusedField={this.refs[this.state.focusedField]} // eslint-disable-line react/no-string-refs
        loadingTitle={strings.loadingProgressDialogTitle}
        onPressLeftButton={this.onPressBackButton}
        scroll
        showLeftNavBar
        title={strings.account_title}
      >
        <ProgressHud
          isVisible={this.state.showProgressDialog}
          text={this.state.progressTitle}
        />
        <Text style={styles.explain_update_data}>
          {strings.userDataExplanaitionLabel}
        </Text>
        <View style={styles.container}>
          <Row disable={true} label="Email" text={state.get('email')} />
          <Row
            label={strings.userNameLabel}
            onChangeText={this.props.changeUserName}
            onRowFocus={() => this.onRowFocus('name')}
            ref="name" // eslint-disable-line react/no-string-refs
            text={state.get('name')}
          />
          <Row
            label={strings.userPasswordLabel}
            onChangeText={this.props.changePassword}
            onRowFocus={() => this.onRowFocus('password')}
            password
            ref="password" // eslint-disable-line react/no-string-refs
            text={state.get('password')}
          />
          <Row
            label={strings.userPasswordConfirmLabel}
            onChangeText={this.props.changeConfirmationPassword}
            onRowFocus={() => this.onRowFocus('confirmationPassword')}
            password
            ref="confirmationPassword" // eslint-disable-line react/no-string-refs
            text={state.get('confirmationPassword')}
          />
        </View>
        <Button
          onPress={this.onPressSaveDetailsButton}
          style={styles.saveButton}
          title={strings.saveButtonTitle}
          titleStyle={styles.saveButtonTitle}
        />
        {this.state.orientation === 'LANDSCAPE' && Device.isTablet()
          ? null
          : <View>
              <Button
                onPress={this.onPressRestoreCoursesButton}
                style={styles.restoreButton}
                title={strings.restoreButtonTitle}
                titleStyle={styles.restoreButtonTitle}
              />
              <Button
                onPress={this.onPressLogoutButton}
                style={styles.logoutButton}
                title={strings.logoutButtonTitle}
                titleStyle={styles.logoutButtonTitle}
              />
            </View>}
      </Screen>
    )
  }
}

export default connect(
  state => ({
    state: state.account,
  }),
  dispatch => ({
    changeUserName: (text: string) => dispatch(changeUserName(text)),
    changeEmail: (text: string) => dispatch(changeEmail(text)),
    changePassword: (text: string) => dispatch(changePassword(text)),
    changeConfirmationPassword: (text: string) =>
      dispatch(changeConfirmationPassword(text)),
    onSaveUserDetails: props => dispatch(onSaveUserDetails(props)),
    onLogout: props => dispatch(onLogout(props)),
    onRestoreCourses: props => dispatch(onRestoreCourses(props)),
  })
)(UpdateAccountScreen)
