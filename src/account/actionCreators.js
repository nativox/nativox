/* @flow */

import {createAction} from 'redux-actions'

export const setUserName: any = createAction('SET_USER_NAME')
export const setEmail: any = createAction('SET_EMAIL')
export const setPassword: any = createAction('SET_PASSWORD')
export const setConfirmationPassword: any = createAction(
  'SET_CONFIRMATION_PASSWORD',
)
export const startSavingUserDetails: any = createAction(
  'START_SAVING_USER_DETAILS',
)
export const finishSavingUserDetails: any = createAction(
  'FINISH_SAVING_USER_DETAILS',
)
