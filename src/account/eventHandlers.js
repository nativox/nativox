/* @flow */

import categories from '../categories'
import {post} from '../common/apiRequest'
import {strings} from '../stringsApp'
import userSession from '../user/userSession'
import {NativeModules} from 'react-native'
import {Answers} from 'react-native-fabric'
import {
  setConfirmationPassword,
  setEmail,
  setUserName,
  setPassword,
  startSavingUserDetails,
  finishSavingUserDetails,
} from './actionCreators'
import showAlertMessage from '../common/showAlertMessage'

const {onClearCategories, onLoadCategories} = categories.eventHandlers
const {InAppUtils} = NativeModules

export const changeUserName = (userName: string) => (dispatch: Function) =>
  dispatch(setUserName(userName))
export const changeEmail = (email: string) => (dispatch: Function) =>
  dispatch(setEmail(email))
export const changePassword = (password: string) => (dispatch: Function) =>
  dispatch(setPassword(password))

export const changeConfirmationPassword = (confirmationPassword: string) => (
  dispatch: Function
) => dispatch(setConfirmationPassword((confirmationPassword: string)))

export const onSaveUserDetails = ({
  onShowProgressDialog,
  onDismissProgressDialog,
}: {
  onShowProgressDialog: Function,
  onDismissProgressDialog: Function,
}) => {
  return (dispatch: Function, getState: Function) => {
    const password = getState().account.get('password')
    const confirmationPassword = getState().account.get('confirmationPassword')

    if (password.length > 0 && password.length < 6) {
      showAlertMessage(strings.errorNewPasswordUpdateAccount)
      
      return
    }

    if (password !== confirmationPassword) {
      showAlertMessage(strings.errorNewOldPasswordUpdateAccount)
      
      return
    }

    const params = {
      nuevo_correo: getState().account.get('email'),
      nuevo_nombre: getState().account.get('name'),
      suscripcion: '0',
      nuevo_password: '',
    }

    if (password.length > 0) {
      params['nuevo_password'] = password
    }

    dispatch(startSavingUserDetails())
    onShowProgressDialog(strings.savingProgressDialogTitle)
    post('usuario_editar', params)
      .then(response => {
        onDismissProgressDialog()
        dispatch(finishSavingUserDetails())
        if (response.status === 'true') {
          const user = userSession.getUser()

          if (user) {
            user.name = getState().account.get('name')
            userSession.setUser(user)
            Answers.logCustom('save_user', {
              name: user.name,
              idUser: user.idUser,
            })
          }

          dispatch(setPassword(''))
          dispatch(setConfirmationPassword(''))

          showAlertMessage(response.result)

          return
        }

        showAlertMessage(strings.error_saving_account)        
      })
      .catch(() => {
        onDismissProgressDialog()
        dispatch(finishSavingUserDetails())
        
        showAlertMessage(strings.error_saving_account)
      })
  }
}

export const onRestoreCourses = ({
  onShowProgressDialog,
  onDismissProgressDialog,
  onFailure,
}: {
  onShowProgressDialog: Function,
  onDismissProgressDialog: Function,
  onFailure: Function,
}) => async (dispatch: Function) => {
  onShowProgressDialog('Restoring courses')
  InAppUtils.restorePurchases(async (error, response) => {
    onDismissProgressDialog()

    if (error) {
      showAlertMessage(strings.error_app_store)        
      
      return
    }

    if (response.length == 0) {
      showAlertMessage(strings.error_no_purchases)        
      
      return
    }

    const user = userSession.getUser()

    if (user) {
      Answers.logCustom('restore_courses', {
        name: user.name,
        idUser: user.idUser,
      })
    }

    showAlertMessage(strings.restore_purchases_success)        
    
    try {
      await dispatch(onClearCategories())
      await dispatch(onLoadCategories())
    } catch (error) {
      console.log(error) //eslint-disable-line
      onFailure()            
    }
  })
}

export const onLogout = ({
  onShowProgressDialog,
  onDismissProgressDialog,
  onSuccess,
}: {
  onShowProgressDialog: Function,
  onDismissProgressDialog: Function,
  onSuccess: Function,
}) => async (dispatch: Function) => {
  onShowProgressDialog(strings.progress_dialog_logout)

  try {
    await post('logout')

    const user = userSession.getUser()

    if (user) {
      Answers.logCustom('user_logout', {
        name: user.name,
        idUser: user.idUser,
      })
    }

    onDismissProgressDialog()

    dispatch(onClearCategories())
    userSession.resetUser()
    onSuccess()
  } catch (error) {
    console.log(error) //eslint-disable-line
    onDismissProgressDialog()
  }
}
