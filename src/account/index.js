/* @flow */

import * as eventHandlers from './eventHandlers'
import * as actionCreators from './actionCreators'
import reducer from './reducer'

export default {
  eventHandlers,
  actionCreators,
  reducer,
}
