/* @flow */

import {handleActions} from 'redux-actions'
import immutable from 'immutable'

import {
  setUserName,
  setEmail,
  setPassword,
  setConfirmationPassword,
  startSavingUserDetails,
  finishSavingUserDetails,
} from './actionCreators'

export default handleActions(
  {
    [setUserName]: (state, action) => state.set('name', action.payload),
    [setEmail]: (state, action) => state.set('email', action.payload),
    [setPassword]: (state, action) => state.set('password', action.payload),
    [setConfirmationPassword]: (state, action) =>
      state.set('confirmationPassword', action.payload),
    [startSavingUserDetails]: state => state.set('isSendingUserDetails', true),
    [finishSavingUserDetails]: state =>
      state.set('isSendingUserDetails', false),
  },
  immutable.fromJS({
    name: '',
    email: '',
    password: '',
    confirmationPassword: '',
    isSendingUserDetails: false,
  }),
)
