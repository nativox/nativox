/* @flow */

import Device from 'react-native-device-info'
import React from 'react'
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native'

const styles: Object = StyleSheet.create({
  categorieRow: {
    flex: 3,
    justifyContent: 'center',
    borderRadius: 5,
    margin: 3,
    padding: 5,
  },
  superPackRow: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 5,
    margin: 3,
    backgroundColor: 'rgb(56, 56, 56)',
  },
  categoriesListContainer: {
    flex: 1,
  },
  categorieImage: {
    flex: 1,
  },
  categorieLabelContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  categorieLabel: {
    fontSize: Device.isTablet() ? 22 : 16,
    color: 'rgb(255, 255, 255)',
  },
})

const CategoryRow = ({
  category,
  onPressCategory,
}: {
  category: Object,
  onPressCategory: Function,
}) => {
  return (
    <TouchableOpacity
      onPress={() => onPressCategory(category)}
      style={[styles.categorieRow, {backgroundColor: category.color_hex}]}>
      <View style={{flex: 4, padding: 10}}>
        <Image
          resizeMode="contain"
          source={{uri: category.icono_url}}
          style={styles.categorieImage}
        />
      </View>
      <View style={styles.categorieLabelContainer}>
        <Text style={styles.categorieLabel}>{category.categoria}</Text>
      </View>
    </TouchableOpacity>
  )
}

const SuperPackText = ({text}: {text: string}) =>
  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    <Text
      style={{color: 'rgb(62, 169, 222)', fontSize: 20, fontWeight: 'bold'}}>
      {text}
    </Text>
  </View>

const SuperPackRow = ({
  category,
  onPressCategory,
}: {
  category: Object,
  onPressCategory: Function,
}) =>
  <TouchableOpacity
    onPress={() => onPressCategory(category)}
    style={styles.superPackRow}>
    <SuperPackText text="Super" />
    <Image
      resizeMode="contain"
      source={{uri: 'icon_superpack', isStatic: true}}
      style={styles.categorieImage}
    />
    <SuperPackText text="Pack" />
  </TouchableOpacity>

const CategoriesList = ({
  categories,
  onPressCategory,
}: {
  categories: Array<any>,
  onPressCategory: Function,
}) =>
  <View style={styles.categoriesListContainer}>
    {categories.map((category: Object, index: number) => {
      if (category.id_in_app_ios === 'id_super_pack' || category.id_in_app_ios === 'com.nativox.course.10') {
        return (
          <SuperPackRow
            category={category}
            key={index}
            onPressCategory={onPressCategory}
          />
        )
      }

      return (
        <CategoryRow
          category={category}
          key={index}
          onPressCategory={onPressCategory}
        />
      )
    })}
  </View>

export default CategoriesList
