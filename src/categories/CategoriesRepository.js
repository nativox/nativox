/* @flow */

import {get} from '../common/apiRequest'
import {AsyncStorage} from 'react-native'

export const removeCategories = () => AsyncStorage.removeItem('categories')

export const getOfflineCategories = async () => {
  const categoriesString = await AsyncStorage.getItem('categories')
  if (!categoriesString) {
    return []
  }

  return JSON.parse(categoriesString)
}

export const getCategories = async () => {
  try {
    const {status, result} = await get('categorias', {incluir_cursos: 1})

    if (status !== 'true') {
      return []
    }

    await AsyncStorage.setItem('categories', JSON.stringify(result))

    return result
  } catch (error) {
    console.log('error: ', error) // eslint-disable-line no-console
    return []
  }
}
