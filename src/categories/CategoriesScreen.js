/* @flow */

import React from 'react'
import {
  Alert,
  Linking,
  Modal,
  StyleSheet,
  View,
  TouchableOpacity,
  WebView,
} from 'react-native'
import Screen from '../common/Screen'
import {connect} from 'react-redux'
import CategoriesList from './CategoriesList'
import {onCategoryPurchased, onClearCategories} from './eventHandlers'
import {get} from '../common/apiRequest'
import ActivityIndicatorView from '../common/ActivityIndicatorView'
import {NavigationActions} from 'react-navigation'
import {strings} from '../stringsApp'
import ProgressHud from '../common/ProgressHud'
import TappableIcon from '../common/TappableIcon'

const styles: any = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 9,
    paddingRight: 9,
    paddingBottom: 3,
  },
  modalPurchaseContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  overlayContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
  },
  webviewContainer: {
    height: 500,
    width: 300,
  },
  webview: {
    height: 500,
    width: 300,
    backgroundColor: 'transparent',
  },
})

const injectScriptPack = `
$('.go_to_comprar').click(function(event){
  postMessage('goToBuy:'+id_categoria);
  
  return false
});

$('.go_to_cerrar').click(function(){  
  postMessage('goToClose:'+id_categoria);
  
  return false  
});

$('.go_to_more_info').click(function(){ 
  postMessage('goToMoreInfo:'+'http://nativox.com/ini/app_info_pack/' + id_categoria);
    
  return false  
})
`
const injectScriptSuperPack = `
$('.go_to_comprar').click(function(){
  postMessage('goToBuy:'+'superpack');
  
	return false;
});

$('.go_to_cerrar').click(function(){
  postMessage('goToClose:'+'superpack');
  
	return false;
});

$('.go_to_more_info').click(function(){
  postMessage('goToMoreInfo:'+'http://nativox.com/ini/app_info_superpack/');
  
	return false;
})
`
let tabBarIconStyleBook = null

class CategoriesScreen extends React.Component {
  static navigationOptions = {
    header: null,

    tabBarIcon: ({tintColor}) => {
      if (!tabBarIconStyleBook || tabBarIconStyleBook.color !== tintColor) {
        tabBarIconStyleBook = {color: tintColor}
      }

      return (
        <TappableIcon
          containerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          name="book"
          size={25}
          style={tabBarIconStyleBook}
        />
      )
    },
  }

  props: {
    categories: Array<any>,
    isHydrating: boolean,
    onClearCategories: Function,
    onCategoryPurchased: Function,
    navigation: Object,
  }

  state: {
    categoriePopupVisible: boolean,
    categoryUrl?: string,
    injectScript?: string,
    purchaseCategoryProgress: boolean,
  }

  state = {
    categoriePopupVisible: false,
    categoryUrl: null,
    injectScript: '',
    purchaseCategoryProgress: false,
  }

  // $FlowIgnore
  componentDidMount = async () => {
    try {
      const {status} = await get('test_token')

      if (status === 'false') {
        this.resetSession()

        Alert.alert('Nativox', strings.error_expired_session)
      }
    } catch (error) {
      this.resetSession()

      Alert.alert('Nativox', strings.error_load_categories)
    }
  }

  resetSession = () => {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Login'})],
    })
    this.props.navigation.dispatch(resetAction)
    this.props.onClearCategories()
  }

  onPressCategory = (category: Object) => {
    if (!category.comprado) {
      let injectScript = ''

      if (
        category.id_in_app_ios === 'id_super_pack' ||
        category.id_in_app_ios === 'com.nativox.course.10'
      ) {
        injectScript = injectScriptSuperPack
      } else {
        injectScript = injectScriptPack
      }

      this.setState({
        categoriePopupVisible: true,
        categoryUrl: category.categoryUrl,
        injectScript,
      })
    } else {
      this.props.navigation.navigate('Category', category.id_categoria)
    }
  }

  onClosePurchasePopup = () => {
    this.setState({
      categoriePopupVisible: false,
      categoryUrl: null,
    })
  }

  onPressBuyCategory = (categoryId: string) => {
    const category = this.props.categories.find(
      category => category.id_categoria === categoryId
    )

    this.setState({
      categoriePopupVisible: false,
      categoryUrl: null,
    })
    this.props.onCategoryPurchased(
      category,
      () =>
        this.setState({
          purchaseCategoryProgress: true,
        }),
      () =>
        this.setState({
          purchaseCategoryProgress: false,
        })
    )
  }

  onMessage = (event: any) => {
    const message = event.nativeEvent.data
    const datas = message.split(':')
    const option = datas[0]

    switch (option) {
      case 'goToBuy':
        this.onPressBuyCategory(message.split(`${option}:`)[1])
        break
      case 'goToClose':
        this.onClosePurchasePopup()
        break
      case 'goToMoreInfo':
        Linking.openURL(message.split(`${option}:`)[1])
        break
    }
  }

  render() {
    return (
      <Screen fullscreen title={strings.courses_label}>
        <ProgressHud
          isVisible={this.state.purchaseCategoryProgress}
          text={strings.categories_progress}
        />
        <Modal
          animationType={'slide'}
          style={{flex: 1}}
          transparent
          visible={this.state.categoriePopupVisible}
        >
          <View style={styles.modalPurchaseContainer}>
            <TouchableOpacity
              activeOpacity={1}
              onPress={this.onClosePurchasePopup}
              style={styles.overlayContainer}
            />
            <View style={styles.webviewContainer}>
              <WebView
                injectedJavaScript={this.state.injectScript}
                onMessage={this.onMessage}
                renderLoading={() => <ActivityIndicatorView />}
                scrollEnabled
                source={{uri: this.state.categoryUrl}}
                startInLoadingState
                style={styles.webview}
              />
            </View>
          </View>
        </Modal>
        <View style={styles.container}>
          {this.props.isHydrating ? (
            <ActivityIndicatorView />
          ) : (
            <CategoriesList
              categories={this.props.categories}
              onPressCategory={this.onPressCategory}
            />
          )}
        </View>
      </Screen>
    )
  }
}

export default connect(
  state => ({
    isHydrating: state.categories.isHydrating,
    categories: state.categories.categories,
  }),
  dispatch => ({
    onCategoryPurchased: (category, onPurchaseStarted, onPurchaseCompleted) =>
      dispatch(
        onCategoryPurchased(category, onPurchaseStarted, onPurchaseCompleted)
      ),
    onClearCategories: () => dispatch(onClearCategories()),
  })
)(CategoriesScreen)
