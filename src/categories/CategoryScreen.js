/* @flow */

import React from 'react'
import Screen from '../common/Screen'
import {Image, StyleSheet, Text, View} from 'react-native'
import Device from 'react-native-device-info'
import {connect} from 'react-redux'
import lesson from '../lesson'
import CoursesList from '../myCourses/CoursesList'

const {onCourseLoaded} = lesson.eventHandlers

const styles: Object = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  categoryLabel: {
    fontSize: Device.isTablet() ? 36 : 22,
    color: 'rgb(255, 255, 255)',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    fontFamily: 'HelveticaNeue-Thin',
    textAlign: 'center',
    margin: 22,
  },
})

class CategoryScreen extends React.Component {
  static navigationOptions = {header: null}

  props: {
    navigation: Object,
    onCourseLoaded: Function,
    categories: Array<Object>,
  }

  state: {
    category: Object,
  }

  constructor(props) {
    super()

    const categoryId = props.navigation.state.params
    const category = props.categories.find(
      category => category.id_categoria === categoryId,
    )

    this.state = {
      category,
    }
  }

  goBack = () => {
    this.props.navigation.goBack()
  }

  onPressPlay = (course: Object) => {
    this.props.onCourseLoaded(course)
    this.props.navigation.navigate('LessonApp')
  }

  render() {
    return (
      <Screen fullscreen onPressLeftButton={this.goBack} showLeftNavBar>
        <View style={styles.container}>
          <Image
            resizeMode="contain"
            source={{
              uri: this.state.category.icono_url,
            }}
            style={{flex: 1}}
          />
          <View style={{flex: 5}}>
            <Text style={styles.categoryLabel}>
              {this.state.category.categoria}
            </Text>
            <CoursesList
              courses={this.state.category.cursos}
              onPressPlay={course => this.onPressPlay(course)}
            />
          </View>
        </View>
      </Screen>
    )
  }
}

export default connect(
  state => ({
    categories: state.categories.categories,
  }),
  dispatch => ({
    onCourseLoaded: course => dispatch(onCourseLoaded(course)),
  }),
)(CategoryScreen)
