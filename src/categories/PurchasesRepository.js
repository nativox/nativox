/* @flow */

import {onLoadCategories} from './eventHandlers'
import {Answers} from 'react-native-fabric'
import {AsyncStorage, Alert} from 'react-native'
import {post} from '../common/apiRequest'
import {strings} from '../stringsApp'

export const retrievePendingPurchases = async () => {
  try {
    const pendingPurchases = await AsyncStorage.getItem('pendingPurchases')

    if (pendingPurchases) {
      return JSON.parse(pendingPurchases)
    }

    return []
  } catch (error) {
    return []
  }
}

export const savePendingPurchase = async (checkoutParams: Object) => {
  const pendingPurchases = await retrievePendingPurchases()

  AsyncStorage.setItem(
    'pendingPurchases',
    JSON.stringify([...pendingPurchases, checkoutParams])
  )
}

export const sendPurchase = async (params: Object, dispatch: Function) => {
  try {
    const response = await post('pedido_completado', {
      ...params,
      sandbox: 'false',
    })

    if (response.status === 'true') {
      Answers.logPurchase(
        params.total,
        params.moneda,
        true,
        params.nombre,
        'Curso',
        params.in_app_purchase_id
      )
      await dispatch(onLoadCategories())
      return true
    } else {
      const responseSandBox = await post('pedido_completado', {
        ...params,
        sandbox: 'true',
      })

      if (responseSandBox.status === 'true') {
        Answers.logPurchase(
          params.total,
          params.moneda,
          true,
          params.nombre,
          'Curso',
          params.in_app_purchase_id
        )
        await dispatch(onLoadCategories())
        return true
      }
    }

    return false
  } catch (error) {
    return false
  }
}

const sendPendingPurchase = async (
  pendingPurchases: Array<Object>,
  purchase: Object,
  dispatch: Function
) => {
  if (purchase) {
    const result = await sendPurchase(purchase, dispatch)

    if (!result) {
      Alert.alert('Nativox', strings.pendingPurchaseError)

      return
    }

    pendingPurchases.shift()
    AsyncStorage.setItem('pendingPurchases', JSON.stringify(pendingPurchases))

    if (pendingPurchases.length > 0) {
      const pendingPurchase = pendingPurchases[0]

      await sendPendingPurchase(pendingPurchases, pendingPurchase, dispatch)
    }
  }
}

export const sendPendingPurchases = async (dispatch: Function) => {
  const pendingPurchases = await retrievePendingPurchases()

  if (pendingPurchases.length > 0) {
    const pendingPurchase = pendingPurchases[0]

    await sendPendingPurchase(
      pendingPurchases.splice(0, 1),
      pendingPurchase,
      dispatch
    )
  }
}
