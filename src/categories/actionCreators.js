/* @flow */

import {createAction} from 'redux-actions'

export const hydrateCategories: any = createAction('HYDRATE_CATEGORIES')
export const startHydratingCategories: any = createAction(
  'START_HYDRATING_CATEGORIES',
)
export const finishHydratingCategories: any = createAction(
  'FINISH_HYDRATING_CATEGORIES',
)
export const clearCategories: any = createAction('CLEAR_CATEGORIES')
export const updateCategoryCourseStats: any = createAction(
  'UPDATE_CATEGORY_COURSE_STATS',
)
