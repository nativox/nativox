/* @flow */

import {getCategories, removeCategories} from './CategoriesRepository'
import {
  clearCategories,
  hydrateCategories,
  startHydratingCategories,
  finishHydratingCategories,
} from './actionCreators'
import InAppBilling from 'react-native-billing'
import userSession from '../user/userSession'
import {savePendingPurchase, sendPurchase} from './PurchasesRepository'
import {strings} from '../stringsApp'
import showAlertMessage from '../common/showAlertMessage'

export const onLoadCategories = () => async (dispatch: Function) => {
  dispatch(startHydratingCategories())

  let categories = await getCategories()
  const boughtCategories = categories.filter(
    (category: Object) => category.comprado
  )

  if (boughtCategories.length !== categories.length) {
    categories.push({
      id_in_app_ios: 'com.nativox.course.10',
      id_categoria: 'id_super_pack',
    })
  }

  categories = categories.map(category => {
    if (category.id_categoria === 'id_super_pack') {
      return category
    }

    return {
      ...category,
      info: null,
      id_in_app_ios: category.demo !== 1 ? 'com.nativox.course.6' : null,
      cursos: category.cursos.map(curso => ({
        ...curso,
        color_hex: category.color_hex,
      })),
    }
  })

  let products = null
  try {
    await InAppBilling.close()
    await InAppBilling.open()
    products = await InAppBilling.getProductDetailsArray([
      'com.nativox.course.6',
      'com.nativox.course.10',
    ])
  } catch (error) {
    if (__DEV__) {
      console.log(error) // eslint-disable-line no-console
      dispatch(
        hydrateCategories(
          categories.map(category => ({
            ...category,
            categoryUrl: `http://nativox.com/ini/app_popup_pack/${
              category.id_categoria
            }?os=android&moneda=eur&precio=1.50`,
          }))
        )
      )
      dispatch(finishHydratingCategories())

      return
    } else {
      throw error
    }
  }

  if (!products) {
    dispatch(hydrateCategories(categories))
    dispatch(finishHydratingCategories())

    return
  }

  const newCategories = categories.map((category: Object) => {
    if (category.demo === 1 || !products) {
      return category
    }

    const product = products.find(
      product => product.productId === category.id_in_app_ios
    )

    if (category.id_in_app_ios === 'com.nativox.course.10') {
      category.categoria = 'Superpack'
      category.categoryUrl = `http://nativox.com/ini/app_popup_superpack?os=android&moneda=${
        product.currency
      }&precio=${product.priceText}`
    } else {
      category.categoryUrl = `http://nativox.com/ini/app_popup_pack/${
        category.id_categoria
      }?os=android&moneda=${product.currency}&precio=${product.priceText}`
    }

    category.total = product.priceText
    category.currency = product.currency

    return category
  })

  dispatch(hydrateCategories(newCategories))
  dispatch(finishHydratingCategories())
}

export const onClearCategories = () => async (dispatch: Function) => {
  await removeCategories()
  dispatch(clearCategories())
}

export const onCategoryPurchased = (
  category: Object,
  onPurchaseStarted: Function,
  onPurchaseCompleted: Function
) => async (dispatch: Function) => {
  try {
    const productId = category.id_in_app_ios
    await InAppBilling.close()
    await InAppBilling.open()
    const details = await InAppBilling.purchase(productId)

    if (details) {
      const user = userSession ? userSession.getUser() || {} : {}
      const cursos = category.cursos.map(curso => curso.id_curso)
      const params = {
        total: category.total,
        lang: user.language,
        moneda: category.currency,
        transactionId: details.orderId,
        cursos,
        nombre: category.categoria,
      }

      onPurchaseStarted()

      const result = await sendPurchase(params, dispatch)

      onPurchaseCompleted()

      if (!result) {
        showAlertMessage(strings.purchaseError)

        savePendingPurchase(params)
      }
    } else {
      showAlertMessage(strings.purchaseError)
    }
  } catch (error) {
    showAlertMessage(error.message)
  }
}
