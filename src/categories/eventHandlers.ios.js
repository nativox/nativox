/* @flow */

import {getCategories, removeCategories} from './CategoriesRepository'
import {
  clearCategories,
  hydrateCategories,
  startHydratingCategories,
  finishHydratingCategories,
} from './actionCreators'
import {NativeModules} from 'react-native'
import DeviceInfo from 'react-native-device-info'
import userSession from '../user/userSession'
import {savePendingPurchase, sendPurchase} from './PurchasesRepository'
import {strings} from '../stringsApp'
import {post} from '../common/apiRequest'
import Device from 'react-native-device-info'
import showAlertMessage from '../common/showAlertMessage'

const {InAppUtils} = NativeModules

const loadProducts = productIds => {
  return new Promise((resolve, reject) => {
    InAppUtils.loadProducts(
      productIds,
      (error: Object, products: Array<Object>) => {
        if (error) {
          reject(error)
        }

        resolve(products)
      }
    )
  })
}

export const onLoadCategories = () => async (dispatch: Function) => {
  dispatch(startHydratingCategories())

  let categories = await getCategories()
  const boughtCategories = categories.filter(
    (category: Object) => category.comprado
  )

  if (boughtCategories.length !== categories.length) {
    categories.push({
      id_in_app_ios: 'id_super_pack',
      id_categoria: 'superpack',
      cursos: [],
    })
  }

  categories = categories.map(category => ({
    ...category,
    cursos: category.cursos.map(curso => ({
      ...curso,
      color_hex: category.color_hex,
    })),
  }))

  const productIds = categories
    .map((category: Object) => category.id_in_app_ios)
    .filter((id: string) => id)
  try {
    const products = await loadProducts(productIds)
    const newCategories = categories.map((category: Object) => {
      const indexProduct = productIds.indexOf(category.id_in_app_ios)

      if (indexProduct === -1) {
        return category
      }

      const product = products[indexProduct]

      if (!product) {
        return category
      }

      if (category.id_in_app_ios === 'id_super_pack') {
        category.categoria = 'Superpack'
        category.categoryUrl = `http://nativox.com/ini/app_popup_superpack?os=ios&moneda=${
          product.currencyCode
        }&precio=${product.priceString}`
      } else {
        category.categoryUrl = `http://nativox.com/ini/app_popup_pack/${
          category.id_categoria
        }?os=ios&moneda=${product.currencyCode}&precio=${product.priceString}`
      }

      category.total = product.priceString
      category.totalValue = product.price
      category.currency = product.currencyCode

      return category
    })

    dispatch(hydrateCategories(newCategories))
    dispatch(finishHydratingCategories())
  } catch (error) {
    if (__DEV__) {
      dispatch(
        hydrateCategories(
          categories.map(category => ({
            ...category,
            categoryUrl: `http://nativox.com/ini/app_popup_pack/${
              category.id_categoria
            }?os=ios&moneda=eur&precio=1.50`,
          }))
        )
      )
      dispatch(finishHydratingCategories())
    } else {
      throw error
    }
  }
}

export const onClearCategories = () => async (dispatch: Function) => {
  await removeCategories()
  dispatch(clearCategories())
}

const sendPurchaseNotRegisteredEmail = error => {
  const user = userSession ? userSession.getUser() || {} : {}
  const params = {
    mensaje: `
    Compra Realizada en AppStore pero no registrada
<br>
<br>
    Informacion del Dispositivo<br>
    ---------------------------<br>
<br>
    Modelo: ${Device.getModel()}<br>
    Nombre: ${Device.getDeviceName()}<br>
    Sistema Operativo: ${Device.getSystemName()}<br>
    Version del Sistema Operativo: ${Device.getSystemVersion()}<br>
<br>
    ---------------------------<br>

    Error<br>
    ---------------------------<br>
<br>
${JSON.stringify(error)}
<br>
---------------------------<br>
    `,
    correo: user.email,
    id_usuario: user.idUser,
  }

  post('contacto', params)
}

export const onCategoryPurchased = (
  category: Object,
  onPurchaseStarted: Function,
  onPurchaseCompleted: Function
) => async (dispatch: Function) => {
  const productId = category.id_in_app_ios

  InAppUtils.purchaseProduct(
    productId,
    async (error: Object, response: Object) => {
      if (error) {
        sendPurchaseNotRegisteredEmail(error)

        showAlertMessage(error.message)

        return
      }

      if (response && response.productIdentifier) {
        try {
          const user = userSession ? userSession.getUser() || {} : {}
          const cursos = category.cursos.map(curso => curso.id_curso)
          const params = {
            total: category.totalValue,
            lang: user.language,
            moneda: category.currency,
            transactionId: response.transactionIdentifier,
            receipt: response.transactionReceipt,
            in_app_purchase_id: category.id_in_app_ios,
            bundle_id: DeviceInfo.getBundleId(),
            cursos,
            nombre: category.categoria,
          }

          onPurchaseStarted()

          const result = await sendPurchase(params, dispatch)

          onPurchaseCompleted()

          if (!result) {
            showAlertMessage(strings.purchaseError)

            savePendingPurchase(params)
          }
        } catch (error) {
          onPurchaseCompleted()

          sendPurchaseNotRegisteredEmail(error)

          showAlertMessage(strings.purchaseError)
        }
      } else {
        sendPurchaseNotRegisteredEmail(
          new Error('Fallo Appstore no hay respuesta')
        )

        showAlertMessage(strings.purchaseError)
      }
    }
  )
}
