/* @flow */

import {handleActions} from 'redux-actions'
import {
  clearCategories,
  hydrateCategories,
  startHydratingCategories,
  finishHydratingCategories,
  updateCategoryCourseStats,
} from './actionCreators'

export default handleActions(
  {
    [hydrateCategories]: (state, action) => ({
      ...state,
      categories: action.payload,
    }),
    [startHydratingCategories]: state => ({...state, isHydrating: true}),
    [finishHydratingCategories]: state => ({...state, isHydrating: false}),
    [clearCategories]: state => ({...state, categories: []}),
    [updateCategoryCourseStats]: (state, action) => ({
      ...state,
      categories: state.categories.map(category => ({
        ...category,
        cursos: category.cursos.map(curso => {
          if (curso.id_curso === action.payload.id_curso) {
            return {
              ...curso,
              stats_curso: action.payload,
            }
          }

          return curso
        }),
      })),
    }),
  },
  {
    categories: [],
    isHydrating: false,
  },
)
