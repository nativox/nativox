import React from 'react'
import {ActivityIndicator, View} from 'react-native'
import Component from './Component'

export default class ActivityIndicatorView extends Component {
  render = () =>
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator animating size="large" style={{height: 80}} />
    </View>
}
