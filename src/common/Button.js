/* @flow */

import React from 'react'
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native'
import {
  PRIMARY_COLOR,
  BUTTON_BACKGROUND_DISABLE_COLOR,
  BUTTON_TITLE_COLOR,
} from './NativoxTheme.js'
import Device from 'react-native-device-info'
import Component from './Component'

const styles: Object = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: Device.isTablet() ? 60 : 35,
    borderRadius: 5,
  },
})

export default class Button extends Component {
  props: {
    disabled?: boolean,
    invisible?: boolean,
    onPress: Function,
    style?: any,
    title: string,
    titleStyle?: any,
    width?: number,
  }

  getBackgroundStyle() {
    if (this.props.invisible) {
      return
    }

    return {
      backgroundColor: this.props.disabled
        ? BUTTON_BACKGROUND_DISABLE_COLOR
        : PRIMARY_COLOR,
    }
  }

  getStyleTitle() {
    return {
      color: this.props.invisible ? PRIMARY_COLOR : BUTTON_TITLE_COLOR,
      fontSize: Device.isTablet() ? 23 : 19,
    }
  }

  getButtonStyle() {
    return [
      styles.button,
      this.getBackgroundStyle(),
      this.props.style,
      {width: this.props.width},
    ]
  }

  getTouchableStyle() {
    if (!this.props.width) {
      return
    }
    return {width: this.props.width + 3, alignSelf: 'center'}
  }

  onPressButton = () => {
    requestAnimationFrame(this.props.onPress)
  }

  render() {
    return (
      <TouchableOpacity
        disabled={this.props.disabled}
        onPress={this.onPressButton}
        style={this.getTouchableStyle()}
      >
        <View style={this.getButtonStyle()}>
          <Text style={[this.getStyleTitle(), this.props.titleStyle]}>
            {this.props.title}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}
