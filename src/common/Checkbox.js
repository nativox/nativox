/* @flow */

import React from 'react'
import {TouchableWithoutFeedback, View, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Component from './Component'

const styles: Object = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
})

export default class Checkbox extends Component {
  props: {
    checked: boolean,
    onChange: Function,
    size: number,
  }

  render = () =>
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={this.props.onChange}>
        <Icon
          color="white"
          name={this.props.checked ? 'check-square-o' : 'square-o'}
          size={this.props.size}
        />
      </TouchableWithoutFeedback>
    </View>
}
