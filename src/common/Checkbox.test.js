import React from 'react'
import Checkbox from './Checkbox'
import renderer from 'react-test-renderer'

test('renders correctly', () => {
  expect(renderer.create(<Checkbox size={20} />).toJSON()).toMatchSnapshot()
})
