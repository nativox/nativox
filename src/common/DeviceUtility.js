/* @flow */

import {Dimensions} from 'react-native'

const IPHONE4S_WIDTH = 320

export const isSmallDevice = () =>
  Dimensions.get('window').width <= IPHONE4S_WIDTH
