/* @flow */

import React from 'react'
import {Image, TouchableOpacity, StyleSheet} from 'react-native'
import Component from './Component'

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
})

const getImageStyle = (styleImage?: Object, width: number, height: number) => {
  if (!width && !height) {
    return styleImage
  }

  if (width && !height) {
    return [styleImage, {width}]
  }

  if (!width && height) {
    return [styleImage, {height}]
  }

  return [styleImage, {width, height}]
}

export default class ImageButton extends Component {
  props: {
    disabled?: boolean,
    height: number,
    image: string,
    onPress: Function,
    resizeMode?: string,
    style?: any,
    styleImage?: any,
    width: number,
  }

  onPressButton = () => {
    requestAnimationFrame(this.props.onPress)
  }

  render = () =>
    <TouchableOpacity
      activeOpacity={this.props.disabled ? 1 : 0.2}
      onPress={this.props.disabled ? null : this.onPressButton}
      style={[styles.container, this.props.style]}
    >
      <Image
        resizeMode={this.props.resizeMode}
        source={{uri: this.props.image, isStatic: true}}
        style={[
          getImageStyle(
            this.props.styleImage,
            this.props.width,
            this.props.height
          ),
          {opacity: this.props.disabled ? 0.3 : 1},
        ]}
      />
    </TouchableOpacity>
}
