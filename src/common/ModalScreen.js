/* @flow */

import React from 'react'
import {StyleSheet, View, Modal, TouchableWithoutFeedback} from 'react-native'
import TappableIcon from '../common/TappableIcon'
import Component from './Component'

const styles: Object = StyleSheet.create({
  backgroundContainer: {
    flexDirection: 'column',
    flexGrow: 1,
    backgroundColor: 'rgb(30, 33, 31)',
  },
  container: {
    flexGrow: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeButton: {
    alignSelf: 'flex-end',
  },
  childrenContainer: {
    padding: 10,
  },
})

export default class ModalScreen extends Component {
  props: {
    children?: any,
    height: number,
    isVisible: boolean,
    onCloseButtonPressed: Function,
    onTouchBackground: Function,
    width: number,
    fullscreen?: boolean,
    supportedOrientations?: Array<string>,
  }

  static defaultProps = {
    supportedOrientations: [
      'portrait',
      'portrait-upside-down',
      'landscape',
      'landscape-left',
      'landscape-right',
    ],
  }

  render = () =>
    <Modal
      animationType={'slide'}
      supportedOrientations={this.props.supportedOrientations}
      transparent={true}
      visible={this.props.isVisible}
    >
      <TouchableWithoutFeedback onPress={this.props.onTouchBackground}>
        <View style={styles.container}>
          <TouchableWithoutFeedback>
            {!this.props.fullscreen
              ? <View
                  style={[{height: this.props.height, width: this.props.width}]}
                >
                  <View
                    style={[
                      styles.backgroundContainer,
                      {height: this.props.height, width: this.props.width},
                    ]}
                  >
                    <View style={styles.childrenContainer}>
                      <View style={{alignItems: 'flex-end'}}>
                        <TappableIcon
                          name="times"
                          onPress={this.props.onCloseButtonPressed}
                          size={22}
                          style={styles.closeButton}
                        />
                      </View>
                      {this.props.children}
                    </View>
                  </View>
                </View>
              : <View
                  style={[{height: this.props.height, width: this.props.width}]}
                >
                  {this.props.children}
                </View>}
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
}
