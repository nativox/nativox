/* @flow */

import React from 'react'
import * as Progress from 'react-native-progress'
import {Modal, StyleSheet, Text, View} from 'react-native'
import Component from './Component'

const styles: Object = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  hudContainer: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 5,
    paddingBottom: 15,
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  hudTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    padding: 5,
    marginBottom: 5,
  },
})

export default class ProgressHud extends Component {
  props: {
    isVisible: boolean,
    text: string,
    supportedOrientations?: Array<string>,
  }

  static defaultProps = {
    supportedOrientations: [
      'portrait',
      'portrait-upside-down',
      'landscape',
      'landscape-left',
      'landscape-right',
    ],
  }

  render = () =>
    <Modal
      animationType={'slide'}
      supportedOrientations={this.props.supportedOrientations}
      transparent={true}
      visible={this.props.isVisible}
    >
      <View style={styles.container}>
        <View style={styles.hudContainer}>
          <Text style={styles.hudTitle}>{this.props.text}</Text>
          <Progress.Circle borderWidth={2} indeterminate={true} size={90} />
        </View>
      </View>
    </Modal>
}
