/* @flow */

import React from 'react'
import {
  StyleSheet,
  ScrollView,
  NativeModules,
  View,
  Text,
  findNodeHandle,
  SafeAreaView,
} from 'react-native'
import TappableIcon from './TappableIcon'
import Device from 'react-native-device-info'
import {isSmallDevice} from './DeviceUtility.js'
import Component from './Component'

const UIManager = NativeModules.UIManager
const styles: Object = StyleSheet.create({
  backgroundContainer: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: 'rgb(30, 33, 31)',
  },
  topLevelContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingTop: isSmallDevice() ? 3 : 15,
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: isSmallDevice() ? 3 : 15,
  },
  scrollContainer: {},
  scrollContentContainer: {
    flex: 1,
  },
  title: {
    color: 'rgb(255, 255, 255)',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    textAlign: 'center',
    fontSize: Device.isTablet() ? 90 : 44,
    fontFamily: 'HelveticaNeue-Thin',
    marginTop: 5,
    marginBottom: 10,
    flex: 1,
  },
  navigationBarContainer: {
    flexDirection: 'row',
    paddingTop: 17,
  },
  navigationBar: {alignSelf: 'flex-start'},
  leftButton: {padding: 7},
})

const NavigationBar = ({
  showLeftNavBar,
  onPressLeftButton,
  title,
}: {
  showLeftNavBar?: boolean,
  onPressLeftButton?: Function,
  title?: string,
}) => (
  <View style={styles.navigationBarContainer}>
    {showLeftNavBar ? (
      <View style={styles.navigationBar}>
        <TappableIcon
          name="angle-left"
          onPress={onPressLeftButton}
          size={Device.isTablet() ? 50 : 40}
          style={styles.leftButton}
        />
      </View>
    ) : null}
    {title ? <Text style={styles.title}>{title}</Text> : null}
  </View>
)

export default class Screen extends Component {
  state = {
    keyboardSpace: 0,
    keyboardHeight: 0,
  }

  props: {
    children?: any,
    focusedField?: any,
    fullscreen?: boolean,
    isTabScreen?: boolean,
    onPressLeftButton?: Function,
    scroll?: boolean,
    showLeftNavBar?: boolean,
    title?: string,
  }

  resetKeyboardSpace() {
    if (this.props.focusedField) {
      this.setState({keyboardHeight: 0})
      this.refs.scrollScreenView.scrollTo({x: 0, y: 0, animated: true}) // eslint-disable-line react/no-string-refs
    }
  }

  updateKeyboardSpace(frames: Object) {
    if (!this.props.focusedField) {
      return
    }

    if (frames.end) {
      this.setState({keyboardHeight: frames.end.height})

      const containerNode = findNodeHandle(this.refs.container) // eslint-disable-line react/no-string-refs
      const fieldViewNode = findNodeHandle(this.props.focusedField)

      UIManager.measureLayout(
        fieldViewNode,
        containerNode,
        () => {},
        (fieldX, fieldY) => {
          /* eslint-disable react/no-string-refs */
          this.refs.container.measure((x, y, width, height) => {
            if (height - frames.end.height - 40 < fieldY) {
              const moveY = fieldY - (height - frames.end.height) + 60

              this.refs.scrollScreenView.scrollTo({
                x: 0,
                y: moveY,
                animated: true,
              })
            }
          })
        }
      )
    }
  }

  getContainerStyle() {
    if (this.props.fullscreen) {
      return {flex: 1}
    }

    return this.props.isTabScreen
      ? [styles.container, {paddingBottom: 70}]
      : styles.container
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'rgb(30, 33, 31)'}}>
        <View style={styles.topLevelContainer}>
          <View
            ref="container" // eslint-disable-line react/no-string-refs
            style={styles.backgroundContainer}
          >
            <NavigationBar
              onPressLeftButton={this.props.onPressLeftButton}
              showLeftNavBar={this.props.showLeftNavBar}
              title={this.props.title}
            />
            <View style={this.getContainerStyle()}>
              {this.props.scroll ? (
                <ScrollView
                  contentContainerStyle={styles.scrollContentContainer}
                  contentInset={{bottom: this.state.keyboardSpace}}
                  keyboardDismissMode="interactive"
                  ref="scrollScreenView" // eslint-disable-line react/no-string-refs
                  scrollEnabled={false}
                  showsVerticalScrollIndicator
                  style={styles.scrollContainer}
                >
                  {this.props.children}
                </ScrollView>
              ) : (
                this.props.children
              )}
            </View>
          </View>
        </View>
      </SafeAreaView>
    )
  }
}
