/* @flow */

import React from 'react'
import {TouchableOpacity, View} from 'react-native'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import IoniconsIcon from 'react-native-vector-icons/Ionicons'
import Component from './Component'

const Icon = ({
  name,
  size,
  style,
}: {
  iconSet: string,
  name: string,
  size: number,
  style?: Object,
}) => (
  <FontAwesomeIcon
    color="rgb(255, 255, 255)"
    name={name}
    size={size}
    style={style}
  />
)

export default class TappableIcon extends Component {
  props: {
    containerStyle?: Object,
    name: string,
    onPress?: Function,
    size: number,
    style?: Object,
    iconSet?: string,
  }

  static defaultProps = {
    iconSet: 'fontawesome',
  }
  render() {
    if (this.props.onPress) {
      return (
        <TouchableOpacity
          onPress={this.props.onPress}
          style={[
            this.props.containerStyle,
            {backgroundColor: 'rgba(255, 255, 255, 0)'},
          ]}
        >
          <Icon
            iconSet={this.props.iconSet}
            name={this.props.name}
            size={this.props.size}
            style={this.props.style}
          />
        </TouchableOpacity>
      )
    }

    return (
      <View
        onPress={this.props.onPress}
        style={[
          this.props.containerStyle,
          {backgroundColor: 'rgba(255, 255, 255, 0)'},
        ]}
      >
        <Icon
          iconSet={this.props.iconSet}
          name={this.props.name}
          size={this.props.size}
          style={this.props.style}
        />
      </View>
    )
  }
}
