/* @flow */

import React from 'react'
import {StyleSheet, TextInput} from 'react-native'
import {FONT_SIZE_TEXT_INPUT, HEIGHT_TEXT_INPUT} from './NativoxTheme'
import Device from 'react-native-device-info'
import Component from './Component'

const styles: Object = StyleSheet.create({
  textInput: {
    height: HEIGHT_TEXT_INPUT,
    borderColor: 'rgb(109, 109, 109)',
    borderWidth: 1,
    backgroundColor: 'white',
    marginBottom: Device.isTablet() ? 20 : 10,
    padding: 5,
    fontSize: FONT_SIZE_TEXT_INPUT,
  },
})

class TextBox extends Component {
  props: {
    isPassword?: boolean,
    onChangeText: Function,
    onRowFocus?: Function,
    placeholder: string,
    style?: any,
    value: string,
  }

  render() {
    return (
      <TextInput
        autoCapitalize="none"
        autoCorrect={false}
        onChangeText={this.props.onChangeText}
        onFocus={this.props.onRowFocus}
        placeholder={this.props.placeholder}
        secureTextEntry={this.props.isPassword}
        style={[styles.textInput, this.props.style]}
        textAlign={'left'}
        underlineColorAndroid={'rgba(255, 255, 255, 0)'}
        value={this.props.value}
      />
    )
  }
}

export default TextBox
