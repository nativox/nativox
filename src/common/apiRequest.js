/* @flow */

import superagent from 'superagent'
import userSession from '../user/userSession'
import queryString from 'query-string'
import DeviceInfo from 'react-native-device-info'
import {Dimensions, Platform} from 'react-native'

export const HOST_NAME = 'https://nativox.com/apis/2.1/'
const {height, width} = Dimensions.get('window')

const handleResponse = (resolve: any, reject: any) => (
  error: Object,
  res: Object,
) => {
  if (error) {
    reject(error)
  }

  // $FlowIgnore
  if (__DEV__ && res) {
    console.log('Response: ', res.body) // eslint-disable-line no-console
  }

  if (res) {
    resolve(res.body)
  } else {
    reject('Error in Nativox try again.')
  }
}

export const getQuery = (parameters?: Object) => {
  const user = userSession ? userSession.getUser() : {}

  const queryParameters = {
    id_usuario: user ? user.idUser : '',
    token: user ? user.token : '',
    lang: user ? user.language : '',
    os: Platform.OS,
    device: DeviceInfo.getDeviceName(),
    height,
    width,
    version: DeviceInfo.getVersion(),
    id_session: user ? user.idSession : '',
    id_device: DeviceInfo.getUniqueID(),
    ...parameters,
  }

  return queryParameters
}

export const post = (endPoint: string, parameters?: Object) => {
  const user = userSession ? userSession.getUser() : {}

  const postParameters = {
    id_usuario: user ? user.idUser : '',
    token: user ? user.token : '',
    ...parameters,
  }

  return new Promise((resolve: Function, reject: Function) => {
    superagent
      .post(`${HOST_NAME}${endPoint}`)
      .query(getQuery(parameters))
      .send(queryString.stringify(postParameters))
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Accept', 'application/x-www-form-urlencoded')
      .end(handleResponse(resolve, reject))
  })
}

export const get = (endPoint: string, parameters?: Object) => {
  return new Promise((resolve: Function, reject: Function) => {
    superagent
      .get(`${HOST_NAME}${endPoint}`)
      .query(getQuery(parameters))
      .end(handleResponse(resolve, reject))
  })
}
