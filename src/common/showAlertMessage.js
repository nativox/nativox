/* @flow */

import {Alert} from 'react-native'

export default (message: string) => {
    setTimeout(() => Alert.alert('Nativox', message), 500)
}