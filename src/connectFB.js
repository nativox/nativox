/* @flow */

import {
  GraphRequest,
  GraphRequestManager,
  LoginManager,
  AccessToken,
} from 'react-native-fbsdk'
import {Alert} from 'react-native'
import showAlertMessage from './common/showAlertMessage'

const getFacebookUserInfo = (token, onLoggedInFB) => {
  new GraphRequestManager()
    .addRequest(
      new GraphRequest('/me?fields=id,name,email', null, (error, result) => {
        if (error) {
          showAlertMessage(error.message)

          return
        }

        onLoggedInFB({
          email: result.email,
          userName: result.name,
          idFacebook: result.id,
          name: result.name,
          token,
        })
      }),
    )
    .start()
}

export default async (onLoggedInFB: Function) => {
  try {
    const data = await AccessToken.getCurrentAccessToken()

    if (data && data.accessToken) {
      getFacebookUserInfo(data.accessToken, onLoggedInFB)

      return
    }

    const result = await LoginManager.logInWithReadPermissions([
      'public_profile',
      'email',
    ])

    if (!result.isCancelled) {
      const newData = await AccessToken.getCurrentAccessToken()

      getFacebookUserInfo(newData.accessToken, onLoggedInFB)
    }
  } catch (error) {
    console.log('error: ', error.message) // eslint-disable-line no-console

    showAlertMessage('There has been an error trying to login with Facebook, try it again later')    
  }
}
