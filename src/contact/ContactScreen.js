/* @flow */

import React from 'react'
import {View, StyleSheet, TextInput} from 'react-native'
import Screen from '../common/Screen'
import Device from 'react-native-device-info'
import Button from '../common/Button'
import ImageButton from '../common/ImageButton'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {
  changeContactEmail,
  changeContactMessage,
  sendContactMessage,
} from './contact'
import userSession from '../user/userSession'
import {isSmallDevice} from '../common/DeviceUtility'
import openUrl from '../common/openUrl'
import {strings} from '../stringsApp'
import ProgressHud from '../common/ProgressHud'

const socialImageSize = isSmallDevice() ? 40 : Device.isTablet() ? 70 : 55
const styles = StyleSheet.create({
  email: {
    height: 45,
    backgroundColor: 'rgb(162, 162, 161)',
    padding: 5,
    fontSize: Device.isTablet() ? 24 : 16,
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontFamily: 'HelveticaNeue',
    marginBottom: 10,
  },
  message: {
    flex: 4,
    backgroundColor: 'rgb(162, 162, 161)',
    fontSize: Device.isTablet() ? 24 : 16,
    padding: 5,
    color: 'white',
    marginBottom: 15,
    fontFamily: 'HelveticaNeue',
  },
  sendButton: {
    marginBottom: 15,
    height: isSmallDevice() ? 35 : Device.isTablet() ? 70 : 40,
  },
  saveButtonTitle: {
    fontSize: Device.isTablet() ? 40 : 19,
  },
  socialButtonsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flexGrow: 1,
    marginBottom: 15,
  },
  socialButton: {
    margin: Device.isTablet() ? 30 : 15,
  },
})

class ContactScreen extends React.Component {
  static navigationOptions = {header: null}

  constructor(props) {
    super(props)

    const user = userSession.getUser()

    if (user) {
      props.changeContactEmail(user.email)
    }

    this.state = {
      showProgressDialog: false,
    }
  }

  state: {
    showProgressDialog: boolean,
  }

  props: {
    actions: any,
    changeContactEmail: Function,
    changeContactMessage: Function,
    contactEmail: string,
    contactMessage: string,
    navigation: Object,
    sendContactMessage: Function,
  }

  onFacebookIconPressed = () => {
    openUrl(
      'fb://profile/1637969713101267',
      'https://www.facebook.com/nativox.english',
    )
  }

  onTwitterIconPressed = () => {
    openUrl(
      'twitter://user?screen_name=nativox_app',
      'https://twitter.com/nativox_app',
    )
  }

  goBack = () => {
    this.props.navigation.goBack()
  }

  onPressSendContactMessage = () => {
    this.props.sendContactMessage({
      onShowProgressDialog: () => this.setState({showProgressDialog: true}),
      onDismissProgressDialog: () => this.setState({showProgressDialog: false}),
    })
  }

  render() {
    return (
      <Screen
        onPressLeftButton={this.goBack}
        showLeftNavBar
        title={strings.contactTitleScreen}>
        <ProgressHud
          isVisible={this.state.showProgressDialog}
          text={strings.sendingProgressDialogTitle}
        />
        <TextInput
          onChangeText={this.props.changeContactEmail}
          placeholder={strings.emailPlaceHolder}
          style={styles.email}
          underlineColorAndroid={'rgba(255, 255, 255, 0)'}
          value={this.props.contactEmail}
        />
        <TextInput
          blurOnSubmit
          multiline
          onChangeText={this.props.changeContactMessage}
          placeholder={strings.messagePlaceHolder}
          placeholderTextColor={'gray'}
          returnKeyLabel="Done"
          returnKeyType="done"
          style={styles.message}
          underlineColorAndroid={'rgba(255, 255, 255, 0)'}
          value={this.props.contactMessage}
        />

        <Button
          onPress={this.onPressSendContactMessage}
          style={styles.sendButton}
          title={strings.sendButtonTitle}
          titleStyle={styles.saveButtonTitle}
        />
        <View style={styles.socialButtonsContainer}>
          <ImageButton
            height={socialImageSize}
            image="facebook_share_button"
            onPress={this.onFacebookIconPressed}
            size={socialImageSize}
            style={styles.socialButton}
            width={socialImageSize}
          />
          <ImageButton
            height={socialImageSize}
            image="twitter_share_button"
            onPress={this.onTwitterIconPressed}
            style={styles.socialButton}
            width={socialImageSize}
          />
        </View>
      </Screen>
    )
  }
}

export default connect(
  state => ({
    contactEmail: state.contact.get('contactEmail'),
    contactMessage: state.contact.get('contactMessage'),
  }),
  dispatch =>
    bindActionCreators(
      {
        changeContactEmail,
        changeContactMessage,
        sendContactMessage,
      },
      dispatch,
    ),
)(ContactScreen)
