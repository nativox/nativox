/* @flow */

import immutable from 'immutable'
import {createAction, handleActions} from 'redux-actions'
import {Alert} from 'react-native'
import {post} from '../common/apiRequest'
import userSession from '../user/userSession'
import {strings} from '../stringsApp'
import showAlertMessage from '../common/showAlertMessage'

export const SET_CONTACT_MESSAGE = 'SET_CONTACT_MESSAGE'
export const SET_CONTACT_EMAIL = 'SET_CONTACT_EMAIL'
export const START_SENDING_CONTACT_MESSAGE = 'START_SENDING_CONTACT_MESSAGE'
export const FINISH_SENDING_CONTACT_MESSAGE = 'FINISH_SENDING_CONTACT_MESSAGE'

const setContactMessage = createAction(SET_CONTACT_MESSAGE)
const setContactEmail = createAction(SET_CONTACT_EMAIL)
const startSendingContactMessage = createAction(START_SENDING_CONTACT_MESSAGE)
const finishSendingContactMessage = createAction(FINISH_SENDING_CONTACT_MESSAGE)

export const changeContactMessage = (message: string) => {
  return (dispatch: Function) => {
    dispatch(setContactMessage(message))
  }
}

export const changeContactEmail = (email: string) => {
  return (dispatch: Function) => {
    dispatch(setContactEmail(email))
  }
}

export const sendContactMessage = ({
  onShowProgressDialog,
  onDismissProgressDialog,
}: {
  onShowProgressDialog: Function,
  onDismissProgressDialog: Function,
}) => {
  return (dispatch: Function, getState: Function) => {
    const email = getState().contact.get('contactEmail')
    const message = getState().contact.get('contactMessage')

    if (!message || !message.length) {
      Alert.alert('Nativox', strings.error_contact_no_message)
      return
    }
    const user = userSession ? userSession.getUser() : {}
    const params = {
      mensaje: message,
      correo: email,
      id_usuario: user ? user.idUser : '',
    }

    dispatch(startSendingContactMessage())
    onShowProgressDialog()
    post('contacto', params)
      .then((response: Object) => {
        onDismissProgressDialog()
        dispatch(finishSendingContactMessage())
        if (response.status === 'true') {
          dispatch(setContactMessage(''))
          
          showAlertMessage(strings.contact_message_success)

          return
        }

        showAlertMessage(strings.error_contact_fail)        
      })
      .catch(() => {
        onDismissProgressDialog()
        dispatch(finishSendingContactMessage())
        
        showAlertMessage(strings.error_contact_fail)        
      })
  }
}

export default handleActions(
  {
    [SET_CONTACT_MESSAGE]: (state, action) =>
      state.set('contactMessage', action.payload),
    [SET_CONTACT_EMAIL]: (state, action) =>
      state.set('contactEmail', action.payload),
    [START_SENDING_CONTACT_MESSAGE]: state =>
      state.set('isSendingContactMessage', true),
    [FINISH_SENDING_CONTACT_MESSAGE]: state =>
      state.set('isSendingContactMessage', false),
  },
  immutable.fromJS({
    contactEmail: '',
    contactMessage: '',
    isSendingContactMessage: false,
  }),
)
