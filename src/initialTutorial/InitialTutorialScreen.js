/* @flow */

import React from 'react'
import {AsyncStorage, View, Text, StyleSheet, Image} from 'react-native'
import Screen from '../common/Screen'
import Carousel from 'react-native-carousel'
import {PRIMARY_COLOR} from '../common/NativoxTheme.js'
import Button from '../common/Button'
import {strings} from '../stringsApp'
import Device from 'react-native-device-info'

const styles: Object = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerPage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  descriptionContainer: {
    flex: 2,
  },
  description: {
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isTablet() ? 28 : 16,
    textAlign: 'center',
    fontFamily: 'HelveticaNeue-Light',
  },
  startButton: {
    marginTop: 10,
    marginBottom: 10,
  },
})

const pages: Array<Object> = [
  {
    image: 'welcome_first_tutorial',
    description: strings.text_first_page_welcome_tutorial,
  },
  {
    image: 'entonation_first_tutorial',
    description: strings.text_second_page_welcome_tutorial,
  },
  {
    image: 'beats_first_tutorial',
    description: strings.text_third_page_welcome_tutorial,
  },
  {
    image: 'melody_first_tutorial',
    description: strings.text_forth_page_welcome_tutorial,
  },
  {
    image: 'visualize_first_tutorial',
    description: strings.text_fifth_page_welcome_tutorial,
  },
]

class InitialTutorialScreen extends React.Component {
  static navigationOptions = {header: null}

  state = {
    width: 0,
    height: 0,
    activePage: 0,
  }

  props: {
    navigation: Object,
  }

  // $FlowIgnore
  componentDidMount = async () => {
    try {
      await AsyncStorage.setItem('hasSeenInitialTutorial', 'true')
    } catch (error) {
      alert(error.message)
    }
  }

  onRenderContainerLayout = (event: Object) => {
    const layout = event.nativeEvent.layout

    this.setState({
      width: layout.width,
      height: layout.height,
      activePage: 0,
    })
  }

  onPressNextPage = () => {
    if (this.state.activePage === 4) {
      this.props.navigation.navigate('Login')
    } else {
      this.refs.carousel.indicatorPressed(this.state.activePage + 1) // eslint-disable-line react/no-string-refs
    }
  }

  render() {
    return (
      <Screen>
        <View onLayout={this.onRenderContainerLayout} style={styles.container}>
          <Carousel
            animate={false}
            indicatorAtBottom
            indicatorColor={PRIMARY_COLOR}
            indicatorOffset={10}
            indicatorSize={20}
            onPageChange={activePage => this.setState({activePage})}
            ref="carousel" // eslint-disable-line react/no-string-refs
            width={this.state.width}>
            {pages.map((page, index) =>
              <View
                key={index}
                style={[styles.containerPage, {width: this.state.width}]}>
                <Image
                  resizeMode='contain'
                  source={{uri: page.image, isStatic: true}}
                  style={{width: this.state.width, height: this.state.height * 0.5}}
                />
                <View style={styles.descriptionContainer}>
                  <Text style={styles.description}>
                    {page.description}
                  </Text>
                </View>
              </View>,
            )}
          </Carousel>
          <Button
            onPress={this.onPressNextPage}
            style={styles.startButton}
            title={
              this.state.activePage === 4
                ? strings.start_label
                : strings.next_label
            }
            width={Device.isTablet() ? 200 : 120}
          />
        </View>
      </Screen>
    )
  }
}

export default InitialTutorialScreen
