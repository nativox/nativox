/* @flow */

import React from 'react'
import {View, StyleSheet} from 'react-native'
import ImageButton from '../common/ImageButton'
import Device from 'react-native-device-info'

const styles = StyleSheet.create({
  container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
})

const ArrowButton = ({onPress, image}: {onPress: Function, image: string}) =>
  <View style={styles.container}>
    <ImageButton
      height={Device.isTablet() ? 115 : 65}
      image={image}
      onPress={onPress}
      resizeMode={'contain'}
      width={Device.isTablet() ? 150 : 100}
    />
  </View>

export default ArrowButton
