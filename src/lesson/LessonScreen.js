/* @flow */

import React from 'react'
import {
  AsyncStorage,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  WebView,
} from 'react-native'
import Screen from '../common/Screen'
import Orientation from 'react-native-orientation'
import Device from 'react-native-device-info'
import {connect} from 'react-redux'
import TappableIcon from '../common/TappableIcon'
import ImageButton from '../common/ImageButton'
import ModalScreen from '../common/ModalScreen'
import SubtitlesButton from './SubtitlesButton'
import ArrowButton from './ArrowButton'
import UserFeedback from './UserFeedback'
import ActivityIndicatorView from '../common/ActivityIndicatorView'
import Video from 'react-native-video'
import {Answers} from 'react-native-fabric'
import {
  onRecordButtonPressed,
  onPlayButtonPressed,
  onPreviousButtonPressed,
  onNextButtonPressed,
  onVideoFinished,
  onStartPreparingRecordVideo,
  onFinishPreparingRecordVideo,
  onCourseUnloaded,
  onSubtitlesButtonPressed,
  onTestButtonPressed,
  onTranslationButtonPressed,
  onListeningGameButtonPressed,
} from './eventHandlers'
import {startRecordingVideo} from './actionCreators'
import {HOST_NAME} from '../common/apiRequest'
import userSession from '../user/userSession'
import packageData from '../../package.json'
import CountdownCircle from 'react-native-countdown-circle'
import {PRIMARY_COLOR} from '../common/NativoxTheme'
import ListeningGame from './ListeningGame'
import StatisticsScreen from './StatisticsScreen'
import Permissions from 'react-native-permissions'
import AlarmReminder from '../settings/AlarmReminder'
import {isSmallDevice} from '../common/DeviceUtility'

const styles: Object = StyleSheet.create({
  screenContainer: {flex: 1},
  container: {
    flexDirection: 'row',
    flex: 11,
  },
  buttonsContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: Device.isTablet() ? 170 : 110,
    height: Device.isTablet() ? 300 : 240,
  },
  video: {
    flex: 1,
  },
  videoButtonsContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 5,
  },
  barButtonsContainer: {
    flex: 2,
    alignItems: 'center',
  },
  pointsContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'black',
    borderRadius: 7,
  },
  countDownContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  uploadingIndicator: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    flex: 1,
  },
  line: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 3,
    backgroundColor: PRIMARY_COLOR,
  },
  pointResultViewer: {
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isTablet() ? 25 : 18,
    textAlign: 'center',
  },
  lineWebView: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  contextInfoContainer: {
    flex: 14,
    backgroundColor: 'rgb(0, 0, 0)',
    borderRadius: 7,
    padding: 5,
  },
  contextInfoContainerWrapper: {flex: 3, flexDirection: 'row'},
  iconContainer: {
    backgroundColor: 'rgb(0, 0, 0)',
    padding: 3,
    margin: 5,
  },
  barButtonContainer: {margin: 5, marginBottom: 8},
  textStats: {
    color: 'rgb(255, 255, 255)',
    textAlign: 'center',
    padding: 2,
    fontWeight: 'bold',
    fontSize: Device.isTablet() ? 20 : 17,
  },
  bigVideoContainer: {flex: 15},
  spacerContainer: {flex: 2},
  tappableIcon: {textAlign: 'center'},
  lessonButton: {margin: 2},
})

const PointLine = ({
  selectedVideo,
  totalWidth,
}: {
  selectedVideo: Object,
  totalWidth: number,
}) => (
  <View
    style={[
      styles.line,
      {
        width:
          selectedVideo && selectedVideo.puntuacion
            ? totalWidth * (selectedVideo.puntuacion / 100)
            : 0,
      },
    ]}
  />
)

const PointsResultViewer = ({
  playingVideo,
  testingVideo,
  audioPoints,
  pointsMessage,
}: {
  playingVideo: boolean,
  testingVideo: boolean,
  audioPoints: number,
  pointsMessage: string,
}) => {
  if (playingVideo || testingVideo || !audioPoints) {
    return null
  }

  return (
    <View style={styles.pointsContainer}>
      <Text style={styles.pointResultViewer}>
        {audioPoints}% {pointsMessage}
      </Text>
    </View>
  )
}

const TranslationViewer = ({
  showTranslation,
  message,
}: {
  showTranslation: boolean,
  message: string,
}) => {
  if (!showTranslation) {
    return null
  }

  return (
    <View style={styles.pointsContainer}>
      <Text style={styles.pointResultViewer}>{message}</Text>
    </View>
  )
}

const CountDownContainer = ({
  isPreparingRecord,
  onRecordPressed,
}: {
  isPreparingRecord: boolean,
  onRecordPressed: Function,
}) => {
  if (!isPreparingRecord) {
    return null
  }

  return (
    <View style={styles.countDownContainer}>
      <CountdownCircle
        bgColor="rgb(255, 255, 255)"
        borderWidth={8}
        color={PRIMARY_COLOR}
        onTimeElapsed={onRecordPressed}
        radius={Device.isTablet() ? 70 : 50}
        seconds={3}
        textStyle={{fontSize: Device.isTablet() ? 30 : 23}}
      />
    </View>
  )
}

const getVideoLineUrl = (video: Object, width: number, height: number) => {
  if (!video) {
    return ''
  }

  const user = userSession ? userSession.getUser() || {} : {}
  const language = user.language ? user.language : ''
  const url = `${HOST_NAME}video_linea?id_video=${video.id_video}&id_usuario=${user.idUser}&token=${user.token}&width=${width}&height=${height}&lang=${language}&os=${Platform.OS}&version=${packageData.version}`

  return url
}

const injectScript = `
  window.receivedMessageFromReactNative = function (message) {
    if (message === "play_canvas") {
      restart_linea();
    }
  };
`

const imageButtonStyle = {
  height: Device.isTablet() ? 115 : isSmallDevice() ? 55 : 65,
  width: Device.isTablet() ? 170 : isSmallDevice() ? 90 : 100,
}

const barButtonSize = Device.isTablet() ? 40 : 30

class BarButtonContainer extends React.PureComponent {
  render() {
    return <View style={styles.barButtonContainer}>{this.props.children}</View>
  }
}

class LessonScreen extends React.PureComponent {
  static navigationOptions = {header: null}

  props: {
    audioPoints: number,
    course: Object,
    loadingVideo: boolean,
    navigation: Object,
    onCourseUnloaded: Function,
    onFinishPreparingRecordVideo: Function,
    onRecordButtonPressed: Function,
    onStartPreparingRecordVideo: Function,
    onStartPreparingTestVideo: Function,
    onFinishPreparingTestVideo: Function,
    onPlayButtonPressed: Function,
    onPreviousButtonPressed: Function,
    onNextButtonPressed: Function,
    onVideoFinished: Function,
    recordingVideo: boolean,
    playingVideo: boolean,
    testingVideo: boolean,
    pointsMessage: string,
    selectedVideo: Object,
    isPlayButtonEnabled: boolean,
    isRecordButtonEnabled: boolean,
    isTestButtonEnabled: boolean,
    isTranslationButtonEnabled: boolean,
    isPreparingRecord: boolean,
    isPreparingTest: boolean,
    uploadingAudio: boolean,
    videoVolume: number,
    showSubtitles: boolean,
    openedSubtitles: boolean,
    onSubtitlesButtonPressed: Function,
    onTestButtonPressed: Function,
    onTranslationButtonPressed: Function,
    showTranslation: boolean,
    showListeningGame: boolean,
    numberPlays: number,
    onListeningGameButtonPressed: Function,
    onStatisticsButtonPressed: Function,
  }

  webviewbridge: any
  player: any

  state = {
    lineWebViewWidth: 0,
    lineWebViewHeight: 0,
    isGraphicLineLoaded: false,
    shouldDisplayFeedbackModal: false,
    showProgressFeedback: false,
    shouldDisplayListeningGame: false,
    shouldDisplayStatistics: false,
    shouldShowAlarmReminder: false,
  }

  componentWillMount = () => {
    Orientation.lockToLandscape()
  }

  componentDidMount = () => {
    Permissions.check('microphone').then((response: string) => {
      if (response !== 'authorized') {
        Permissions.request('microphone')
      }
    })
  }

  componentWillUnmount = () => {
    if (!Device.isTablet()) {
      Orientation.lockToPortrait()
    } else {
      Orientation.unlockAllOrientations()
    }
  }

  goBack = async () => {
    const alarmReminder = await AsyncStorage.getItem('alarmReminder')

    if (!alarmReminder) {
      this.setState({
        shouldShowAlarmReminder: true,
      })

      return
    }

    this.props.onCourseUnloaded()
    this.props.navigation.goBack(null)
  }

  onPressDrawerButton = () => this.props.navigation.navigate('DrawerOpen')

  onVideoEnd = async () => {
    this.props.onVideoFinished()
    setTimeout(() => {
      if (this.player) {
        this.player.seek(0)
      }
    }, 500)

    if (this.props.testingVideo) {
      const numberPlays = parseInt(
        (await AsyncStorage.getItem(
          `num_plays_${this.props.selectedVideo.id_curso}`
        )) || '0'
      )

      AsyncStorage.setItem(
        `num_plays_${this.props.selectedVideo.id_curso}`,
        `${numberPlays + 1}`
      )

      if (numberPlays === 6) {
        this.setState({
          shouldDisplayFeedbackModal: true,
        })
      }
    }
  }

  onPressPreviousButton = () => {
    this.props.navigation.navigate('DrawerOpen')
    this.props.onPreviousButtonPressed()
  }

  onPressNextButton = () => {
    this.props.navigation.navigate('DrawerOpen')
    this.props.onNextButtonPressed()
  }

  onPlayPressed = () => {
    this.webviewbridge.injectJavaScript(
      'receivedMessageFromReactNative("play_canvas")'
    )

    this.props.onPlayButtonPressed()
  }

  onRecordPressed = () => {
    this.props.onFinishPreparingRecordVideo()
    this.props.onRecordButtonPressed(() => {
      this.webviewbridge.injectJavaScript(
        'receivedMessageFromReactNative("play_canvas")'
      )
      this.props.startRecordingVideo()
    })
  }

  onLayoutLineWebView = (event: Object) => {
    const {width, height} = event.nativeEvent.layout

    this.setState({
      lineWebViewWidth: width,
      lineWebViewHeight: height,
    })
  }

  onLoadGraphicLine = () => {
    this.setState({isGraphicLineLoaded: true})
  }

  onTestButtonPressed = async () => {
    this.props.onTestButtonPressed(() => {
      if (this.player) {
        this.player.seek(0)
      }
      this.webviewbridge.injectJavaScript(
        'receivedMessageFromReactNative("play_canvas")'
      )
    })
  }

  onListeningGameButtonPressed = () => {
    this.setState({
      shouldDisplayListeningGame: !this.state.shouldDisplayListeningGame,
    })
  }

  onCloseAlarmReminder = () => {
    this.setState({
      shouldShowAlarmReminder: false,
    })
    this.props.onCourseUnloaded()
    this.props.navigation.goBack(null)
  }

  onCloseButtonPressedFeedbackModal = () => {
    this.setState({shouldDisplayFeedbackModal: false})
  }

  onCloseButtonPressedStatistics = () => {
    this.setState({shouldDisplayStatistics: false})
  }

  onCloseButtonPressedListeningGame = () => {
    this.setState({shouldDisplayListeningGame: false})
  }

  onPressStatisticsButton = () => {
    this.setState({shouldDisplayStatistics: true})
    const user = userSession.getUser()
    if (user) {
      Answers.logCustom('statistics_button_pressed', {
        name: user.name,
        idUser: user.idUser,
      })
    }
  }

  onLoadVideoPlayer = () => {
    if (this.player) {
      this.player.seek(0)
    }
  }

  render = () => {
    return (
      <Screen fullscreen>
        <ModalScreen
          height={220}
          isVisible={this.state.shouldDisplayFeedbackModal}
          onCloseButtonPressed={this.onCloseButtonPressedFeedbackModal}
          onTouchBackground={this.onCloseButtonPressedFeedbackModal}
          supportedOrientations={['landscape']}
          width={350}
        >
          <UserFeedback
            onCompleteFeedbackSend={this.onCloseButtonPressedFeedbackModal}
          />
        </ModalScreen>
        <ModalScreen
          height={240}
          isVisible={this.state.shouldDisplayStatistics}
          onCloseButtonPressed={this.onCloseButtonPressedStatistics}
          onTouchBackground={this.onCloseButtonPressedStatistics}
          supportedOrientations={['landscape']}
          width={440}
        >
          <StatisticsScreen course={this.props.course || {}} />
        </ModalScreen>
        <ModalScreen
          fullscreen
          height={300}
          isVisible={this.state.shouldDisplayListeningGame}
          onCloseButtonPressed={this.onCloseButtonPressedListeningGame}
          onTouchBackground={this.onCloseButtonPressedListeningGame}
          supportedOrientations={['landscape']}
          width={440}
        >
          <ListeningGame
            onFinishGame={this.onCloseButtonPressedListeningGame}
            onPressPlay={this.onPlayPressed}
            sentence={
              this.props.selectedVideo ? this.props.selectedVideo.frase : ''
            }
          />
        </ModalScreen>
        <ModalScreen
          height={340}
          isVisible={this.state.shouldShowAlarmReminder}
          onCloseButtonPressed={this.onCloseAlarmReminder}
          onTouchBackground={this.onCloseAlarmReminder}
          width={300}
        >
          <AlarmReminder
            onCancelAlarms={this.onCloseAlarmReminder}
            onSetAlarms={this.onCloseAlarmReminder}
          />
        </ModalScreen>
        <View style={styles.screenContainer}>
          <View style={styles.container}>
            <View style={styles.barButtonsContainer}>
              <BarButtonContainer>
                <TappableIcon
                  name={'bars'}
                  onPress={this.onPressDrawerButton}
                  size={barButtonSize}
                  style={styles.tappableIcon}
                />
              </BarButtonContainer>
              <BarButtonContainer>
                <TappableIcon
                  name={'chevron-left'}
                  onPress={this.goBack}
                  size={barButtonSize}
                  style={styles.tappableIcon}
                />
              </BarButtonContainer>
              <BarButtonContainer>
                <TappableIcon
                  name={'bar-chart'}
                  onPress={this.onPressStatisticsButton}
                  size={barButtonSize}
                  style={styles.tappableIcon}
                />
                <Text style={styles.textStats}>
                  {`${this.props.course.stats_curso
                    ? parseInt(
                        this.props.course.stats_curso.porcentaje_completado
                      )
                    : 0}%`}
                </Text>
              </BarButtonContainer>
            </View>
            <TouchableOpacity
              activeOpacity={1}
              onPress={
                this.state.isGraphicLineLoaded &&
                !this.props.loadingVideo &&
                !this.props.playingVideo &&
                !this.props.recordingVideo
                  ? this.onPlayPressed
                  : null
              }
              style={styles.bigVideoContainer}
            >
              {this.props.selectedVideo &&
              this.props.selectedVideo.videoPath &&
              !this.props.loadingVideo ? (
                <Video
                  onEnd={this.onVideoEnd}
                  onLoad={this.onLoadVideoPlayer}
                  paused={
                    !(
                      this.props.playingVideo ||
                      this.props.recordingVideo ||
                      this.props.testingVideo
                    )
                  }
                  rate={1.0}
                  ref={ref => {
                    this.player = ref
                  }}
                  resizeMode={'cover'}
                  source={{uri: this.props.selectedVideo.videoPath}}
                  style={styles.video}
                  volume={this.props.videoVolume}
                />
              ) : (
                <ActivityIndicatorView />
              )}
              <View style={styles.videoButtonsContainer}>
                {this.props.showListeningGame ? (
                  <View style={styles.iconContainer}>
                    <TappableIcon
                      name={'keyboard-o'}
                      onPress={this.onListeningGameButtonPressed}
                      setIcon={'ionicons'}
                      size={Device.isTablet() ? 40 : 27}
                    />
                  </View>
                ) : null}
                {this.props.showSubtitles ? (
                  <SubtitlesButton
                    onPressSubtitlesButton={this.props.onSubtitlesButtonPressed}
                    openedSubtitles={this.props.openedSubtitles}
                    selectedVideo={this.props.selectedVideo}
                  />
                ) : null}
              </View>
              <PointLine
                selectedVideo={this.props.selectedVideo}
                totalWidth={this.state.lineWebViewWidth}
              />
            </TouchableOpacity>
            <View style={styles.spacerContainer} />
            <View style={styles.buttonsContainer}>
              <ImageButton
                disabled={
                  !this.state.isGraphicLineLoaded ||
                  !this.props.isPlayButtonEnabled
                }
                height={imageButtonStyle.height}
                image={'play_button'}
                onPress={this.onPlayPressed}
                resizeMode={'contain'}
                style={styles.lessonButton}
                width={imageButtonStyle.width}
              />
              <ImageButton
                disabled={
                  !this.state.isGraphicLineLoaded ||
                  !this.props.isRecordButtonEnabled
                }
                height={imageButtonStyle.height}
                image={'record_button'}
                onPress={this.props.onStartPreparingRecordVideo}
                resizeMode={'contain'}
                style={styles.lessonButton}
                width={imageButtonStyle.width}
              />
              <ImageButton
                disabled={
                  !this.state.isGraphicLineLoaded ||
                  !this.props.isTestButtonEnabled
                }
                height={imageButtonStyle.height}
                image={'test_button'}
                onPress={this.onTestButtonPressed}
                resizeMode={'contain'}
                style={styles.lessonButton}
                width={imageButtonStyle.width}
              />
              <ImageButton
                disabled={
                  !this.state.isGraphicLineLoaded ||
                  !this.props.isTranslationButtonEnabled
                }
                height={imageButtonStyle.height}
                image={'translation_button'}
                onPress={this.props.onTranslationButtonPressed}
                resizeMode={'contain'}
                style={styles.lessonButton}
                width={imageButtonStyle.width}
              />
            </View>
          </View>
          <View style={styles.contextInfoContainerWrapper}>
            <ArrowButton
              image={'video_button_izquierda'}
              onPress={this.onPressPreviousButton}
            />
            <View
              onLayout={this.onLayoutLineWebView}
              style={styles.contextInfoContainer}
            >
              <WebView
                injectedJavaScript={injectScript}
                onLoadEnd={this.onLoadGraphicLine}
                ref={ref => {
                  this.webviewbridge = ref
                }}
                renderLoading={() => <ActivityIndicatorView />}
                source={{
                  uri: getVideoLineUrl(
                    this.props.selectedVideo,
                    this.state.lineWebViewWidth,
                    this.state.lineWebViewHeight
                  ),
                }}
                startInLoadingState
                style={styles.lineWebView}
              />
              <PointsResultViewer
                audioPoints={this.props.audioPoints}
                playingVideo={this.props.playingVideo}
                pointsMessage={this.props.pointsMessage}
                testingVideo={this.props.testingVideo}
              />
              <TranslationViewer
                message={
                  this.props.selectedVideo &&
                  this.props.selectedVideo.traduccion
                    ? this.props.selectedVideo.traduccion
                    : 'Translation not available'
                }
                showTranslation={this.props.showTranslation}
              />
              {this.props.uploadingAudio ? (
                <View style={styles.uploadingIndicator}>
                  <ActivityIndicatorView />
                </View>
              ) : null}
            </View>
            <ArrowButton
              image={'video_button_derecha'}
              onPress={this.onPressNextButton}
            />
          </View>
        </View>
        <CountDownContainer
          isPreparingRecord={this.props.isPreparingRecord}
          onRecordPressed={this.onRecordPressed}
        />
      </Screen>
    )
  }
}

export default connect(
  state => ({
    course: state.lesson.course,
    selectedVideo: state.lesson.selectedVideo,
    loadingVideo: state.lesson.loadingVideo,
    recordingVideo: state.lesson.recordingVideo,
    playingVideo: state.lesson.playingVideo,
    testingVideo: state.lesson.testingVideo,
    isPlayButtonEnabled: state.lesson.isPlayButtonEnabled,
    isRecordButtonEnabled: state.lesson.isRecordButtonEnabled,
    isTestButtonEnabled: state.lesson.isTestButtonEnabled,
    isTranslationButtonEnabled: state.lesson.isTranslationButtonEnabled,
    isPreparingRecord: state.lesson.isPreparingRecord,
    isPreparingTest: state.lesson.isPreparingTest,
    uploadingAudio: state.lesson.uploadingAudio,
    videoVolume: state.lesson.videoVolume,
    audioPoints: state.lesson.audioEvaluation.audioPoints,
    pointsMessage: state.lesson.audioEvaluation.pointsMessage,
    showSubtitles: state.lesson.showSubtitles,
    openedSubtitles: state.lesson.openedSubtitles,
    showTranslation: state.lesson.showTranslation,
    numberPlays: state.lesson.numberPlays,
    showListeningGame: state.lesson.showListeningGame,
    showStatistics: state.lesson.showStatistics,
  }),
  dispatch => ({
    onNextButtonPressed: () => dispatch(onNextButtonPressed()),
    onPlayButtonPressed: () => dispatch(onPlayButtonPressed()),
    onRecordButtonPressed: callback =>
      dispatch(onRecordButtonPressed(callback)),
    startRecordingVideo: () => dispatch(startRecordingVideo()),
    onPreviousButtonPressed: () => dispatch(onPreviousButtonPressed()),
    onVideoFinished: () => dispatch(onVideoFinished()),
    onStartPreparingRecordVideo: () => dispatch(onStartPreparingRecordVideo()),
    onFinishPreparingRecordVideo: () =>
      dispatch(onFinishPreparingRecordVideo()),
    onCourseUnloaded: () => dispatch(onCourseUnloaded()),
    onSubtitlesButtonPressed: () => dispatch(onSubtitlesButtonPressed()),
    onTestButtonPressed: callback => dispatch(onTestButtonPressed(callback)),
    onTranslationButtonPressed: () => dispatch(onTranslationButtonPressed()),
    onListeningGameButtonPressed: () =>
      dispatch(onListeningGameButtonPressed()),
  })
)(LessonScreen)
