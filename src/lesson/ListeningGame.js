/* @flow */

import React, {Component} from 'react'
import {Animated, StyleSheet, View, Text, TouchableOpacity} from 'react-native'
import TappableIcon from '../common/TappableIcon'
import {strings} from '../stringsApp'

const styles: Object = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  keyLetterContainer: {
    backgroundColor: 'rgb(43, 152, 215)',
    height: 33,
    width: 33,
    margin: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

const KeyLetterButton = ({
  letter,
  onPress,
}: {
  letter: string,
  onPress: Function,
}) =>
  <TouchableOpacity
    onPress={() => onPress(letter.toLowerCase())}
    style={styles.keyLetterContainer}>
    <Text style={{color: 'rgb(255, 255, 255)', fontSize: 16}}>{letter}</Text>
  </TouchableOpacity>

const CHARACTERS_TO_GUESS = [
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'l',
  'm',
  'n',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'y',
  'w',
  'x',
  'z',
]

class GuessLetter extends Component {
  state = {
    fadeAnim: new Animated.Value(0),
  }

  props: {
    animated: boolean,
    guessed: boolean,
    letter: string,
  }

  componentDidMount = () => {
    if (this.props.animated) {
      this.startAnimation()
    }
  }

  componentDidUpdate = prevProps => {
    if (prevProps.animated !== this.props.animated) {
      this.startAnimation()
    }
  }

  startAnimation = () => {
    Animated.timing(this.state.fadeAnim, {
      toValue: this.state.fadeAnim._value === 1 ? 0 : 1,
      duration: 1000,
    }).start(() => {
      this.startAnimation()
    })
  }

  render() {
    return (
      <View>
        {!this.props.guessed
          ? <View
              style={{
                borderBottomWidth: 1,
                borderBottomColor: 'rgb(255, 255, 255)',
                height: 20,
                width: 11,
              }}>
              {this.props.animated
                ? <Animated.View
                    style={{
                      backgroundColor: 'rgb(255, 255, 255)',
                      opacity: this.state.fadeAnim,
                      flex: 1,
                    }}
                  />
                : null}
            </View>
          : <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 20,
                width: 11,
              }}>
              <Text
                style={{
                  color: 'rgb(255, 255, 255)',
                }}>
                {this.props.letter}
              </Text>
            </View>}
      </View>
    )
  }
}

const GuessWord = ({word}: {word: Object}) =>
  <View style={{flexDirection: 'row', margin: 5}}>
    {word.characters.map((character, index) =>
      <GuessLetter
        animated={character.current}
        guessed={character.guessed}
        key={index}
        letter={character.value}
      />,
    )}
  </View>

const GuessSentence = ({words}: {words: Array<Object>}) =>
  <View
    style={{
      flexDirection: 'row',
      flexWrap: 'wrap',
      padding: 5,
      alignItems: 'center',
    }}>
    {words.map((word, index) => <GuessWord key={index} word={word} />)}
  </View>

const KeyboardRow = ({
  keyLetters,
  onTryGuessCharacter,
}: {
  keyLetters: Array<string>,
  onTryGuessCharacter: Function,
}) =>
  <View
    style={{
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    {keyLetters.map((letter: string, index: number) =>
      <KeyLetterButton
        key={index}
        letter={letter}
        onPress={onTryGuessCharacter}
      />,
    )}
  </View>

export default class ListeningGame extends React.Component {
  props: {
    sentence: string,
    onFinishGame: Function,
    onPressPlay: Function,
  }

  state: {
    showFailure: boolean,
    showSuccess: boolean,
    indexWord: number,
    indexCharacter: number,
    words: Array<Object>,
  }

  constructor(props: Object) {
    super()

    this.state = {
      showFailure: false,
      showSuccess: false,
      indexWord: 0,
      indexCharacter: 0,
      words: props.sentence
        .split(' ')
        .map((word: string, indexWord: number) => ({
          characters: word
            .split('')
            .map((letter: string, indexCharacter: number) => ({
              value: letter,
              guessed: CHARACTERS_TO_GUESS.indexOf(letter.toLowerCase()) === -1,
              current: indexWord === 0 && indexCharacter === 0,
            })),
        })),
    }
  }

  onTryGuessCharacter = (character: string) => {
    const currentCharacter = this.state.words[this.state.indexWord].characters[
      this.state.indexCharacter
    ]

    const guessed = currentCharacter.value.toLowerCase() === character

    if (!guessed) {
      this.setState({showFailure: true})

      return
    }

    this.setState({showFailure: false})

    const newWords = this.state.words

    newWords[this.state.indexWord].characters[
      this.state.indexCharacter
    ].guessed = true
    newWords[this.state.indexWord].characters[
      this.state.indexCharacter
    ].current = false

    let nextIndexWord = this.state.indexWord
    let nextIndexCharacter = this.state.indexCharacter

    if (
      this.state.indexCharacter + 1 <
      newWords[this.state.indexWord].characters.length
    ) {
      nextIndexCharacter = nextIndexCharacter + 1
    } else if (this.state.indexWord + 1 < newWords.length) {
      nextIndexWord = nextIndexWord + 1
      nextIndexCharacter = 0
    } else {
      this.setState({showSuccess: true})

      setTimeout(() => this.props.onFinishGame(), 2000)

      return
    }

    if (
      !newWords[nextIndexWord] ||
      !newWords[nextIndexWord].characters[nextIndexCharacter]
    ) {
      return
    }

    newWords[nextIndexWord].characters[nextIndexCharacter].current = true

    this.setState({
      indexWord: nextIndexWord,
      indexCharacter: nextIndexCharacter,
      words: newWords,
    })

    if (
      CHARACTERS_TO_GUESS.indexOf(
        newWords[nextIndexWord].characters[
          nextIndexCharacter
        ].value.toLowerCase(),
      ) === -1
    ) {
      setTimeout(
        () =>
          this.onTryGuessCharacter(
            newWords[nextIndexWord].characters[
              nextIndexCharacter
            ].value.toLowerCase(),
          ),
        500,
      )
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgb(0, 0, 0)',
            alignItems: 'center',
          }}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text style={{color: 'rgb(255, 255, 255)', fontSize: 19}}>
              {strings.listening_game_label}
            </Text>
            <View style={{marginLeft: 10}}>
              <TappableIcon
                name={'volume-down'}
                onPress={this.props.onPressPlay}
                size={27}
              />
            </View>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            {this.state.showFailure
              ? <Text style={{color: 'rgb(255, 26, 26)', fontSize: 15}}>
                  {strings.listening_game_incorrect_letter}
                </Text>
              : null}
            {this.state.showSuccess
              ? <Text style={{color: 'rgb(0, 218, 64)', fontSize: 15}}>
                  {strings.listening_game_all_guessed}
                </Text>
              : null}
          </View>
          <View
            style={{
              flex: 3,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <GuessSentence words={this.state.words} />
          </View>
        </View>
        <View style={{backgroundColor: 'rgb(152,152,152)', padding: 2}}>
          <KeyboardRow
            keyLetters={['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P']}
            onTryGuessCharacter={this.onTryGuessCharacter}
          />
          <KeyboardRow
            keyLetters={['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L']}
            onTryGuessCharacter={this.onTryGuessCharacter}
          />
          <KeyboardRow
            keyLetters={['Z', 'X', 'C', 'V', 'B', 'N', 'M']}
            onTryGuessCharacter={this.onTryGuessCharacter}
          />
        </View>
      </View>
    )
  }
}
