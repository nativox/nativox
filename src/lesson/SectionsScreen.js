/* @flow */

import Icon from 'react-native-vector-icons/FontAwesome'
import React from 'react'
import {StyleSheet, Text, View} from 'react-native'
import {connect} from 'react-redux'
import TreeView from './TreeView'
import {PRIMARY_COLOR} from '../common/NativoxTheme'
import {onVideoSelected} from './eventHandlers'

const styles = StyleSheet.create({
  level0: {backgroundColor: PRIMARY_COLOR, padding: 7, margin: 3}, // eslint-disable-line react-native/no-unused-styles
  /* eslint-disable react-native/no-unused-styles */
  level1: {
    backgroundColor: 'gray',
    padding: 7,
    marginTop: 3,
    marginBottom: 3,
    marginLeft: 10,
    marginRight: 3,
  },
  /* eslint-enable react-native/no-unused-styles */
  videoContainer: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    marginTop: 3,
    marginBottom: 3,
    marginLeft: 10,
    marginRight: 3,
  },
})

const VideoRow = ({
  title,
  percentageCompleted,
  superado,
  isSelected,
}: {
  title: string,
  percentageCompleted: string,
  superado: boolean,
  isSelected: boolean,
}) =>
  <View style={styles.videoContainer}>
    {isSelected
      ? <View
          style={{
            backgroundColor: 'rgb(255, 203, 12)',
            width: 5,
            height: 35,
            marginRight: 5,
          }}
        />
      : null}
    <Text style={{color: 'rgb(255, 255, 255)', fontSize: 16, flex: 6}}>
      {title}
    </Text>
    <Icon
      color={superado ? PRIMARY_COLOR : 'rgb(128,128,128)'}
      name={'check'}
      size={33}
      style={{flex: 1}}
    />
    <Text
      style={{
        flex: 1,
        color: 'rgb(255, 255, 255)',
        fontSize: 16,
        textAlign: 'center',
      }}>
      {percentageCompleted || 0} %
    </Text>
  </View>

class SectionsScreen extends React.Component {
  static navigationOptions = {header: null}

  props: {
    course: any,
    navigation: any,
    selectedVideo: any,
    onVideoSelected: Function,
  }

  treeview: any

  renderNode = (node: Object, level: string) => {
    let title = 'Node'

    if (node.seccion) {
      title = node.seccion
    }

    if (node.subseccion) {
      title = node.subseccion
    }

    if (node.video) {
      return (
        <VideoRow
          isSelected={
            this.props.selectedVideo &&
            this.props.selectedVideo.id_video === node.id_video
          }
          percentageCompleted={node.puntuacion}
          superado={node.superado === '1'}
          title={node.resumen}
        />
      )
    }

    return (
      <View style={styles[`level${level}`]}>
        <Text style={{color: 'rgb(255, 255, 255)', fontSize: 19}}>
          {title}
        </Text>
      </View>
    )
  }

  onNodePressed = (node: any) => {
    if (node.video && this.treeview) {
      this.props.onVideoSelected(node)
      this.treeview.contractAllNodesInTree()
      this.treeview.expandNode(node)
      this.props.navigation.navigate('DrawerClose')
    }
  }

  getChildrenName = (node: Object) => {
    if (!node) {
      return null
    }

    if (node.subseccion) {
      return 'videos'
    }

    if (node.seccion) {
      return 'subsecciones'
    }

    if (node.curso) {
      return 'secciones'
    }

    return null
  }

  componentDidMount = () => {
    setTimeout(() => {
      this.treeview.expandNode(this.props.selectedVideo)
    }, 1000)
  }

  componentDidUpdate = (prevProps: any) => {
    if (
      prevProps.selectedVideo &&
      this.props.selectedVideo &&
      prevProps.selectedVideo.id_video !== this.props.selectedVideo.id_video
    ) {
      this.treeview.contractAllNodesInTree()
      this.treeview.expandNode(this.props.selectedVideo)
    }
  }

  render = () => {
    return (
      <View style={{flex: 1, backgroundColor: 'rgb(0, 0, 0)'}}>
        <TreeView
          autoScrollToNodeId={
            this.props.selectedVideo ? this.props.selectedVideo.id_video : null
          }
          data={this.props.course}
          getChildrenName={this.getChildrenName}
          onNodePressed={this.onNodePressed}
          ref={ref => {
            this.treeview = ref
          }}
          renderNode={this.renderNode}
        />
      </View>
    )
  }
}

export default connect(
  state => ({
    course: state.lesson.course,
    selectedVideo: state.lesson.selectedVideo,
  }),
  dispatch => ({
    onVideoSelected: video => dispatch(onVideoSelected(video)),
  }),
)(SectionsScreen)
