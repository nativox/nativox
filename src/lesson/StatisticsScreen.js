/* @flow */

import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {AnimatedCircularProgress} from 'react-native-circular-progress'
import {strings} from '../stringsApp'

const styles = StyleSheet.create({
  pointsCentral: {
    backgroundColor: 'transparent',
    position: 'absolute',
    textAlign: 'center',
    color: 'rgb(255, 255, 255)',
    fontSize: 18,
    width: 100,
    top: 40,
    left: 26,
  },
  pointsLateral: {
    backgroundColor: 'transparent',
    position: 'absolute',
    textAlign: 'center',
    color: 'rgb(255, 255, 255)',
    fontSize: 16,
    width: 100,
    top: 35,
    left: 10,
  },
})
export default class StatisticsScreen extends React.PureComponent {
  props: {
    course: Object,
  }

  render() {
    return (
      <View style={{flexGrow: 1}}>
        <View
          style={{
            flexGrow: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: 'rgb(255, 255, 255)',
              fontSize: 22,
              marginBottom: 10,
            }}>
            {this.props.course.curso}
          </Text>
        </View>
        <View
          style={{
            flexGrow: 1,
            flexDirection: 'row',
            justifyContent: 'center',
          }}>
          <View style={{padding: 5}}>
            <AnimatedCircularProgress
              backgroundColor="rgb(61, 88, 117)"
              fill={parseFloat(
                this.props.course.stats_curso
                  .nota_media_ultima_per_video_y_total_videos,
              )}
              linecap={'round'}
              prefill={0}
              rotation={0}
              size={122}
              tintColor={'rgb(255, 255, 255)'}
              width={13}>
              {fill =>
                <Text style={styles.pointsLateral}>
                  {` ${Math.round(100 * fill / 100)} `}%
                  {'\n'}
                  {strings.statistics_progress_label}
                </Text>}
            </AnimatedCircularProgress>
          </View>
          <View style={{padding: 5}}>
            <AnimatedCircularProgress
              backgroundColor="rgb(61, 88, 117)"
              fill={parseFloat(
                this.props.course.stats_curso.porcentaje_completado,
              )}
              linecap={'round'}
              prefill={0}
              rotation={0}
              size={145}
              tintColor={'rgb(255, 255, 255)'}
              width={13}>
              {fill =>
                <Text style={styles.pointsCentral}>
                  {` ${Math.round(100 * fill / 100)} `}%
                  {'\n'}
                  {strings.statistics_completed_label}
                </Text>}
            </AnimatedCircularProgress>
          </View>
          <View style={{padding: 5}}>
            <AnimatedCircularProgress
              backgroundColor="rgb(61, 88, 117)"
              fill={parseFloat(
                this.props.course.stats_curso.nota_media_grabaciones,
              )}
              linecap={'round'}
              prefill={0}
              rotation={0}
              size={122}
              tintColor={'rgb(255, 255, 255)'}
              width={13}>
              {fill =>
                <Text style={styles.pointsLateral}>
                  {` ${Math.round(100 * fill / 100)} `}%
                  {'\n'}
                  {strings.statistics_average_grade_label}
                </Text>}
            </AnimatedCircularProgress>
          </View>
        </View>
      </View>
    )
  }
}
