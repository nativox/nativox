/* @flow */

import React from 'react'
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native'
import TappableIcon from '../common/TappableIcon'
import Device from 'react-native-device-info'

const styles = StyleSheet.create({
  iconContainer: {
    backgroundColor: 'rgb(0, 0, 0)',
    padding: 3,
    margin: 5,
  },
  subtitlesText: {
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isTablet() ? 25 : 17,
    backgroundColor: 'rgb(0, 0, 0)',
    padding: 3,
  },
})

const SubtitlesButton = ({
  openedSubtitles,
  onPressSubtitlesButton,
  selectedVideo,
}: {
  openedSubtitles: boolean,
  onPressSubtitlesButton: Function,
  selectedVideo: Object,
}) => {
  if (!openedSubtitles) {
    return (
      <View style={styles.iconContainer}>
        <TappableIcon
          name={'eye'}
          onPress={onPressSubtitlesButton}
          setIcon={'ionicons'}
          size={Device.isTablet() ? 40 : 27}
        />
      </View>
    )
  }

  return (
    <TouchableOpacity onPress={onPressSubtitlesButton}>
      <Text style={styles.subtitlesText}>
        {selectedVideo ? selectedVideo.frase : ''}
      </Text>
    </TouchableOpacity>
  )
}

export default SubtitlesButton
