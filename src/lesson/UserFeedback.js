/* @flow */

import React, {Component} from 'react'
import {View, TextInput, StyleSheet, Alert} from 'react-native'
import {strings} from '../stringsApp'
import Button from '../common/Button'
import {post} from '../common/apiRequest'
import Device from 'react-native-device-info'
import userSession from '../user/userSession'
import ProgressHud from '../common/ProgressHud'

const styles = StyleSheet.create({
  messageContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    marginTop: 10,
    marginBottom: 10,
  },
  button: {
    height: 35,
  },
  feedbackTextInput: {
    width: 320,
    height: 100,
    borderColor: 'gray',
    backgroundColor: 'rgb(146, 147, 148)',
    borderWidth: 1,
    padding: 10,
    fontSize: 16,
  },
})

export default class UserFeedback extends Component {
  state = {
    feedback: '',
    text: '',
    showProgressFeedback: false,
  }

  props: {
    onCompleteFeedbackSend: Function,
  }

  onSendButtonPressed = () => {
    const user = userSession ? userSession.getUser() || {} : {}
    const params = {
      mensaje: `
      ${strings.userFeedbackPlaceHolder}
      ${this.state.feedback}<br>
<br>
      Informacion del Dispositivo<br>
      ---------------------------<br>
<br>
      Modelo: ${Device.getModel()}<br>
      Nombre: ${Device.getDeviceName()}<br>
      Sistema Operativo: ${Device.getSystemName()}<br>
      Version del Sistema Operativo: ${Device.getSystemVersion()}<br>
<br>
      ---------------------------<br>
      `,
      correo: user.email,
      id_usuario: user.idUser,
    }

    this.setState({showProgressFeedback: true})
    post('contacto', params)
      .then(() => {
        this.setState({showProgressFeedback: false})
        setTimeout(() => {
          Alert.alert('Nativox', strings.successfulSendFeedback, [
            {text: 'OK', onPress: this.props.onCompleteFeedbackSend},
          ])
        }, 1000)
      })
      .catch(() => {
        this.setState({showProgressFeedback: false})
        setTimeout(() => {
          Alert.alert('Nativox', strings.successfulSendFeedback, [
            {text: 'OK', onPress: this.props.onCompleteFeedbackSend},
          ])
        }, 1000)
      })
  }

  render() {
    return (
      <View style={{flexGrow: 1}}>
        <ProgressHud
          isVisible={this.state.showProgressFeedback}
          supportedOrientations={['landscape']}
          text={strings.feedback_progress_send}
        />
        <View style={styles.messageContainer}>
          <TextInput
            multiline
            onChangeText={feedback => this.setState({feedback})}
            placeholder={strings.userFeedbackPlaceHolder}
            placeholderTextColor={'rgb(112, 112, 112)'}
            style={styles.feedbackTextInput}
            underlineColorAndroid={'rgba(255, 255, 255, 0)'}
            value={this.state.feedback}
          />
        </View>
        <Button
          disabled={!this.state.feedback}
          onPress={this.onSendButtonPressed}
          style={styles.button}
          title={strings.sendButtonTitle}
          width={120}
        />
      </View>
    )
  }
}
