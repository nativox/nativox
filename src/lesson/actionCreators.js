/* @flow */

import {createAction} from 'redux-actions'

export const clearLesson: any = createAction('CLEAR_LESSON')
export const resetLesson: any = createAction('RESET_LESSON')
export const loadCourse: any = createAction('LOAD_COURSE')
export const setFlatVideos: any = createAction('SET_FLAT_VIDEOS')
export const selectVideo: any = createAction('SELECT_VIDEO')
export const startLoadingVideo: any = createAction('START_LOADING_VIDEO')
export const finishLoadingVideo: any = createAction('FINISH_LOADING_VIDEO')
export const startPlayingVideo: any = createAction('START_PLAYING_VIDEO')
export const finishPlayingVideo: any = createAction('FINISH_PLAYING_VIDEO')
export const startRecordingVideo: any = createAction('START_RECORDING_VIDEO')
export const finishRecordingVideo: any = createAction('FINISH_RECORDING_VIDEO')
export const startPreparingRecordVideo: any = createAction(
  'START_PREPARING_RECORD_VIDEO',
)
export const finishPreparingRecordVideo: any = createAction(
  'FINISH_PREPARING_RECORD_VIDEO',
)
export const startTestingVideo: any = createAction('START_TESTING_VIDEO')
export const finishTestingVideo: any = createAction('FINISH_TESTING_VIDEO')
export const setAudioPath: any = createAction('SET_AUDIO_PATH')
export const startUploadingAudio: any = createAction('START_UPLOADING_AUDIO')
export const finishUploadingAudio: any = createAction('FINISH_UPLOADING_AUDIO')
export const setAudioEvaluation: any = createAction('SET_AUDIO_EVALUATION')
export const showSubtitles: any = createAction('SHOW_SUBTITLES')
export const hideSubtitles: any = createAction('HIDE_SUBTITLES')
export const openSubtitles: any = createAction('OPEN_SUBTITLES')
export const closeSubtitles: any = createAction('CLOSE_SUBTITLES')
export const showTranslation: any = createAction('SHOW_TRANSLATION')
export const showListeningGame: any = createAction('SHOW_LISTENING_GAME')
export const hideListeningGame: any = createAction('HIDE_LISTENING_GAME')
export const updateCourseStats: any = createAction('UPDATE_COURSE_STATS')
