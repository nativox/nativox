/* @flow */

import {
  loadCourse,
  selectVideo,
  setAudioEvaluation,
  setFlatVideos,
  finishRecordingVideo,
  startLoadingVideo,
  finishLoadingVideo,
  startPlayingVideo,
  finishPlayingVideo,
  startPreparingRecordVideo,
  finishPreparingRecordVideo,
  setAudioPath,
  startUploadingAudio,
  finishUploadingAudio,
  clearLesson,
  resetLesson,
  showSubtitles,
  closeSubtitles,
  openSubtitles,
  startTestingVideo,
  finishTestingVideo,
  showTranslation,
  showListeningGame,
  hideListeningGame,
  updateCourseStats,
} from './actionCreators'
import {updateCategoryCourseStats} from '../categories/actionCreators'

import RNFS from 'react-native-fs'
import {AudioRecorder, AudioUtils} from 'react-native-audio'
import Sound from 'react-native-sound'
import {Alert} from 'react-native'
import {HOST_NAME} from '../common/apiRequest'
import RNFetchBlob from 'react-native-fetch-blob'
import userSession from '../user/userSession'
import DeviceInfo from 'react-native-device-info'
import {strings} from '../stringsApp'
import {Answers} from 'react-native-fabric'

let audioPlayer: any = null
const applausePlayer: Object = new Sound('applause_wow.mp3', Sound.MAIN_BUNDLE)

const getFirstVideo = (curso: Object) =>
  curso.secciones[0].subsecciones[0].videos[0]

const moveToNextVideo = (flatVideos: Array<Object>, index: number) => {
  if (index === flatVideos.length - 1) {
    return flatVideos[flatVideos.length - 1]
  }

  const video: Object = flatVideos[index]

  if (video.superado !== '1') {
    return video
  }

  return moveToNextVideo(flatVideos, index + 1)
}

const moveToPreviousVideo = (flatVideos: Array<Object>, index: number) => {
  if (index < 1) {
    return flatVideos[0]
  }

  const video = flatVideos[index]

  if (video.superado !== '1') {
    return video
  }

  return moveToPreviousVideo(flatVideos, index - 1)
}

const downloadVideo = async (video: Object) => {
  const videoPath: string = `${RNFS.DocumentDirectoryPath}/${video.id_video}.mp4`

  try {
    if (!await RNFS.exists(videoPath)) {
      await RNFS.downloadFile({
        fromUrl: video.video_web_mp4,
        toFile: videoPath,
      }).promise
    }

    return {...video, videoPath}
  } catch (error) {
    console.log(error) // eslint-disable-line no-console

    return video
  }
}
export const onCourseLoaded = (course: Object) => async (
  dispatch: Function
) => {
  dispatch(loadCourse(course))

  let videos = []

  course.secciones.forEach((seccion: Object) =>
    seccion.subsecciones.forEach(
      (subseccion: Object) => (videos = videos.concat(subseccion.videos))
    )
  )

  dispatch(setFlatVideos(videos))
  dispatch(startLoadingVideo())

  const selectedVideo = await downloadVideo(getFirstVideo(course))

  dispatch(finishLoadingVideo())
  dispatch(selectVideo(selectedVideo))
  dispatch(onNextButtonPressed())
}

export const onCourseUnloaded = () => (dispatch: Function) => {
  dispatch(clearLesson())
}
export const onVideoSelected = (video: Object) => async (
  dispatch: Function
) => {
  dispatch(resetLesson())
  dispatch(selectVideo(video))
  dispatch(startLoadingVideo())

  const selectedVideo = await downloadVideo(video)

  dispatch(finishLoadingVideo())

  dispatch(selectVideo(selectedVideo))
}

export const onPreviousButtonPressed = () => (
  dispatch: Function,
  getState: Function
) => {
  const flatVideos = getState().lesson.flatVideos
  const selectedVideo = getState().lesson.selectedVideo

  const selectedFlatVideo = flatVideos.find(
    video => video.id_video === selectedVideo.id_video
  )
  const indexVideo = flatVideos.indexOf(selectedFlatVideo)

  if (indexVideo < 1) {
    dispatch(onVideoSelected(flatVideos[0]))
    return
  }

  const video = moveToPreviousVideo(flatVideos, indexVideo - 1)

  dispatch(onVideoSelected(video))

  const user = userSession.getUser()

  if (user) {
    Answers.logCustom('previous_button_pressed', {
      name: user.name,
      idUser: user.idUser,
    })
  }
}

export const onNextButtonPressed = () => (
  dispatch: Function,
  getState: Function
) => {
  const flatVideos = getState().lesson.flatVideos
  const selectedVideo = getState().lesson.selectedVideo

  const selectedFlatVideo = flatVideos.find(
    video => video.id_video === selectedVideo.id_video
  )
  const indexVideo = flatVideos.indexOf(selectedFlatVideo)

  if (indexVideo === flatVideos.length - 1) {
    dispatch(onVideoSelected(flatVideos[flatVideos.length - 1]))

    return
  }

  const video = moveToNextVideo(flatVideos, indexVideo + 1)

  dispatch(onVideoSelected(video))

  const user = userSession.getUser()

  if (user) {
    Answers.logCustom('next_button_pressed', {
      name: user.name,
      idUser: user.idUser,
    })
  }
}

export const onPlayButtonPressed = () => (dispatch: Function) => {
  Sound.setCategory('Playback')
  dispatch(startPlayingVideo())

  const user = userSession.getUser()

  if (user) {
    Answers.logCustom('play_button_pressed', {
      name: user.name,
      idUser: user.idUser,
    })
  }
}

export const onRecordButtonPressed = (callback: Function) => async (
  dispatch: Function
) => {
  Sound.setCategory('PlayAndRecord', true)

  const audioPath = `${AudioUtils.DocumentDirectoryPath}/${Date.now()}.wav`

  AudioRecorder.prepareRecordingAtPath(audioPath, {
    SampleRate: 44100,
    Channels: 1,
    AudioQuality: 'Medium',
    AudioEncoding: 'lpcm',
  })

  try {
    dispatch(setAudioPath(audioPath))

    await AudioRecorder.startRecording()

    callback()

    const user = userSession.getUser()

    if (user) {
      Answers.logCustom('record_button_pressed', {
        name: user.name,
        idUser: user.idUser,
      })
    }
  } catch (error) {
    Alert.alert('Nativox', strings.error_audio_recording)
  }
}

export const onVideoFinished = () => async (
  dispatch: Function,
  getState: Function
) => {
  const recordingVideo = getState().lesson.recordingVideo
  const testingVideo = getState().lesson.testingVideo
  const selectedVideo = getState().lesson.selectedVideo

  if (recordingVideo) {
    try {
      await AudioRecorder.stopRecording()

      dispatch(finishRecordingVideo())

      const audioPath = getState().lesson.audioPath

      audioPlayer = new Sound(audioPath, '', () => audioPlayer.setVolume(1.0))

      dispatch(startUploadingAudio())

      const user = userSession ? userSession.getUser() || {} : {}
      const language = user.language ? user.language : ''
      const {data} = await RNFetchBlob.fetch(
        'POST',
        `${HOST_NAME}evalua_audio?id_video=${selectedVideo.id_video}&id_usuario=${user.idUser}&realizar_notificaciones=false&lang=${language}&os=ios&id_device=${DeviceInfo.getUniqueID()}&version=${DeviceInfo.getVersion()}`,
        {
          'Content-Type': 'audio/wav',
        },
        RNFetchBlob.wrap(audioPath)
      )

      const dataObject: Object = JSON.parse(data)

      if (dataObject.result.error) {
        Alert.alert('Nativox', dataObject.result.error)

        return dispatch(finishUploadingAudio())
      }

      dispatch(
        setAudioEvaluation({
          audioPoints: dataObject.result.puntuacion,
          pointsMessage: dataObject.result.comentario,
        })
      )
      dispatch(updateCourseStats(dataObject.result.stats_curso))
      dispatch(updateCategoryCourseStats(dataObject.result.stats_curso))

      if (dataObject.result.puntuacion > 79) {
        applausePlayer.play()
      }

      dispatch(finishUploadingAudio())
    } catch (error) {
      Alert.alert('Nativox', strings.error_audio_recording)

      dispatch(finishUploadingAudio())
    }

    return
  }

  if (testingVideo) {
    dispatch(finishTestingVideo())

    return
  }

  dispatch(finishPlayingVideo())
  dispatch(showSubtitles())
  dispatch(showListeningGame())

  if (selectedVideo.superado === '1') {
    dispatch(openSubtitles())
    dispatch(hideListeningGame())
  }
}

export const onStartPreparingRecordVideo = () => (dispatch: Function) => {
  dispatch(startPreparingRecordVideo())
}

export const onFinishPreparingRecordVideo = () => (dispatch: Function) => {
  dispatch(finishPreparingRecordVideo())
}

export const onSubtitlesButtonPressed = () => (
  dispatch: Function,
  getState: Function
) => {
  const openedSubtitles = getState().lesson.openedSubtitles

  if (openedSubtitles) {
    dispatch(showListeningGame())
    dispatch(closeSubtitles())
  } else {
    dispatch(openSubtitles())
    dispatch(hideListeningGame())
  }
  const user = userSession.getUser()

  if (user) {
    Answers.logCustom('subtitles_button_pressed', {
      name: user.name,
      idUser: user.idUser,
    })
  }
}

export const onTestButtonPressed = (callback: Function) => async (
  dispatch: Function
) => {
  Sound.setCategory('Playback')
  audioPlayer.setCurrentTime(0)
  audioPlayer.play(success => {
    if (!success) {
      Alert.alert('Nativox', strings.error_loading_audio)

      return
    }
  })

  dispatch(startTestingVideo())
  callback()

  const user = userSession.getUser()

  if (user) {
    Answers.logCustom('test_button_pressed', {
      name: user.name,
      idUser: user.idUser,
    })
  }
}

export const onTranslationButtonPressed = () => (dispatch: Function) => {
  dispatch(showTranslation())

  const user = userSession.getUser()

  if (user) {
    Answers.logCustom('translation_button_pressed', {
      name: user.name,
      idUser: user.idUser,
    })
  }
}

export const onListeningGameButtonPressed = () => (dispatch: Function) => {
  dispatch(showListeningGame())

  const user = userSession.getUser()

  if (user) {
    Answers.logCustom('listening_button_pressed', {
      name: user.name,
      idUser: user.idUser,
    })
  }
}
