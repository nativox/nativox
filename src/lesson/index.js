/* @flow */

import reducer from './reducer'
import * as actionCreators from './actionCreators'
import * as eventHandlers from './eventHandlers'

export default {
  reducer,
  actionCreators,
  eventHandlers,
}
