/* @flow */

import {handleActions} from 'redux-actions'
import {
  finishPlayingVideo,
  finishLoadingVideo,
  finishRecordingVideo,
  finishTestingVideo,
  loadCourse,
  selectVideo,
  setAudioEvaluation,
  setFlatVideos,
  startLoadingVideo,
  startPlayingVideo,
  startTestingVideo,
  startRecordingVideo,
  startPreparingRecordVideo,
  finishPreparingRecordVideo,
  setAudioPath,
  startUploadingAudio,
  finishUploadingAudio,
  clearLesson,
  resetLesson,
  showSubtitles,
  hideSubtitles,
  openSubtitles,
  closeSubtitles,
  showTranslation,
  showListeningGame,
  hideListeningGame,
  updateCourseStats,
} from './actionCreators'

const defaultState: Object = {
  course: {},
  selectedVideo: null,
  audioPath: null,
  loadingVideo: false,
  playingVideo: false,
  uploadingAudio: false,
  recordingVideo: false,
  testingVideo: false,
  isPreparingRecord: false,
  isPreparingTest: false,
  isPlayButtonEnabled: true,
  isRecordButtonEnabled: false,
  isTestButtonEnabled: false,
  isTranslationButtonEnabled: false,
  videoVolume: 1.0,
  flatVideos: [],
  audioEvaluation: {},
  showSubtitles: false,
  openedSubtitles: false,
  showTranslation: false,
  numberPlays: 0,
  showListeningGame: false,
}

const updateVideoPointsInCourse = (
  course: Object,
  points: number,
  selectedVideo: Object,
) => {
  const newCourse = course
  const seccion = course.secciones.find(
    item => item.id_seccion === selectedVideo.id_seccion,
  )
  const indexSeccion = course.secciones.indexOf(seccion)
  const subseccion = course.secciones[indexSeccion].subsecciones.find(
    item => item.id_subseccion === selectedVideo.id_subseccion,
  )
  const indexSubSeccion = course.secciones[indexSeccion].subsecciones.indexOf(
    subseccion,
  )
  const video = course.secciones[indexSeccion].subsecciones[
    indexSubSeccion
  ].videos.find(item => item.id_video === selectedVideo.id_video)
  const indexVideo = subseccion.videos.indexOf(video)

  newCourse.secciones[indexSeccion].subsecciones[indexSubSeccion].videos[
    indexVideo
  ].puntuacion = points

  return newCourse
}

export default handleActions(
  {
    [loadCourse]: (state: Object, action: Object) => ({
      ...state,
      course: action.payload,
    }),
    [selectVideo]: (state: Object, action: Object) => ({
      ...state,
      selectedVideo: action.payload,
    }),
    [startLoadingVideo]: (state: Object) => ({...state, loadingVideo: true}),
    [finishLoadingVideo]: (state: Object) => ({...state, loadingVideo: false}),
    [startPlayingVideo]: (state: Object) => ({
      ...state,
      playingVideo: true,
      recordingVideo: false,
      testingVideo: false,
      isPlayButtonEnabled: false,
      isRecordButtonEnabled: false,
      isTestButtonEnabled: false,
      isTranslationButtonEnabled: false,
      videoVolume: 1.0,
      audioEvaluation: {},
      showTranslation: false,
      showListeningGame: false,
      showSubtitles: false,
    }),
    [finishPlayingVideo]: (state: Object) => ({
      ...state,
      playingVideo: false,
      isPlayButtonEnabled: true,
      isRecordButtonEnabled: true,
      isTestButtonEnabled: false,
      isTranslationButtonEnabled: false,
    }),
    [startRecordingVideo]: (state: Object) => ({
      ...state,
      playingVideo: false,
      recordingVideo: true,
      testingVideo: false,
      isPlayButtonEnabled: false,
      isRecordButtonEnabled: false,
      isTestButtonEnabled: false,
      isTranslationButtonEnabled: false,
      videoVolume: 0.0,
      audioEvaluation: {},
      showTranslation: false,
    }),
    [finishRecordingVideo]: (state: Object) => ({
      ...state,
      playingVideo: false,
      recordingVideo: false,
      testingVideo: false,
      isPlayButtonEnabled: false,
      isRecordButtonEnabled: false,
      isTestButtonEnabled: true,
      videoVolume: 1.0,
    }),
    [startTestingVideo]: (state: Object) => ({
      ...state,
      playingVideo: false,
      recordingVideo: false,
      testingVideo: true,
      isPlayButtonEnabled: false,
      isRecordButtonEnabled: false,
      isTestButtonEnabled: false,
      isTranslationButtonEnabled: false,
      videoVolume: 1.0,
      showTranslation: false,
    }),
    [finishTestingVideo]: (state: Object) => ({
      ...state,
      playingVideo: false,
      recordingVideo: false,
      testingVideo: false,
      isPlayButtonEnabled: true,
      isRecordButtonEnabled: true,
      isTestButtonEnabled: true,
      isTranslationButtonEnabled: state.audioEvaluation
        ? state.audioEvaluation.audioPoints > 69
        : false,
    }),
    [setFlatVideos]: (state: Object, action: Object) => ({
      ...state,
      flatVideos: action.payload,
    }),
    [startPreparingRecordVideo]: (state: Object) => ({
      ...state,
      isPreparingRecord: true,
    }),
    [finishPreparingRecordVideo]: (state: Object) => ({
      ...state,
      isPreparingRecord: false,
    }),
    [setAudioPath]: (state: Object, action: Object) => ({
      ...state,
      audioPath: action.payload,
    }),
    [startUploadingAudio]: (state: Object) => ({
      ...state,
      uploadingAudio: true,
      isTestButtonEnabled: true,
    }),
    [finishUploadingAudio]: (state: Object) => ({
      ...state,
      uploadingAudio: false,
      testingVideo: state.testingVideo ? state.testingVideo : false,
      isPlayButtonEnabled: true,
      isRecordButtonEnabled: true,
      isTestButtonEnabled: true,
    }),
    [clearLesson]: () => defaultState,
    [resetLesson]: (state: Object) => ({
      ...state,
      playingVideo: false,
      testingVideo: false,
      recordingVideo: false,
      uploadingAudio: false,
      isPlayButtonEnabled: true,
      isRecordButtonEnabled: false,
      isTestButtonEnabled: false,
      isTranslationButtonEnabled: false,
      audioEvaluation: {},
      showSubtitles: false,
      openedSubtitles: false,
      showTranslation: false,
      showListeningGame: false,
    }),
    [setAudioEvaluation]: (state: Object, action: Object) => ({
      ...state,
      selectedVideo: {
        ...state.selectedVideo,
        puntuacion: action.payload.audioPoints,
      },
      course: updateVideoPointsInCourse(
        state.course,
        action.payload.audioPoints,
        state.selectedVideo,
      ),
      audioEvaluation: action.payload,
      isTranslationButtonEnabled: action.payload.audioPoints > 69,
    }),
    [showSubtitles]: (state: Object) => ({
      ...state,
      showSubtitles: true,
    }),
    [hideSubtitles]: (state: Object) => ({
      ...state,
      showSubtitles: false,
    }),
    [openSubtitles]: (state: Object) => ({
      ...state,
      openedSubtitles: true,
    }),
    [closeSubtitles]: (state: Object) => ({
      ...state,
      openedSubtitles: false,
    }),
    [showTranslation]: (state: Object) => ({
      ...state,
      showTranslation: true,
    }),
    [showListeningGame]: (state: Object) => ({
      ...state,
      showListeningGame: true,
    }),
    [hideListeningGame]: (state: Object) => ({
      ...state,
      showListeningGame: false,
    }),
    [updateCourseStats]: (state: Object, action: Object) => ({
      ...state,
      course: {...state.course, stats_curso: action.payload},
    }),
  },
  defaultState,
)
