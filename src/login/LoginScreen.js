/* @flow */

import React from 'react'
import {AsyncStorage, Text, StyleSheet, View} from 'react-native'
import Screen from '../common/Screen'
import Device from 'react-native-device-info'
import {connect} from 'react-redux'
import TextBox from '../common/TextBox'
import Button from '../common/Button'
import ImageButton from '../common/ImageButton'
import {isSmallDevice} from '../common/DeviceUtility'
import {
  changeEmail,
  changePassword,
  makeVisibleResetPassword,
  makeInvisibleResetPassword,
  changeResetEmail,
  onResetPasswordPressed,
  onLoginPressed,
  onLoginFBPressed,
} from './loginDuck'
import emailValidator from 'email-validator'
import ModalScreen from '../common/ModalScreen'
import ResetPasswordModal from './ResetPasswordModal'
import connectFB from './connectFB'
import {strings} from '../stringsApp'
import ProgressHud from '../common/ProgressHud'
import Permissions from 'react-native-permissions'
import Component from '../common/Component'
import {NavigationActions} from 'react-navigation'

const styles: Object = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  loginContainer: {
    width: Device.isTablet() ? 270 : 170,
    backgroundColor: 'rgb(162, 162, 161)',
    borderRadius: 6,
    paddingTop: Device.isTablet() ? 25 : 15,
    paddingLeft: Device.isTablet() ? 20 : 13,
    paddingRight: Device.isTablet() ? 20 : 13,
    alignSelf: 'center',
  },
  loginTitle: {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    color: 'rgb(162, 162, 161)',
    fontSize: Device.isTablet() ? 41 : 21,
    marginBottom: 20,
    fontFamily: 'Helvetica Neue',
    fontWeight: '300',
    textAlign: 'center',
  },
  loginButton: {
    height: Device.isTablet() ? 45 : 30,
    marginBottom: 15,
    marginTop: 15,
  },
  forgotButtonTitle: {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    fontSize: Device.isTablet() ? 25 : 14,
  },
  signupButtonTitle: {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    fontSize: Device.isTablet() ? 25 : 16,
    fontWeight: 'bold',
    color: 'rgb(255,255,255)',
  },
  loginTitleButton: {
    fontSize: Device.isTablet() ? 22 : 16,
  },
  noAccountLabel: {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    fontSize: Device.isTablet() ? 25 : 17,
    color: 'rgb(255, 255, 255)',
  },
  facebookButtonContainer: {
    paddingTop: Device.isTablet() ? 80 : isSmallDevice() ? 60 : 80,
  },
  signupButtonContainer: {
    flexDirection: 'row',
    marginTop: 40,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  signupButton: {marginLeft: 20},
})

const shouldEnableLoginButton = (email: string, password: string) =>
  email && emailValidator.validate(email) && password

let shouldShowRegistrationScreen = null

AsyncStorage.getItem('shouldShowRegistrationScreen').then((data: string) => {
  shouldShowRegistrationScreen = data !== 'true'
})

const heightResetPassword = Device.isTablet() ? 250 : 200
const widthResetPassword = Device.isTablet() ? 300 : 250

class LoginScreen extends Component {
  static navigationOptions = {header: null}

  props: {
    changeEmail: Function,
    changePassword: Function,
    changeResetEmail: Function,
    email: string,
    makeInvisibleResetPassword: Function,
    makeVisibleResetPassword: Function,
    navigation: Object,
    onLoginFBPressed: Function,
    onLoginPressed: Function,
    onResetPasswordPressed: Function,
    password: string,
    resetEmail: string,
    showResetPassword: boolean,
  }

  state = {
    showProgressLogin: false,
    progressTitle: '',
  }

  componentWillMount() {
    if (shouldShowRegistrationScreen) {
      shouldShowRegistrationScreen = false
      this.props.navigation.navigate('Register', {shouldPresentFBLogin: true})
    }
  }

  componentDidMount() {
    Permissions.check('microphone').then((response: string) => {
      if (response !== 'authorized') {
        Permissions.request('microphone')
      }
    })
  }

  connectFB = () => {
    connectFB((params: Object) => {
      this.props.onLoginFBPressed({
        facebookParams: params,
        isSignUp: false,
        onShowProgressDialog: (title: string) => {
          this.setState({
            showProgressLogin: true,
            progressTitle: title,
          })
        },
        onDismissProgressDialog: () =>
          this.setState({showProgressLogin: false}),
        onSuccess: () => this.props.navigation.navigate('Main'),
        onFailure: () => {
          const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'Login'})],
          })
          this.props.navigation.dispatch(resetAction)
        },
      })
    })
  }

  onLoginPressed = () => {
    this.props.onLoginPressed({
      onShowProgressDialog: (title: string) => {
        this.setState({
          showProgressLogin: true,
          progressTitle: title,
        })
      },
      onDismissProgressDialog: () => this.setState({showProgressLogin: false}),
      onSuccess: () => this.props.navigation.navigate('Main'),
      onFailure: () => {
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'Login'})],
        })
        this.props.navigation.dispatch(resetAction)
      },
    })
  }

  onResetPressed = () => {
    this.props.onResetPasswordPressed({
      onShowProgressDialog: (title: string) => {
        this.setState({
          showProgressLogin: true,
          progressTitle: title,
        })
      },
      onDismissProgressDialog: () => this.setState({showProgressLogin: false}),
    })
  }

  onPressRegisterButton = () => {
    this.props.navigation.navigate('Register')
  }

  render() {
    return (
      <Screen scroll>
        <ProgressHud
          isVisible={this.state.showProgressLogin}
          text={this.state.progressTitle}
        />
        <ModalScreen
          height={heightResetPassword}
          isVisible={this.props.showResetPassword}
          onCloseButtonPressed={this.props.makeInvisibleResetPassword}
          onTouchBackground={this.props.makeInvisibleResetPassword}
          width={widthResetPassword}
        >
          <ResetPasswordModal
            email={this.props.resetEmail}
            onChangeEmail={this.props.changeResetEmail}
            onResetPassword={this.onResetPressed}
          />
        </ModalScreen>
        <View style={styles.container}>
          <Text style={styles.loginTitle}>{strings.loginTitle}</Text>
          <View style={styles.loginContainer}>
            <TextBox
              onChangeText={this.props.changeEmail}
              placeholder="Email"
              value={this.props.email}
            />
            <TextBox
              isPassword
              onChangeText={this.props.changePassword}
              placeholder={strings.placeholderPassword}
              value={this.props.password}
            />
            <Button
              disabled={
                !shouldEnableLoginButton(this.props.email, this.props.password)
              }
              onPress={this.onLoginPressed}
              style={styles.loginButton}
              title={'Log in'}
              titleStyle={styles.loginTitleButton}
            />
          </View>

          <Button
            invisible
            onPress={this.props.makeVisibleResetPassword}
            title={strings.labelForgotPassword}
            titleStyle={styles.forgotButtonTitle}
          />
          <View style={styles.facebookButtonContainer}>
            <ImageButton
              height={Device.isTablet() ? 50 : 25}
              image="fb_button"
              onPress={this.connectFB}
              width={Device.isTablet() ? 260 : 150}
            />
          </View>
          <View style={styles.signupButtonContainer}>
            <Text style={styles.noAccountLabel}>{strings.labelRegister}</Text>
            <Button
              invisible
              onPress={this.onPressRegisterButton}
              style={styles.signupButton}
              title={strings.signUpButtonTitle}
              titleStyle={styles.signupButtonTitle}
            />
          </View>
        </View>
      </Screen>
    )
  }
}

export default connect(
  (state: Object) => ({
    email: state.login.get('email'),
    resetEmail: state.login.get('resetEmail'),
    password: state.login.get('password'),
    showResetPassword: state.login.get('showResetPassword'),
  }),
  (dispatch: Function) => ({
    changeEmail: text => dispatch(changeEmail(text)),
    changePassword: text => dispatch(changePassword(text)),
    changeResetEmail: text => dispatch(changeResetEmail(text)),
    makeVisibleResetPassword: () => dispatch(makeVisibleResetPassword()),
    makeInvisibleResetPassword: () => dispatch(makeInvisibleResetPassword()),
    onResetPasswordPressed: props => dispatch(onResetPasswordPressed(props)),
    onLoginPressed: props => dispatch(onLoginPressed(props)),
    onLoginFBPressed: props => dispatch(onLoginFBPressed(props)),
  })
)(LoginScreen)
