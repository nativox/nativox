/* @flow */

import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import TextBox from '../common/TextBox'
import Button from '../common/Button'
import {EXPLAINATION_LABEL_SIZE} from '../common/NativoxTheme'
import emailValidator from 'email-validator'
import {strings} from '../stringsApp'
import Component from '../common/Component'

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  explaination: {
    color: 'rgb(255, 255, 255)',
    fontSize: EXPLAINATION_LABEL_SIZE,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  textbox: {
    marginBottom: 15,
  },
})

const shouldResetButton = (email: string) =>
  email && emailValidator.validate(email)

export default class ResetPasswordModal extends Component {
  props: {
    email: string,
    onChangeEmail: Function,
    onResetPassword: Function,
  }

  render = () =>
    <View style={styles.container}>
      <Text style={styles.explaination}>
        {strings.labelResetPassword}
      </Text>
      <TextBox
        onChangeText={this.props.onChangeEmail}
        placeholder={'Email'}
        style={styles.textbox}
        value={this.props.email}
      />
      <Button
        disabled={!shouldResetButton(this.props.email)}
        onPress={this.props.onResetPassword}
        title={strings.resetButtonTitle}
      />
    </View>
}
