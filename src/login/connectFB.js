/* @flow */

import {
  GraphRequest,
  GraphRequestManager,
  LoginManager,
  AccessToken,
} from 'react-native-fbsdk'
import {strings} from '../stringsApp'
import showAlertMessage from '../common/showAlertMessage'

const getFacebookUserInfo = (token: string, onLoggedInFB: Function) => {
  new GraphRequestManager()
    .addRequest(
      new GraphRequest(
        '/me?fields=id,name,email',
        null,
        (error: Object, result: Object) => {
          if (error) {
            showAlertMessage(error.message)

            return
          }

          onLoggedInFB({
            email: result.email,
            userName: result.name,
            idFacebook: result.id,
            name: result.name,
            token,
          })
        },
      ),
    )
    .start()
}

export default async (onLoggedInFB: Function) => {
  try {
    const data = await AccessToken.getCurrentAccessToken()

    if (data && data.accessToken) {
      getFacebookUserInfo(data.accessToken, onLoggedInFB)

      return
    }

    const result = await LoginManager.logInWithReadPermissions([
      'public_profile',
      'email',
    ])

    if (!result.isCancelled) {
      const newData = await AccessToken.getCurrentAccessToken()

      getFacebookUserInfo(newData.accessToken, onLoggedInFB)
    }
  } catch (error) {
    showAlertMessage(strings.error_login_fb)    
  }
}
