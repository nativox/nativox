/* @flow */

import {fromJS} from 'immutable'
import {createAction, handleActions} from 'redux-actions'
import {post} from '../common/apiRequest'
import userSession from '../user/userSession'
import {strings} from '../stringsApp'
import {Answers} from 'react-native-fabric'
import categories from '../categories'
import showAlertMessage from '../common/showAlertMessage'

const {onLoadCategories} = categories.eventHandlers

export const SET_EMAIL = 'SET_EMAIL'
export const SET_PASSWORD = 'SET_PASSWORD'
export const SET_RESET_EMAIL = 'SET_RESET_EMAIL'
export const SHOW_RESET_PASSWORD = 'SHOW_RESET_PASSWORD'
export const HIDE_RESET_PASSWORD = 'HIDE_RESET_PASSWORD'

const setEmail = createAction(SET_EMAIL)
const setPassword = createAction(SET_PASSWORD)
const setResetEmail = createAction(SET_RESET_EMAIL)
const showResetPassword = createAction(SHOW_RESET_PASSWORD)
const hideResetPassword = createAction(HIDE_RESET_PASSWORD)

export const changeEmail = (email: string) => {
  return (dispatch: Function) => {
    dispatch(setEmail(email))
  }
}

export const changePassword = (password: string) => {
  return (dispatch: Function) => {
    dispatch(setPassword(password))
  }
}

export const changeResetEmail = (email: string) => {
  return (dispatch: Function) => {
    dispatch(setResetEmail(email))
  }
}

export const makeVisibleResetPassword = () => {
  return (dispatch: Function) => {
    dispatch(showResetPassword())
  }
}

export const makeInvisibleResetPassword = () => {
  return (dispatch: Function) => {
    dispatch(hideResetPassword())
  }
}

export const onResetPasswordPressed = ({
  onShowProgressDialog,
  onDismissProgressDialog,
}: {
  onShowProgressDialog: Function,
  onDismissProgressDialog: Function,
}) => {
  return (dispatch: Function, getState: Function) => {
    const loginState = getState().login
    const params = {
      correo: loginState.get('resetEmail'),
    }

    dispatch(makeInvisibleResetPassword())
    onShowProgressDialog(strings.progressResetting)
    post('correo_recuperar_contrasenya', params)
      .then(response => {
        onDismissProgressDialog()
        if (response.status === 'true') {
          dispatch(changeResetEmail(''))
          Answers.logCustom('Reset Password', {email: params.correo})
        }
        
        showAlertMessage(response.result)          
      })
      .catch(() => {
        onDismissProgressDialog()
        showAlertMessage(strings.issueResetPassword)          
      })
  }
}

export const onLoginPressed = ({
  onShowProgressDialog,
  onDismissProgressDialog,
  onSuccess,
  onFailure,
}: {
  onShowProgressDialog: Function,
  onDismissProgressDialog: Function,
  onSuccess: Function,
  onFailure: Function,
}) => {
  return async (dispatch: Function, getState: Function) => {
    const loginState = getState().login
    const params = {
      correo: loginState.get('email'),
      password: loginState.get('password'),
    }

    onShowProgressDialog(strings.progressLogin)

    try {
      const response = await post('login', params)

      onDismissProgressDialog()

      if (response.status === 'true') {
        dispatch(changeEmail(''))
        dispatch(changePassword(''))

        Answers.logLogin('Email', true)

        await userSession.setUser({
          email: loginState.get('email'),
          name: response.result.nombre,
          token: response.result.token,
          idUser: response.result.id_usuario,
        })

        dispatch(onLoadCategories()).catch((error) => {
          console.log(error) // eslint-disable-line no-console        
          onFailure()
          showAlertMessage(strings.issueLogin)          
        })

        onSuccess()
      } else {
        Answers.logLogin('Email', false)
        showAlertMessage(response.result)          
        
      }
    } catch (error) {
      console.log(error) // eslint-disable-line no-console
      onDismissProgressDialog()
      Answers.logLogin('Email', false)
      showAlertMessage(strings.issueLogin)                
    }
  }
}

export const onLoginFBPressed = ({
  facebookParams,
  isSignUp,
  onShowProgressDialog,
  onDismissProgressDialog,
  onSuccess,
  onFailure,
}: {
  facebookParams: Object,
  isSignUp: boolean,
  onShowProgressDialog: Function,
  onDismissProgressDialog: Function,
  onSuccess: Function,
}) => {
  return (dispatch: Function) => {
    const params = {
      correo: facebookParams.email,
      nombre_usuario: facebookParams.userName,
      id_facebook: facebookParams.idFacebook,
      nombre: facebookParams.name,
      access_token: facebookParams.token,
    }

    onShowProgressDialog(strings.progressLogin)
    post('login_fb', params)
      .then(response => {
        onDismissProgressDialog()

        if (response.status === 'true') {
          if (isSignUp) {
            Answers.logSignUp('Email', true)
          } else {
            Answers.logLogin('Facebook', true)
          }

          userSession.setUser({
            email: facebookParams.email,
            name: response.result.nombre,
            token: response.result.token,
            idUser: response.result.id_usuario,
          })
          dispatch(onLoadCategories()).catch(() => {
            onFailure()

            showAlertMessage(strings.issueLogin)                
          })
          onSuccess()
        } else {
          Answers.logLogin('Facebook', false)
          showAlertMessage(response.result)                
        }
      })
      .catch(() => {
        onDismissProgressDialog()
        Answers.logLogin('Facebook', false)
        showAlertMessage(strings.issueLogin)                
      })
  }
}

const defaultState = fromJS({
  email: '',
  password: '',
  showResetPassword: false,
  resetEmail: '',
})

export default handleActions(
  {
    [SET_EMAIL]: (state, action) => state.set('email', action.payload),
    [SET_PASSWORD]: (state, action) => state.set('password', action.payload),
    [SHOW_RESET_PASSWORD]: state => state.set('showResetPassword', true),
    [HIDE_RESET_PASSWORD]: state => state.set('showResetPassword', false),
    [SET_RESET_EMAIL]: (state, action) =>
      state.set('resetEmail', action.payload),
  },
  defaultState
)
