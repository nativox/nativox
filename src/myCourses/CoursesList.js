/* @flow */

import React from 'react'
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  WebView,
} from 'react-native'
import Accordion from 'react-native-collapsible/Accordion'
import {PRIMARY_COLOR} from '../common/NativoxTheme.js'
import Component from '../common/Component'
import Device from 'react-native-device-info'

const styles = StyleSheet.create({
  header: {
    flex: 1,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    height: Device.isTablet() ? 65 : 50,
    borderRadius: 7,
    marginTop: 5,
    marginBottom: 5,
  },
  headerText: {
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isTablet() ? 22 : 17,
    backgroundColor: 'rgba(255, 255, 255, 0)',
  },
  content: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0)',
  },
  webviewContainer: {
    height: 200,
    backgroundColor: 'rgba(255, 255, 255, 0)',
  },
  headerBarTransparent: {
    borderRadius: 7,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
})

class CoursesList extends Component {
  props: {
    courses: Array<Object>,
    onPressPlay: Function,
  }

  state = {
    width: 0,
    accordionVisible: true,
  }

  renderCourseHeader = (course: Object) => {
    const percentageCompleted =
      parseFloat(course.stats_curso.porcentaje_completado) / 100
    const widthHeader = this.state.width * percentageCompleted

    return (
      <View style={[styles.header, {backgroundColor: course.color_hex}]}>
        <View style={styles.headerBarTransparent} />
        <View
          style={{
            borderRadius: 7,
            position: 'absolute',
            backgroundColor: course.color_hex,
            top: 0,
            left: 0,
            bottom: 0,
            width: widthHeader,
          }}
        />
        <Text style={styles.headerText}>{course.curso}</Text>
      </View>
    )
  }

  renderContent = (course: Object) => {
    const contentWebView = `
      <html> <body style=\"font-family:Helvetica Neue;background-color: transparent;color:#FFF;text-align:center;font-size:${Device.isTablet()
        ? '19px'
        : Platform.OS === 'android' ? '14px' : '8px'};vertical-align:middle;\">
        ${course.descripcion}
       </body></html>
    `

    return (
      <View style={styles.content}>
        <WebView
          source={{html: contentWebView}}
          style={styles.webviewContainer}
        />
        <View style={{flex: 1, alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => {
              this.props.onPressPlay(course)
              this.setState({accordionVisible: false})
              setTimeout(() => this.setState({accordionVisible: true}), 500)
            }}
            style={{
              backgroundColor: PRIMARY_COLOR,
              borderRadius: 7,
              padding: 5,
              margin: 5,
            }}
          >
            <Text
              style={{
                color: 'rgb(255, 255, 255)',
                width: Device.isTablet() ? 100 : 80,
                textAlign: 'center',
                fontSize: Device.isTablet() ? 22 : 17,
              }}
            >
              Play
            </Text>
          </TouchableOpacity>
        </View>

      </View>
    )
  }

  onLayoutContainer = (event: Object) => {
    const {width} = event.nativeEvent.layout

    this.setState({
      width,
    })
  }

  render() {
    return (
      <ScrollView onLayout={this.onLayoutContainer} style={{flex: 1}}>
        {this.state.accordionVisible
          ? <Accordion
              renderContent={this.renderContent}
              renderHeader={this.renderCourseHeader}
              sections={this.props.courses}
              underlayColor={'rgba(255, 255, 255, 0)'}
            />
          : null}

      </ScrollView>
    )
  }
}

export default CoursesList
