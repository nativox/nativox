/* @flow */

import React from 'react'
import {StyleSheet, View, Text} from 'react-native'
import Screen from '../common/Screen'
import {connect} from 'react-redux'
import ActivityIndicatorView from '../common/ActivityIndicatorView'
import CoursesList from './CoursesList'
import lesson from '../lesson'
import {strings} from '../stringsApp'
import Button from '../common/Button'
import Device from 'react-native-device-info'

const {onCourseLoaded} = lesson.eventHandlers

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  goToCoursesButtonTitle: {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isTablet() ? 30 : 19,
    fontFamily: 'Helvetica Neue',
    fontWeight: '300',
    textAlign: 'center',
    padding: Device.isTablet() ? 40 : 20,
  },
  goToCoursesButton: {
    height: Device.isTablet() ? 60 : 35,
    margin: 15,
  },
  noCoursesLabel: {color: 'rgb(255, 255, 255)', fontSize: Device.isTablet() ? 30 : 19},
  noCoursesContainer: {flex: 1, justifyContent: 'center', alignItems: 'center', margin: 5},
})

class MyCoursesScreen extends React.Component {
  static navigationOptions = {header: null}

  state = {
    height: null,
    width: null,
  }

  props: {
    courses: Array<Object>,
    isHydrating: boolean,
    navigation: Object,
    onCourseLoaded: Function,
  }

  onPressPlay = course => {
    this.props.onCourseLoaded(course)
    this.props.navigation.navigate('LessonApp')
  }

  onPressLeftButton = () => this.props.navigation.goBack()

  onGoToCoursesButtonPressed = () => this.props.navigation.navigate('Categories')
  
  renderCourses = () => {
    if (this.props.courses.length === 0) {
      return (
        <View style={styles.noCoursesContainer}>
          <Text style={styles.noCoursesLabel}>{strings.no_courses_purchased}</Text>
          <Button
            onPress={this.onGoToCoursesButtonPressed}
            style={styles.goToCoursesButton}
            title={strings.go_to_courses_button}
            titleStyle={styles.goToCoursesButtonTitle} />
        </View>
      )
    }
    return (
      <CoursesList
        courses={this.props.courses}
        onPressPlay={this.onPressPlay}
      />
    )
  }

  render() {
    return (
      <Screen
        fullscreen
        onPressLeftButton={this.onPressLeftButton}
        showLeftNavBar
        title={strings.mycourses_title}
      >
        <View style={styles.container}>
          {this.props.isHydrating ? <ActivityIndicatorView /> : this.renderCourses()}
        </View>
      </Screen>
    )
  }
}

export default connect(
  state => ({
    isHydrating: state.categories.isHydrating,
    courses: state.categories.categories.reduce((reduction, value) => {
      if (!value.comprado || value.demo) {
        return reduction
      }

      return reduction.concat(value.cursos)
    }, []),
  }),
  dispatch => ({
    onCourseLoaded: course => dispatch(onCourseLoaded(course)),
  })
)(MyCoursesScreen)
