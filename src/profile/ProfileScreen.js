/* @flow */

import React from 'react'
import {Image, StyleSheet, Text, TouchableOpacity} from 'react-native'
import Screen from '../common/Screen'
import {strings} from '../stringsApp'
import {connect} from 'react-redux'
import Device from 'react-native-device-info'
import TappableIcon from '../common/TappableIcon'

const styles: Object = StyleSheet.create({
  menuOptionContainer: {
    flex: 1,
    borderRadius: 7,
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 9,
    marginRight: 9,
    padding: 7,
  },
  menuOptionText: {
    textAlign: 'center',
    fontSize: Device.isTablet() ? 22 : 17,
    color: 'rgb(255, 255, 255)',
  },
})

let tabBarIconStyleUser = null

class ProfileScreen extends React.Component {
  static navigationOptions = {
    header: null,
    tabBarIcon: ({tintColor}) => {
      if (!tabBarIconStyleUser || tabBarIconStyleUser.color !== tintColor) {
        tabBarIconStyleUser = {color: tintColor}
      }

      return (
        <TappableIcon
          containerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          name="user"
          size={25}
          style={tabBarIconStyleUser}
        />
      )
    },
  }

  props: {
    navigation: Object,
  }

  state = {
    menuOptions: [
      {
        icon: 'my_courses_box_icon',
        text: () => strings.my_courses_label,
        backgroundColor: 'rgb(0, 37, 75)',
        screen: 'MyCourses',
      },
      {
        icon: 'my_settings_box_icon',
        text: () => strings.settings_label,
        backgroundColor: 'rgb(15, 124, 178)',
        screen: 'Settings',
      },
      {
        icon: 'caseta',
        text: () => strings.account_label,
        backgroundColor: 'rgb(17, 116, 110)',
        screen: 'Account',
      },
      {
        icon: 'contact_icono',
        text: () => strings.contact_label,
        backgroundColor: 'rgb(128, 9, 0)',
        screen: 'Contact',
      },
    ],
  }

  navigateToScreen = (screen: string) => {
    this.props.navigation.navigate(screen)
  }

  render() {
    return (
      <Screen fullscreen title={strings.profile_title}>
        {this.state.menuOptions.map((menuOption: Object, index: number) => (
          <TouchableOpacity
            key={index}
            onPress={() => this.navigateToScreen(menuOption.screen)}
            style={[
              styles.menuOptionContainer,
              {backgroundColor: menuOption.backgroundColor},
            ]}
          >
            <Image
              resizeMode={'contain'}
              source={{uri: menuOption.icon, isStatic: true}}
              style={{flex: 1, margin: 5}}
            />
            <Text style={styles.menuOptionText}>{menuOption.text()}</Text>
          </TouchableOpacity>
        ))}
      </Screen>
    )
  }
}

export default connect(state => ({
  categories: state.categories.categories,
}))(ProfileScreen)
