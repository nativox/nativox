/* @flow */

import React from 'react'
import {Picker, StyleSheet, View} from 'react-native'
import languages from './languages'
import Button from '../common/Button'
import {strings} from '../stringsApp'

const styles = StyleSheet.create({
  itemStyle: {
    color: 'rgb(255, 255, 255)',
    fontSize: 23,
    fontWeight: 'bold',
  },
  setButtonTitle: {
    fontSize: 20,
  },
})

export default class LanguageSelector extends React.PureComponent {
  constructor() {
    super()

    this.state = {
      selectedLanguage: languages[0].key,
    }
  }

  props: {
    onLanguageSelected: Function,
    selectedLanguage?: string,
  }

  state: {
    selectedLanguage: string,
  }

  componentDidMount() {
    if (this.props.selectedLanguage) {
      this.setState({selectedLanguage: this.props.selectedLanguage})
    }
  }

  changeLanguage = (value: string) => {
    this.setState({selectedLanguage: value})
  }

  onLanguageSelected = () => {
    this.props.onLanguageSelected(this.state.selectedLanguage)
  }

  render() {
    return (
      <View>
        <Picker
          itemStyle={styles.itemStyle}
          onValueChange={this.changeLanguage}
          selectedValue={this.state.selectedLanguage}
          style={{color: 'rgb(255, 255, 255)', height: 250}}>
          {languages.map(language =>
            <Picker.Item
              key={language.key}
              label={language.description}
              value={language.key}
            />,
          )}
        </Picker>
        <Button
          invisible
          onPress={this.onLanguageSelected}
          title={strings.setButtonTitle}
          titleStyle={styles.setButtonTitle}
        />
      </View>
    )
  }
}
