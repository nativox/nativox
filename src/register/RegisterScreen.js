/* @flow */

import React from 'react'
import {
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native'
import Screen from '../common/Screen'
import ModalScreen from '../common/ModalScreen'
import Button from '../common/Button'
import Checkbox from '../common/Checkbox'
import {
  changeRegisterName,
  changeRegisterEmail,
  changeRegisterPassword,
  changeRegisterConfirmPassword,
  changeTermsAndConditionsAccepted,
  openLanguageSelector,
  closeLanguageSelector,
  changeSelectedLanguage,
  openConfirmRegistration,
  closeConfirmRegistration,
  onRegisterUserPressed,
} from './registerDuck'
import {onLoginFBPressed} from '../login/loginDuck'
import {connect} from 'react-redux'
import LanguageSelector from './LanguageSelector'
import ConfirmRegistration from './ConfirmRegistration'
import {getDescriptionLanguageSelected} from './languages'
import emailValidator from 'email-validator'
import openUrl from '../common/openUrl'
import Device from 'react-native-device-info'
import {strings} from '../stringsApp'
import ImageButton from '../common/ImageButton'
import TextBox from '../common/TextBox'
import connectFB from '../login/connectFB'
import {isSmallDevice} from '../common/DeviceUtility'
import ProgressHud from '../common/ProgressHud'

const styles: Object = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -20,
  },
  registerContainer: {
    width: Device.isTablet() ? 300 : 200,
    backgroundColor: 'rgb(162, 162, 161)',
    borderRadius: 6,
    paddingTop: Device.isTablet() ? 25 : 15,
    paddingLeft: Device.isTablet() ? 20 : 13,
    paddingRight: Device.isTablet() ? 20 : 13,
  },
  registerTitle: {
    color: 'rgb(162, 162, 161)',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    fontSize: Device.isTablet() ? 41 : 21,
    marginBottom: 20,
    fontFamily: 'Helvetica Neue',
    fontWeight: '300',
  },
  selectLanguageButton: {
    paddingTop: 7,
    paddingBottom: 10,
    textAlign: 'center',
    color: 'rgb(51, 51, 51)',
    fontSize: Device.isTablet() ? 22 : 16,
  },
  checkBoxLabel: {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    fontSize: Device.isTablet() ? 19 : 12,
    color: 'rgb(255, 255, 255)',
    margin: 8,
    flex: 5,
    fontFamily: 'Helvetica Neue',
    fontWeight: '300',
  },
  checkboxContainer: {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    marginTop: 12,
    width: Device.isTablet() ? 270 : 180,
    height: Device.isTablet() ? 65 : 45,
    flexDirection: 'row',
    alignItems: 'center',
  },
  registerButton: {
    height: Device.isTablet() ? 45 : 30,
    marginBottom: 15,
  },
  registerTitleButton: {
    fontSize: Device.isTablet() ? 22 : 16,
  },
  languageButton: {
    backgroundColor: 'transparent',
    marginTop: 10,
    marginBottom: 10,
  },
  facebookButtonContainer: {
    paddingTop: Device.isTablet() ? 80 : isSmallDevice() ? 60 : 80,
  },
})

const openTermsAndConditionsUrl = () =>
  openUrl(
    'http://nativox.com/index.php/ini/terms',
    'http://nativox.com/index.php/ini/terms',
  )

class RegisterScreen extends React.Component {
  static navigationOptions = {header: null}

  props: {
    changeRegisterConfirmPassword: Function,
    changeRegisterEmail: Function,
    changeRegisterName: Function,
    changeRegisterPassword: Function,
    changeSelectedLanguage: Function,
    changeTermsAndConditionsAccepted: Function,
    closeConfirmRegistration: Function,
    closeLanguageSelector: Function,
    hasPresentedRegister: boolean,
    navigation: Object,
    onLoginFBPressed: Function,
    onRegisterUserPressed: Function,
    openConfirmRegistration: Function,
    openLanguageSelector: Function,
    registerConfirmPassword: string,
    registerEmail: string,
    registerName: string,
    registerPassword: string,
    selectedLanguage: string,
    showConfirmRegistration: boolean,
    showLanguageSelector: boolean,
    termsAndConditionsAccepted: boolean,
  }

  state: {
    focusedField?: any,
    showProgressLogin: boolean,
    progressTitle: string,
  }

  state = {
    focusedField: null,
    showProgressLogin: false,
    progressTitle: '',
  }

  componentDidMount() {
    AsyncStorage.setItem('shouldShowRegistrationScreen', 'true')
  }

  onRowFocus = (ref: any) => {
    this.setState({focusedField: ref})
  }

  onPressRegistrationButton = () => {
    if (!this.props.registerName) {
      Alert.alert('Nativox', strings.error_registration_name) // eslint-disable-line no-undef
      return
    }

    if (!this.props.registerEmail) {
      Alert.alert('Nativox', strings.error_registration_email) // eslint-disable-line no-undef
      return
    }

    if (!emailValidator.validate(this.props.registerEmail)) {
      Alert.alert('Nativox', strings.error_registration_email_no_valid) // eslint-disable-line no-undef
      return
    }

    if (!this.props.registerPassword) {
      Alert.alert('Nativox', strings.error_registration_password) // eslint-disable-line no-undef
      return
    }

    if (!this.props.registerConfirmPassword) {
      Alert.alert('Nativox', strings.error_registration_confirmation_password) // eslint-disable-line no-undef
      return
    }

    if (this.props.registerPassword.length < 6) {
      Alert.alert('Nativox', strings.error_registration_password_length) // eslint-disable-line no-undef
      return
    }

    if (this.props.registerPassword !== this.props.registerConfirmPassword) {
      Alert.alert('Nativox', strings.error_registration_password_different) // eslint-disable-line no-undef
      return
    }

    if (!this.props.selectedLanguage) {
      Alert.alert('Nativox', strings.error_registration_no_language) // eslint-disable-line no-undef
      return
    }

    if (!this.props.termsAndConditionsAccepted) {
      Alert.alert('Nativox', strings.error_registration_no_tcs) // eslint-disable-line no-undef
      return
    }

    this.props.openConfirmRegistration()
  }

  onRegisterUserPressed = () => {
    this.props.onRegisterUserPressed({
      onShowProgressDialog: title => {
        this.setState({
          showProgressLogin: true,
          progressTitle: title,
        })
      },
      onDismissProgressDialog: () => this.setState({showProgressLogin: false}),
      onSuccess: () => this.props.navigation.goBack(),
    })
  }

  onPressFBButton = () => {
    connectFB(params => {
      this.props.onLoginFBPressed({
        facebookParams: params,
        isSignUp: true,
        onShowProgressDialog: title => {
          this.setState({
            showProgressLogin: true,
            progressTitle: title,
          })
        },
        onDismissProgressDialog: () =>
          this.setState({showProgressLogin: false}),
        onSuccess: () => this.props.navigation.navigate('Main'),
      })
    })
  }

  render() {
    const {params} = this.props.navigation.state

    return (
      <Screen
        focusedField={this.refs[this.state.focusedField]} // eslint-disable-line react/no-string-refs
        onPressLeftButton={() => this.props.navigation.goBack()}
        showLeftNavBar>
        <ProgressHud
          isVisible={this.state.showProgressLogin}
          text={this.state.progressTitle}
        />
        <ModalScreen
          height={320}
          isVisible={this.props.showLanguageSelector}
          onCloseButtonPressed={this.props.closeLanguageSelector}
          onTouchBackground={this.props.closeLanguageSelector}
          width={250}>
          <LanguageSelector
            onLanguageSelected={this.props.changeSelectedLanguage}
          />
        </ModalScreen>
        <ModalScreen
          height={320}
          isVisible={this.props.showConfirmRegistration}
          onCloseButtonPressed={this.props.closeConfirmRegistration}
          onTouchBackground={this.props.closeConfirmRegistration}
          width={Device.isTablet() ? 290 : 250}>
          <ConfirmRegistration
            email={this.props.registerEmail}
            onChangeDataPressed={this.props.closeConfirmRegistration}
            onYesButtonPressed={this.onRegisterUserPressed}
            password={this.props.registerPassword}
          />
        </ModalScreen>
        <View style={styles.container}>
          <Text style={styles.registerTitle}>
            {strings.registerTitle}
          </Text>
          <View style={styles.registerContainer}>
            <TextBox
              onChangeText={this.props.changeRegisterName}
              placeholder={strings.placeholderName}
              value={this.props.registerName}
            />
            <TextBox
              onChangeText={this.props.changeRegisterEmail}
              placeholder={strings.placeholderEmail}
              value={this.props.registerEmail}
            />
            <TextBox
              isPassword
              onChangeText={this.props.changeRegisterPassword}
              placeholder={strings.placeholderPassword}
              value={this.props.registerPassword}
            />
            <TextBox
              isPassword
              onChangeText={this.props.changeRegisterConfirmPassword}
              onRowFocus={() => this.onRowFocus('confirmPassword')}
              placeholder={strings.placeholderConfirmPassword}
              ref="confirmPassword" // eslint-disable-line react/no-string-refs
              value={this.props.registerConfirmPassword}
            />
            <Button
              onPress={this.props.openLanguageSelector}
              style={styles.languageButton}
              title={
                getDescriptionLanguageSelected(this.props.selectedLanguage) ||
                strings.selectLanguage
              }
              titleStyle={styles.selectLanguageButton}
            />
            <Button
              onPress={this.onPressRegistrationButton}
              style={styles.registerButton}
              title={strings.registerButtonTitle}
              titleStyle={styles.registerTitleButton}
            />
          </View>
          <View style={styles.checkboxContainer}>
            <Checkbox
              checked={this.props.termsAndConditionsAccepted}
              onChange={this.props.changeTermsAndConditionsAccepted}
              size={Device.isTablet() ? 32 : 24}
            />
            <TouchableOpacity onPress={openTermsAndConditionsUrl}>
              <Text style={styles.checkBoxLabel}>
                {strings.termsAndConditionsDescription}
              </Text>
            </TouchableOpacity>
          </View>
          {params && params.shouldPresentFBLogin
            ? <View style={styles.facebookButtonContainer}>
                <ImageButton
                  height={Device.isTablet() ? 44 : 25}
                  image="fb_button"
                  onPress={this.onPressFBButton}
                  width={Device.isTablet() ? 220 : 150}
                />
              </View>
            : null}

        </View>
      </Screen>
    )
  }
}

export default connect(
  state => ({
    registerName: state.register.get('registerName'),
    registerEmail: state.register.get('registerEmail'),
    registerPassword: state.register.get('registerPassword'),
    registerConfirmPassword: state.register.get('registerConfirmPassword'),
    showLanguageSelector: state.register.get('showLanguageSelector'),
    showConfirmRegistration: state.register.get('showConfirmRegistration'),
    termsAndConditionsAccepted: state.register.get(
      'termsAndConditionsAccepted',
    ),
    selectedLanguage: state.register.get('selectedLanguage'),
  }),
  dispatch => ({
    changeRegisterName: text => dispatch(changeRegisterName(text)),
    changeRegisterEmail: text => dispatch(changeRegisterEmail(text)),
    changeRegisterPassword: text => dispatch(changeRegisterPassword(text)),
    changeRegisterConfirmPassword: text =>
      dispatch(changeRegisterConfirmPassword(text)),
    changeTermsAndConditionsAccepted: () =>
      dispatch(changeTermsAndConditionsAccepted()),
    openLanguageSelector: () => dispatch(openLanguageSelector()),
    closeLanguageSelector: () => dispatch(closeLanguageSelector()),
    openConfirmRegistration: () => dispatch(openConfirmRegistration()),
    closeConfirmRegistration: () => dispatch(closeConfirmRegistration()),
    changeSelectedLanguage: language => {
      dispatch(changeSelectedLanguage(language))
      dispatch(closeLanguageSelector())
    },
    onRegisterUserPressed: props => {
      dispatch(onRegisterUserPressed(props))
    },
    onLoginFBPressed: params => dispatch(onLoginFBPressed(params, true)),
  }),
)(RegisterScreen)
