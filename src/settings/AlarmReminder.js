/* @flow */

import React from 'react'
import {
  AsyncStorage,
  Button,
  TimePickerAndroid, // eslint-disable-line react-native/split-platform-components
  DatePickerIOS, // eslint-disable-line react-native/split-platform-components
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import {PRIMARY_COLOR} from '../common/NativoxTheme.js'
import moment from 'moment'
import ModalScreen from '../common/ModalScreen'
import PushNotification from 'react-native-push-notification'
import {strings} from '../stringsApp'
import {Answers} from 'react-native-fabric'
import userSession from '../user/userSession'

const styles: Object = StyleSheet.create({
  squareContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
  },
})

const SquareDay = ({
  day,
  isActive,
  onToggleDay,
  size,
}: {
  day: string,
  isActive: boolean,
  onToggleDay: Function,
  size: number,
}) => (
  <TouchableOpacity
    onPress={onToggleDay}
    style={[
      styles.squareContainer,
      {
        height: size,
        width: size,
        backgroundColor: isActive ? PRIMARY_COLOR : 'gray',
      },
    ]}
  >
    <Text style={{color: 'rgb(255, 255, 255)'}}>{day}</Text>
  </TouchableOpacity>
)

const days = [
  'monday',
  'tuesday',
  'wednesday',
  'thursday',
  'friday',
  'saturday',
  'sunday',
]

class AlarmReminder extends React.Component {
  state = {
    monday: false,
    tuesday: false,
    wednesday: false,
    thursday: false,
    friday: false,
    saturday: false,
    sunday: false,
    date: new Date(),
    showTimePicker: false,
    tempDate: new Date(),
  }

  props: {
    onSetAlarms: Function,
    onCancelAlarms: Function,
  }

  // $FlowIgnore
  componentDidMount = async () => {
    const oldStateString = await AsyncStorage.getItem('alarmReminder')

    if (oldStateString) {
      const oldState = JSON.parse(oldStateString)

      this.setState({
        ...oldState,
      })
    }
  }

  onChangeDay = (day: string) => {
    this.setState({
      [day]: !this.state[day],
    })
  }

  onPressSetAlarms = async () => {
    PushNotification.cancelAllLocalNotifications()

    await AsyncStorage.setItem('alarmReminder', JSON.stringify(this.state))

    days.forEach((item: string) => {
      if (this.state[item]) {
        const notificationDate = moment(this.state.date)
          .day(item)
          .second(0)
          .local()
          .toDate()

        PushNotification.localNotificationSchedule({
          title: 'Nativox',
          repeatType: 'week',
          date: notificationDate,
          message: strings.alarm_reminder_sentence,
          applicationIconBadgeNumber: 0,
        })
      }
    })

    this.props.onSetAlarms()

    const user = userSession.getUser()

    if (user) {
      Answers.logCustom('set_alarms', {
        name: user.name,
        idUser: user.idUser,
      })
    }
  }

  onPressCancelAlarms = () => {
    PushNotification.cancelAllLocalNotifications()
    AsyncStorage.removeItem('alarmReminder')
    this.props.onCancelAlarms()

    const user = userSession.getUser()

    if (user) {
      Answers.logCustom('cancel_alarms', {
        name: user.name,
        idUser: user.idUser,
      })
    }
  }

  onCloseTimePicker = () => {
    this.setState({
      showTimePicker: false,
    })
  }

  onOpenTimePicker = async () => {
    if (Platform.OS === 'ios') {
      this.setState({
        showTimePicker: true,
        tempDate: this.state.date
          ? moment(this.state.date).toDate()
          : new Date(),
      })

      return
    }

    try {
      const momentDate = moment(this.state.date)
      const {action, hour, minute} = await TimePickerAndroid.open({
        date: this.state.date ? moment(this.state.date).toDate() : new Date(),
        hour: momentDate.hour(),
        minute: momentDate.minute(),
        is24Hour: true,
      })
      if (action !== TimePickerAndroid.dismissedAction) {
        const momentSet = moment().minute(minute)
        momentSet.hour(hour)

        this.setState({
          date: momentSet.toDate(),
        })
      }
    } catch ({code, message}) {
      console.log('Cannot open date picker', message)
    }
  }

  onSetTime = () => {
    this.setState({
      showTimePicker: false,
      date: this.state.tempDate,
    })
  }

  onChangePickerTime = date => {
    this.setState({tempDate: date})
  }

  render() {
    return (
      <View>
        <ModalScreen
          height={300}
          isVisible={this.state.showTimePicker}
          onCloseButtonPressed={this.onCloseTimePicker}
          onTouchBackground={this.onCloseTimePicker}
          width={200}
        >
          <View>
            <DatePickerIOS
              date={this.state.tempDate || new Date()}
              mode="time"
              onDateChange={this.onChangePickerTime}
            />
            <Button
              color={PRIMARY_COLOR}
              onPress={this.onSetTime}
              title={'Set'}
            />
          </View>
        </ModalScreen>
        <Text
          style={{
            color: 'rgb(255, 255, 255)',
            textAlign: 'center',
            fontSize: 14,
          }}
        >
          {'\n'}
          {strings.alarm_reminder_explain}
          {'\n'}
          {strings.alarm_reminder_question}
          {'\n'}
        </Text>
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <SquareDay
            day={'M'}
            isActive={this.state.monday}
            onToggleDay={() => this.onChangeDay('monday')}
            size={32}
          />
          <SquareDay
            day={'T'}
            isActive={this.state.tuesday}
            onToggleDay={() => this.onChangeDay('tuesday')}
            size={32}
          />
          <SquareDay
            day={'W'}
            isActive={this.state.wednesday}
            onToggleDay={() => this.onChangeDay('wednesday')}
            size={32}
          />
          <SquareDay
            day={'T'}
            isActive={this.state.thursday}
            onToggleDay={() => this.onChangeDay('thursday')}
            size={32}
          />
          <SquareDay
            day={'F'}
            isActive={this.state.friday}
            onToggleDay={() => this.onChangeDay('friday')}
            size={32}
          />
          <SquareDay
            day={'S'}
            isActive={this.state.saturday}
            onToggleDay={() => this.onChangeDay('saturday')}
            size={32}
          />
          <SquareDay
            day={'S'}
            isActive={this.state.sunday}
            onToggleDay={() => this.onChangeDay('sunday')}
            size={32}
          />
        </View>
        <Text
          style={{
            color: 'rgb(255, 255, 255)',
            textAlign: 'center',
            fontSize: 14,
          }}
        >
          {'\n'}
          {strings.alarm_reminder_time}
          {'\n'}
        </Text>
        <TouchableOpacity onPress={this.onOpenTimePicker}>
          <Text
            style={{
              margin: 7,
              color: 'rgb(255, 255, 255)',
              textAlign: 'center',
              fontWeight: 'bold',
            }}
          >
            {this.state.date
              ? moment(this.state.date).format('HH:mm')
              : moment().format('HH:mm')}
          </Text>
        </TouchableOpacity>
        <View style={{padding: 1}}>
          <Button
            color={PRIMARY_COLOR}
            onPress={this.onPressSetAlarms}
            style={{fontWeight: 'bold'}}
            title={strings.alarm_reminder_set}
          />
        </View>
        <View style={{padding: 1}}>
          <Button
            color={PRIMARY_COLOR}
            onPress={this.onPressCancelAlarms}
            style={{margin: 5, fontWeight: 'bold'}}
            title={strings.alarm_reminder_cancel}
          />
        </View>
      </View>
    )
  }
}

export default AlarmReminder
