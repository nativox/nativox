/* @flow */

import React from 'react'
import {
  AsyncStorage,
  StyleSheet,
  Text,
  TouchableOpacity,
  Switch,
  View,
} from 'react-native'
import moment from 'moment'
import {connect} from 'react-redux'
import Screen from '../common/Screen'
import ModalScreen from '../common/ModalScreen'
import LanguageSelector from '../register/LanguageSelector'
import {strings} from '../stringsApp'
import {PRIMARY_COLOR} from '../common/NativoxTheme.js'
import userSession from '../user/userSession'
import {getDescriptionLanguageSelected} from '../register/languages'
import AlarmReminder from './AlarmReminder'
import Device from 'react-native-device-info'
import categories from '../categories'
import ProgressHud from '../common/ProgressHud'
import {Answers} from 'react-native-fabric'
import {NavigationActions} from 'react-navigation'

const {onLoadCategories, onClearCategories} = categories.eventHandlers

const styles: any = StyleSheet.create({
  buttonTitle: {
    textAlign: 'right',
    color: PRIMARY_COLOR,
    backgroundColor: 'rgba(255, 255, 255, 0)',
    fontSize: Device.isTablet() ? 24 : 16,
  },
  labelRow: {
    flex: 3,
    color: 'rgb(255, 255, 255)',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    fontSize: Device.isTablet() ? 24 : 16,
  },
  row: {
    flexDirection: 'row',
    marginTop: 15,
    marginBottom: 15,
  },
})

const Row = ({
  label,
  buttonTitle,
  onPressTouchable,
  onSwitchChange,
  isSwitchOn,
  isSwitch,
}: {
  label: string,
  buttonTitle?: string,
  onPressTouchable?: Function,
  onSwitchChange?: Function,
  isSwitchOn?: boolean,
  isSwitch?: boolean,
}) =>
  <View style={styles.row}>
    <Text style={styles.labelRow}>{label}</Text>
    {isSwitch
      ? <Switch onValueChange={onSwitchChange} value={isSwitchOn} />
      : <TouchableOpacity onPress={onPressTouchable} style={{flex: 2}}>
          <Text style={styles.buttonTitle}>{buttonTitle}</Text>
        </TouchableOpacity>}

  </View>

class SettingsScreen extends React.Component {
  static navigationOptions = {
    header: null,
  }

  props: {
    navigation: Object,
    onClearCategories: Function,
    onLoadCategories: Function,
  }

  state: {
    showSentenceAfterPlay?: boolean,
    showLanguageSelector: boolean,
    showAlarmReminder: boolean,
    showProgressDialog: boolean,
    date: ?Object,
  }

  constructor() {
    super()

    const user = userSession ? userSession.getUser() || {} : {}

    this.state = {
      showSentenceAfterPlay: user.showSentenceAfterPlay,
      showLanguageSelector: false,
      showAlarmReminder: false,
      showProgressDialog: false,
      date: null,
    }
  }

  componentDidMount = async (): any => {
    const oldStateString = await AsyncStorage.getItem('alarmReminder')

    if (oldStateString) {
      const oldState = JSON.parse(oldStateString)

      this.setState({
        date: oldState.date,
      })
    }
  }

  goBack = () => {
    this.props.navigation.goBack()
  }

  onShowSentenceAfterPlayChange = () => {
    const user = userSession.getUser()

    if (user) {
      userSession.setUser({
        ...user,
        showSentenceAfterPlay: !user.showSentenceAfterPlay,
      })
      this.setState({
        showSentenceAfterPlay: !user.showSentenceAfterPlay,
      })
    }
  }

  onOpenLanguageSelector = () => {
    this.setState({
      showLanguageSelector: true,
    })
  }

  onCloseLanguageSelector = () => {
    this.setState({
      showLanguageSelector: false,
    })
  }

  onLanguageSelected = async (language: string) => {
    userSession.setLanguage(language)
    strings.setLanguage(language.split('-')[0])

    this.setState({showLanguageSelector: false, showProgressDialog: true})

    try {
      await this.props.onClearCategories()
      await this.props.onLoadCategories()
    } catch (error) {
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: 'Login'})],
      })
      this.props.navigation.dispatch(resetAction)
    }

    setTimeout(() => this.setState({showProgressDialog: false}), 500)
    const user = userSession.getUser()

    if (user) {
      Answers.logCustom('language_selected', {
        name: user.name,
        idUser: user.idUser,
      })
    }
  }

  onCloseAlarmReminder = () => {
    this.setState({
      showAlarmReminder: false,
    })
  }

  onOpenAlarmReminder = () => {
    this.setState({
      showAlarmReminder: true,
    })
  }

  onSetAlarms = async () => {
    const reminderStateString = await AsyncStorage.getItem('alarmReminder')
    const reminderState = JSON.parse(reminderStateString)

    this.setState({
      showAlarmReminder: false,
      date: reminderState.date,
    })
  }

  onCancelAlarms = () => {
    this.setState({
      showAlarmReminder: false,
      date: null,
    })
  }

  render() {
    const user = userSession ? userSession.getUser() || {} : {}

    return (
      <Screen
        onPressLeftButton={this.goBack}
        showLeftNavBar
        title={strings.settings_label}
      >
        <ModalScreen
          height={340}
          isVisible={this.state.showAlarmReminder}
          onCloseButtonPressed={this.onCloseAlarmReminder}
          onTouchBackground={this.onCloseAlarmReminder}
          width={300}
        >
          <AlarmReminder
            onCancelAlarms={this.onCancelAlarms}
            onSetAlarms={this.onSetAlarms}
          />
        </ModalScreen>
        <ModalScreen
          height={320}
          isVisible={this.state.showLanguageSelector}
          onCloseButtonPressed={this.onCloseLanguageSelector}
          onTouchBackground={this.onCloseLanguageSelector}
          width={250}
        >
          <LanguageSelector
            onLanguageSelected={this.onLanguageSelected}
            selectedLanguage={user.languageKey}
          />
        </ModalScreen>
        <ProgressHud
          isVisible={this.state.showProgressDialog}
          text={strings.categories_progress}
        />
        <View style={{flex: 1}}>
          <Row
            isSwitch
            isSwitchOn={this.state.showSentenceAfterPlay}
            label={strings.settings_show_sentence_label}
            onSwitchChange={this.onShowSentenceAfterPlayChange}
          />
          <Row
            buttonTitle={
              this.state.date
                ? moment(this.state.date).format('HH:mm')
                : strings.select_label
            }
            label={strings.traning_alarms_label}
            onPressTouchable={this.onOpenAlarmReminder}
          />
          <Row
            buttonTitle={getDescriptionLanguageSelected(user.languageKey || '')}
            label={strings.native_language_label}
            onPressTouchable={this.onOpenLanguageSelector}
          />
        </View>
        <Text
          style={{
            fontSize: Device.isTablet() ? 17 : 13,
            color: 'rgb(255, 255, 255)',
            backgroundColor: 'transparent',
            textAlign: 'center',
          }}
        >
          Version: {Device.getVersion()} Build Number: {Device.getBuildNumber()}
        </Text>
      </Screen>
    )
  }
}

export default connect(null, dispatch => ({
  onLoadCategories: () => dispatch(onLoadCategories()),
  onClearCategories: () => dispatch(onClearCategories()),
}))(SettingsScreen)
