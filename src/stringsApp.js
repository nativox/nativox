/* @flow */

import LocalizedStrings from 'react-native-localization'
import stringsEnglish from './stringsEnglish'
import stringsSpanish from './stringsSpanish'

export const strings = new LocalizedStrings({
  en: stringsEnglish,
  es: stringsSpanish,
})
