/* @flow */

export default {
  userFeedbackPlaceHolder:
    'What do you think about this lesson or how would you improve it? Tell us your opinion here.',
  errorNewPasswordUpdateAccount:
    'Required a minimum of 5 characters in your new password.',
  errorNewOldPasswordUpdateAccount:
    'New and confirmation password are not the same , please type the same and try it again later',
  sendButtonTitle: 'Send',
  userDataTitleScreen: 'User Data',
  userDataExplanaitionLabel:
    'You can change the username of your acount here, also you can change your password confirming your previous one.',
  userNameLabel: 'User Name',
  userPasswordLabel: 'Password',
  userPasswordConfirmLabel: 'Confirm Password',
  saveButtonTitle: 'Save',
  restoreButtonTitle: 'Restore courses',
  logoutButtonTitle: 'Log out',
  loadingProgressDialogTitle: 'Loading',
  savingProgressDialogTitle: 'Saving',
  sendingProgressDialogTitle: 'Sending',
  contactTitleScreen: 'Contact',
  emailPlaceHolder: 'Email to receive a reply (optional)',
  messagePlaceHolder: 'Message you would like to send us.',
  selectLanguage: "What's your language?",
  registerTitle: 'Sign Up',
  placeholderName: 'Name',
  placeholderEmail: 'Email',
  placeholderPassword: 'Password',
  placeholderConfirmPassword: 'Confirm Password',
  registerButtonTitle: 'Register',
  termsAndConditionsDescription: 'Accept the terms of data\n protection.',
  setButtonTitle: 'Set',
  confirmRegistrationTitle: 'Is your data correct?',
  showPasswordTitle: 'Show',
  yesButtonTitle: 'Yes',
  changeButtonTitle: 'Change',
  loginTitle: 'Log in',
  labelForgotPassword: 'Forgot password?',
  labelRegister: "Don't you have an account?",
  signUpButtonTitle: 'Sign up',
  progressLogin: 'Starting',
  progressResetting: 'Resetting',
  issueLogin: 'There was an issue logging.',
  issueResetPassword: 'There was an issue resetting password.',
  labelResetPassword: 'Please provide an email address to reset your password.',
  resetButtonTitle: 'Reset',
  progressSendingFeedback: 'Sending feedback',
  successfulSendFeedback: 'Feedback sent successfully.',
  issueSendFeedback: 'There has been some issue sending your feedback.',
  error_registration_name: 'Enter a name for your registration.',
  error_registration_email: 'Enter an email address for your registration.',
  error_registration_email_no_valid: 'Email address is not valid.',
  error_registration_password: 'Enter a password for your registration.',
  error_registration_confirmation_password:
    'Enter a confirmation password for your registration.',
  error_registration_password_length:
    'Your password must have at least 6 characters.',
  error_registration_password_different:
    'Your password and confirmation password are different.',
  error_registration_no_language: "You haven't selected a language",
  error_registration_no_tcs: "You haven't accepted the terms and conditions.",
  error_registration_service_failure:
    'There has been some issue registering your account.',
  text_first_page_welcome_tutorial: 'Read this quick intro.',
  text_second_page_welcome_tutorial:
    'Nativox is a new language learning method based on intonation.',
  text_third_page_welcome_tutorial:
    'Intonation is the pattern or melody created by the rise and fall of the pitch of the voice when speaking. Think of them as beats in music.',
  text_forth_page_welcome_tutorial:
    'Every language has its own intonation patterns, its own music. Spanish, for instance, has a completely different melody than English.',
  text_fifth_page_welcome_tutorial:
    'Nativox will help you visualize and embrace the English intonation patterns. So you can understand and be understood better.',
  pendingPurchaseError:
    'We tried to recover a pending purchase, try to get in contact with us to solve this.',
  purchaseError:
    'It was not possible to complete this purchase, try it again later.',
  text_first_page_tutorial:
    "Listen how the American girl says the sentence and pay attention to the intonation graph. It shows the sounds that are more important and those that aren't.",
  text_second_page_tutorial: 'Record yourself saying the same sentence.',
  text_third_page_tutorial:
    'Compare your pronunciation with that of the girl and get your score. Important: You have to score above 70%  accuracy to move to the next and final step.',
  text_forth_page_tutorial:
    'Now discover what you said, after saying it correctly.',
  text_video_tutorial:
    'If you still have doubts, take a look at this short video and discover how Nativox works.',
  my_courses_label: 'My Courses',
  courses_label: 'Courses',
  settings_label: 'Settings',
  account_label: 'Account',
  contact_label: 'Contact',
  start_label: 'Start',
  next_label: 'Next',
  error_login_fb:
    'There has been an error trying to login with Facebook, try it again later',
  error_saving_account:
    'It was not possible to save your new details, please try again later.',
  error_app_store: 'Could not connect to itunes store.',
  error_no_purchases: "We didn't find any purchases to restore.",
  restore_purchases_success: 'Successfully restores all your purchases.',
  progress_dialog_logout: 'Logging out',
  account_title: 'Account',
  error_expired_session: 'Session has expired please log in again.',
  error_load_categories:
    'An error ocurred while loading categories, log in again',
  error_contact_no_message: 'There is no message',
  contact_message_success: 'Message sent successfully.',
  error_contact_fail: 'There has been some issue sending your message.',
  error_audio_recording: 'Failure recording your voice.',
  error_loading_audio: 'Failure loading audio.',
  listening_game_label: 'What did she say?',
  listening_game_incorrect_letter:
    'The letter you typed is not correct, try it again.',
  listening_game_all_guessed: 'Congratulations you have guessed them all!!!',
  statistics_progress_label: 'Progress',
  statistics_completed_label: 'Completed',
  statistics_average_grade_label: 'Average Grade',
  feedback_progress_send: 'Sending Feedback',
  mycourses_title: 'My Courses',
  profile_title: 'Profile',
  alarm_reminder_sentence: 'Time for your 15 minutes of english practice.',
  alarm_reminder_explain:
    '2 or 3 sessions of 15 minutes per week can go a long way. And its less than going to the gym.',
  alarm_reminder_question: 'Do you want to set up an alarm?',
  alarm_reminder_time: 'At what time do you want to be reminded?',
  alarm_reminder_set: 'Set Alarms',
  alarm_reminder_cancel: 'Cancel Alarms',
  categories_progress: 'Loading categories',
  settings_show_sentence_label: 'Show sentence after pressing play',
  select_label: 'Select',
  traning_alarms_label: 'Training Alarms',
  native_language_label: 'Your native language',
  no_courses_purchased: 'No courses purchased yet...',
  go_to_courses_button: 'Go to Courses',
}
