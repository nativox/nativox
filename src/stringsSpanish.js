/* @flow */

export default {
  userFeedbackPlaceHolder:
    '¿Cual es tu opinion de esta lección o como la mejorarias? Cuentanos tus impresiones aquí.',
  errorNewPasswordUpdateAccount:
    'Se requiere un mínimo de 5 caracteres en tu nueva contraseña.',
  errorNewOldPasswordUpdateAccount:
    'La contraseña nueva y de confirmación no son la misma, introduce la misma y vuelve a intentarlo.',
  sendButtonTitle: 'Enviar',
  userDataTitleScreen: 'Datos de Usuario',
  userDataExplanaitionLabel:
    'En esta sección puedes cambiar tu nombre de usuario y la contraseña de tu cuenta.',
  userNameLabel: 'Nombre de Usuario',
  userPasswordLabel: 'Contraseña',
  userPasswordConfirmLabel: 'Confirmar Contraseña',
  saveButtonTitle: 'Guardar',
  restoreButtonTitle: 'Restaurar cursos',
  logoutButtonTitle: 'Salir',
  loadingProgressDialogTitle: 'Cargando',
  savingProgressDialogTitle: 'Guardando',
  sendingProgressDialogTitle: 'Enviando',
  contactTitleScreen: 'Contacto',
  emailPlaceHolder: 'Email donde recibir contestación (opcional)',
  messagePlaceHolder: 'Mensaje que nos quieres enviar.',
  selectLanguage: '¿Cuál es tu idioma?',
  registerTitle: 'Regístrate',
  placeholderName: 'Nombre',
  placeholderEmail: 'Email',
  placeholderPassword: 'Contraseña',
  placeholderConfirmPassword: 'Confirmar Contraseña',
  registerButtonTitle: 'Regístrate',
  termsAndConditionsDescription:
    'Acepto las condiciones de \nprotección de datos.',
  setButtonTitle: 'Establecer',
  confirmRegistrationTitle: '¿estos datos son correctos?',
  showPasswordTitle: 'Ver',
  yesButtonTitle: 'Si',
  changeButtonTitle: 'Modificar',
  loginTitle: 'Log in',
  labelForgotPassword: '¿Olvidaste tu contraseña?',
  labelRegister: '¿No tienes una cuenta?',
  signUpButtonTitle: 'Regístrate',
  progressLogin: 'Logueandote',
  progressResetting: 'Reseteando',
  issueLogin:
    'Hubo algun problema durante el login, vuelva a intentarlo más tarde.',
  issueResetPassword:
    'Hubo algun problema durante el reseteo de la password, vuelva a intentarlo más tarde.',
  labelResetPassword: 'Danos un email para que podamos resetear tu password.',
  resetButtonTitle: 'Resetear',
  progressSendingFeedback: 'Enviando feedback',
  successfulSendFeedback: 'Feedback enviado correctamente.',
  issueSendFeedback:
    'Hubo un problema enviando el feedback, intentalo más tarde',
  error_registration_name: 'Danos un nombre para tu registro.',
  error_registration_email:
    'Danos una dirección de correo electrónico para tu registro.',
  error_registration_email_no_valid:
    'El correo electrónico introducido no es válido.',
  error_registration_password: 'Danos una contraseña para tu registro.',
  error_registration_confirmation_password:
    'Danos la confirmación de tu contraseña.',
  error_registration_password_length:
    'Tu contraseña debe de tener al menos 6 caracteres.',
  error_registration_password_different:
    'Tu contraseña y la contraseña de confirmación son diferentes.',
  error_registration_no_language: 'No seleccionaste tu idioma',
  error_registration_no_tcs:
    'No aceptaste las condiciones de protección de datos.',
  error_registration_service_failure:
    'Hubo algun problema durante el registro de tu cuenta, vuelva a intentarlo más tarde.',
  text_first_page_welcome_tutorial: 'Lee esta rápida introducción.',
  text_second_page_welcome_tutorial:
    'Nativox es un nuevo método para aprender idiomas basado en la entonación.',
  text_third_page_welcome_tutorial:
    'Por entonación entendemos la melodía creada por las subidas y bajadas en el tono de voz mientras hablamos. Piensa como si fueran los beats de un ritmo de música.',
  text_forth_page_welcome_tutorial:
    'Cada idioma tiene sus propios patrones de entonación. Su propia música. El español, por ejemplo, es una melodía completamente distinta del inglés.',
  text_fifth_page_welcome_tutorial:
    'Nativox te ayudará a visualizar e integrar los patrones de entonación del inglés.',
  pendingPurchaseError:
    'Hemos intentado recuperar una compra realizada anteriormente, pero ha habido un error, por favor contacte con nosostros para solucionar el error.',
  purchaseError:
    'No fue posible completar la compra, por favor intentelo más tarde.',
  text_first_page_tutorial:
    'Escucha cómo la chica americana del video dice una frase y fíjate en el gráfico que marca la entonación de la frase, es decir, los sonidos que son más y menos importantes.',
  text_second_page_tutorial: 'Grábate diciendo la misma frase.',
  text_third_page_tutorial:
    'Compara tu pronunciación con la de la chica y obtén tu puntuación. Importante: tienes que puntuar por encima de un 70% de precisión para poder pasar al siguiente y último paso.',
  text_forth_page_tutorial:
    'Ahora sí, descubre lo que has dicho después de haberlo  pronunciado correctamente.',
  text_video_tutorial:
    'Si aún tienes dudas, échale un vistazo a este breve video y descubre cómo funciona Nativox.',
  my_courses_label: 'Mis Cursos',
  courses_label: 'Cursos',
  settings_label: 'Configuración',
  account_label: 'Cuenta',
  contact_label: 'Contacto',
  start_label: 'Empezar',
  next_label: 'Siguiente',
  error_login_fb:
    'Ha habido un error conectando con facebook, por favor intentelo más tarde.',
  error_saving_account:
    'No fue posible guardar sus datos de usuario, por favor intentelo más tarde.',
  error_app_store: 'No se pudo obtener conexión con la AppStore.',
  error_no_purchases: 'No se encontro ninguna compra que restaurar.',
  restore_purchases_success:
    'Las compras se han restaurado de forma satisfactoria.',
  progress_dialog_logout: 'Saliendo de la sesión',
  account_title: 'Cuenta',
  error_expired_session:
    'Su sesión ha expirado, por favor logueate para poder continuar utilizando Nativox.',
  error_load_categories:
    'Ha habido un error cargando las categorias, intenta loguearte otra vez.',
  error_contact_no_message: 'Escribe un mensaje',
  contact_message_success: 'Mensaje enviado satisfactoriamente.',
  error_contact_fail:
    'Ha habido un error en el enviado del mensaje, por favor intentalo más tarde.',
  error_audio_recording: 'Ha habido un error grabando tu voz.',
  error_loading_audio: 'Ha habido un error cargando tu voz.',
  listening_game_label: '¿Que ha dicho?',
  listening_game_incorrect_letter:
    'La letra que tecleaste es incorrect, vuelve a intentarlo',
  listening_game_all_guessed: 'Enhobuena adivinaste la frase!!!',
  statistics_progress_label: 'Progreso',
  statistics_completed_label: 'Completado',
  statistics_average_grade_label: 'Nota Media',
  feedback_progress_send: 'Enviando Feedback',
  mycourses_title: 'Mis Cursos',
  profile_title: 'Perfil',
  alarm_reminder_sentence: 'Que tal practicar unos minutos inglés.',
  alarm_reminder_explain:
    '2 o 3 lecciones por semana pueden hacer mucho. Y es menos que ir al gimnasio.',
  alarm_reminder_question: '¿Quieres configuras las alarmas?',
  alarm_reminder_time: '¿A qué hora quieres que te lo recuerde?',
  alarm_reminder_set: 'Configurar Alarmas',
  alarm_reminder_cancel: 'Cancelar Alarmas',
  categories_progress: 'Cargando categorias',
  settings_show_sentence_label: 'Mostrar frase en play',
  select_label: 'Seleccionar',
  traning_alarms_label: 'Alarmas',
  native_language_label: 'Tu idioma',
  no_courses_purchased: 'No hay cursos todavía...', 
  go_to_courses_button: 'Ir a Cursos',  
}
