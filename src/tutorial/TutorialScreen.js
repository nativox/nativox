/* @flow */

import React from 'react'
import {
  ActivityIndicator,
  StyleSheet,
  Dimensions,
  View,
  WebView,
} from 'react-native'
import Screen from '../common/Screen'
import userSession from '../user/userSession'
import TappableIcon from '../common/TappableIcon'

const styles: Object = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0)',
  },
})

const injectScript = `
$('.go_to_tutorial').click(function(){
  postMessage('goToTutorial');

		return false;
});

$('.go_to_courses').click(function(){
  postMessage('goToCourses');
  
  return false;
});

$('.go_to_cart').click(function(){
  postMessage('goToCheckout');  

  goto('cart');return false;
});
`

const ActivityIndicatorView = () => (
  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    <ActivityIndicator animating size="large" style={{height: 80}} />
  </View>
)

let tabBarIconStyleCap = null

class TutorialScreen extends React.Component {
  static navigationOptions = {
    header: null,
    tabBarIcon: ({tintColor}) => {
      if (!tabBarIconStyleCap || tabBarIconStyleCap.color !== tintColor) {
        tabBarIconStyleCap = {color: tintColor}
      }

      return (
        <TappableIcon
          containerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          name="graduation-cap"
          size={21}
          style={tabBarIconStyleCap}
        />
      )
    },
  }

  props: {
    navigation: Object,
  }

  onMessage = (event: any) => {
    const message = event.nativeEvent.data

    switch (message) {
      case 'goToTutorial':
        this.props.navigation.navigate('VideoTutorial')
        break
      case 'goToCourses':
        this.props.navigation.navigate('Categories')
        break
      case 'goToCheckout':
        this.props.navigation.navigate('Categories')
        break
    }
  }

  render() {
    const {width, height} = Dimensions.get('window')
    const user = userSession ? userSession.getUser() || {} : {}

    return (
      <Screen fullscreen>
        <WebView
          injectedJavaScript={injectScript}
          onMessage={this.onMessage}
          renderLoading={() => <ActivityIndicatorView />}
          source={{
            uri: `http://nativox.com/ini/app_sales?os=ios&width=${width}&height=${height}&lang=${user.language
              ? user.language
              : ''}`,
          }}
          startInLoadingState
          style={styles.container}
        />
      </Screen>
    )
  }
}

export default TutorialScreen
