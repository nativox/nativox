/* @flow */

import React from 'react'
import {
  Dimensions,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Image,
} from 'react-native'
import Screen from '../common/Screen'
import Device from 'react-native-device-info'
import {strings} from '../stringsApp'
import {PRIMARY_COLOR} from '../common/NativoxTheme'
import Video from 'react-native-video'
import Carousel from 'react-native-carousel'
import userSession from '../user/userSession'

const {width} = Dimensions.get('window')
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  video: {
    flex: 2,
  },
  containerPage: {
    flex: 1,
    padding: 15,
    width,
  },
  image: {
    flex: 2,
  },
  descriptionContainer: {
    flex: 1,
    padding: 15,
  },
  description: {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    color: 'rgb(255, 255, 255)',
    fontSize: Device.isTablet() ? 28 : 16,
    textAlign: 'center',
    fontFamily: 'HelveticaNeue-Light',
  },
})

const user = userSession.getUser()

const pages = [
  {
    image: 'play_tutorial',
    description: strings.text_first_page_tutorial,
  },
  {
    image: 'record_tutorial',
    description: strings.text_second_page_tutorial,
  },
  {
    image: 'test_tutorial',
    description: strings.text_third_page_tutorial,
  },
  {
    image: 'translation_tutorial',
    description: strings.text_forth_page_tutorial,
  },
  {
    video: user && user.language === 'es'
      ? 'http://nativox.com/videos/tutorial/Nativox_640.mp4'
      : 'http://nativox.com/videos/tutorial/Nativox_en_640.mp4',
    description: strings.text_video_tutorial,
  },
]

class VideoTutorialScreen extends React.Component {
  static navigationOptions = {header: null}

  state = {
    paused: true,
    activePage: 0,
  }

  props: {
    navigation: Object,
  }

  onPressBackButton = () => {
    this.props.navigation.goBack()
  }

  onTouchVideoScreen = () => {
    this.setState({paused: !this.state.paused})
  }

  onPageChange = (activePage: number) => {
    if (activePage === pages.length - 1) {
      this.setState({
        activePage,
        paused: false,
      })
    } else {
      this.setState({
        activePage,
        paused: true,
      })
    }
  }

  render() {
    return (
      <Screen
        fullscreen
        onPressLeftButton={this.onPressBackButton}
        showLeftNavBar
        title={'Tutorial'}>
        <View style={styles.container}>
          <Carousel
            animate={false}
            indicatorAtBottom
            indicatorColor={PRIMARY_COLOR}
            indicatorOffset={10}
            indicatorSize={20}
            onPageChange={this.onPageChange}>
            {pages.map((page, index) =>
              <View key={index} level={5} style={styles.containerPage}>
                {page.image
                  ? <Image
                      resizeMode={'contain'}
                      source={{uri: page.image, isStatic: true}}
                      style={styles.image}
                    />
                  : <TouchableOpacity
                      onPress={this.onTouchVideoScreen}
                      style={{flex: 1}}>
                      <Video
                        paused={this.state.paused}
                        rate={1.0}
                        resizeMode="contain"
                        source={{uri: page.video}}
                        style={styles.video}
                        volume={1.0}
                      />
                    </TouchableOpacity>}
                <View style={styles.descriptionContainer}>
                  <Text style={styles.description}>
                    {page.description}
                  </Text>
                </View>
              </View>,
            )}
          </Carousel>
        </View>
      </Screen>
    )
  }
}

export default VideoTutorialScreen
