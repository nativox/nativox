/* @flow */

import {AsyncStorage} from 'react-native'
import Device from 'react-native-device-info'

type TypeUser = {
  name: string,
  email: string,
  token: string,
  idUser: string,
  language?: string,
  languageKey?: string,
  idSession?: string,
  showSentenceAfterPlay?: boolean,
}

type TypeUserSession = {
  user: ?TypeUser,
  getUser: Function,
  setUser: Function,
  setLanguage: Function,
  resetUser: Function,
}

let userSession: ?TypeUserSession = null

class UserSession {
  constructor() {
    if (!userSession) {
      // $FlowIgnore
      userSession = this
      userSession.user = {
        name: '',
        email: '',
        token: '',
        idUser: '',
        language: Device.getDeviceLocale().split('-')[0],
        languageKey: Device.getDeviceLocale(),
        idSession: '',
        showSentenceAfterPlay: true,
      }

      AsyncStorage.getItem('user').then(userString => {
        if (userString && userSession) {
          userSession.user = JSON.parse(userString)
        }
      })
    }

    return userSession
  }

  user: TypeUser

  setUser(user: TypeUser) {
    if (userSession) {
      userSession.user = {
        ...userSession.user,
        ...user,
      }
      AsyncStorage.setItem('user', JSON.stringify(userSession.user))
    }
  }

  getUser(): ?TypeUser {
    if (userSession) {
      return userSession.user
    }

    return null
  }

  resetUser() {
    if (userSession && userSession.user) {
      userSession.user = {
        name: '',
        email: '',
        token: '',
        idUser: '',
        language: userSession.user.language,
        languageKey: userSession.user.languageKey,
        idSession: '',
        showSentenceAfterPlay: true,
      }

      AsyncStorage.setItem('user', JSON.stringify(userSession.user))
    }
  }

  setLanguage(language: string) {
    if (this.user) {
      this.user.language = language.split('-')[0]
      this.user.languageKey = language
      AsyncStorage.setItem('user', JSON.stringify(this.user))
    }
  }
}

export default new UserSession()
